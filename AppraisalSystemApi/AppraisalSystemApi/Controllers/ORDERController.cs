﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AppraisalSystemApi.Models;
using NHibernate;
using Newtonsoft.Json;
namespace AppraisalSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ORDERController : ControllerBase
    {
        IConfiguration _iconfiguration;
        public ORDERController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        // GET api/values
       // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("Get")]
        public ActionResult<ORDER> Get(string APPLICATION_NO)
        {
            ORDER od = new ORDER();
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(APPLICATIONMapping.getmap());
            maplist.Add(DEBITURMapping.getmap());
            maplist.Add(ASSESSMENT_HISTORYMapping.getmap());
            maplist.Add(CERTIFICATEMapping.getmap());
            maplist.Add(APPFLAGMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);

            od.APPLICATION  = session.QueryOver<APPLICATION>().Where(f => f.APPLICATION_NO == APPLICATION_NO).List()[0];
            if(od.APPLICATION != null)
            {
                od.APPFLAG = session.QueryOver<APPFLAG>().Where(f => f.AP_REGNO == APPLICATION_NO).List()[0];
                od.DEBITUR = session.QueryOver<DEBITUR>().Where(f => f.APPLICATION_ID == od.APPLICATION.ID).List()[0];
                od.ASSESSMENT_HISTORY = session.QueryOver<ASSESSMENT_HISTORY>().Where(f => f.APPLICATION_ID == od.APPLICATION.ID).List() as List<ASSESSMENT_HISTORY>;
                od.CERTIFICATE = session.QueryOver<CERTIFICATE>().Where(f => f.APPLICATION_ID == od.APPLICATION.ID).List() as List<CERTIFICATE>;
                string query = @"Select
                                alttr.TR_ALT_DESC as Kriteria 
                                ,d.DUPDESC as Matchlevel
                                ,pe.CUSTOMER_NAME as Nama
                                , a.APPLICATION_NO as NoFererensi
                                , tr.TR_MAIN_DESC as Status
                                , br.BRANCHNAME as Cabang
                                From los.APPLICATION a
                                Left join los.APPCUST c on c.AP_REGNO =a.APPLICATION_NO
                                Left join los.DEBITUR pe on pe.ID = c.CU_REF 
                                Join los.APPDUPRESULT r on r.SOURCEKEY = c.CU_REF
                                Join dup.RFDUPCRITERIA d on d.DUPCATCODE = r.DUPCATCODE and d.DUPCODE =r.DUPCODE
                                Join los.RFBRANCH br on br.BRANCHID =a.REGIONAL
                                join flow.APPFLAG flag on flag.AP_REGNO = a.APPLICATION_NO
                                join flow.RF_TRACKMAIN tr on tr.TR_MAIN_ID = flag.AP_CURRTRCODE 
                                left join flow.ALTFLAG alt on alt.ALT_TYPE = 'DEDUP' and alt.AP_REGNO = a.APPLICATION_NO 
                                left join flow.RF_TRACKALT alttr on alttr.TR_ALT_ID = alt.ALT_CURRTRCODE
                                where a.APPLICATION_NO ='" + APPLICATION_NO + "'";
                od.DEDUPRESULT = SessionFactoryBuilder.GetList<DEDUPRESULT>(connectionStringName, query, session);

            }
                
            return od;
        }

        [HttpPost("UPDATE")]
        public ActionResult<Dictionary<string, string>> UPDATE([FromBody]ORDER ORDER)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(APPLICATIONMapping.getmap());
            maplist.Add(DEBITURMapping.getmap());
            maplist.Add(ASSESSMENT_HISTORYMapping.getmap());
            maplist.Add(CERTIFICATEMapping.getmap());
            maplist.Add(APPFLAGMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            ITransaction tran = session.BeginTransaction();
            try
            {
                session.Update(ORDER.APPLICATION);
                session.Update(ORDER.DEBITUR);
                foreach(CERTIFICATE cer in ORDER.CERTIFICATE)
                {
                    if (cer.ID == ORDER.APPLICATION.ID)
                    {
                        cer.ID = Guid.NewGuid();
                        session.Save(cer);
                    }
                    else
                        session.Update(cer);
                }
              

                tran.Commit();
                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Scan Status");
                vsLogMessage.Add("msg", "Save Sucess!!");
                return vsLogMessage;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                vsLogMessage.Add("errorcode", "500");
                vsLogMessage.Add("title", "Scan Status");
                vsLogMessage.Add("msg", ex.Message);
                return vsLogMessage;
            }

        }

        // GET api/values
        // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("SUSER")]
        public ActionResult<List<SUSER>> SUSER()
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            string query = @"select 
                    usr.USERID
                    ,usr.USERNAME
                    ,gr.GROUPNAME
                    from sec.SCUSER usr
                    join sec.SCGROUP gr on usr.GROUPID = gr.GROUPID";
            List<SUSER> voTable =SessionFactoryBuilder.GetList<SUSER>(connectionStringName, query);
            return voTable;
        }

      

    }
}
