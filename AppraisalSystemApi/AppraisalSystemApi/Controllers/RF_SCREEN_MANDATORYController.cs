﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AppraisalSystemApi.Models;
using NHibernate;
namespace AppraisalSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RF_SCREEN_MANDATORYController : ControllerBase
    {
        IConfiguration _iconfiguration;
        public RF_SCREEN_MANDATORYController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        // GET api/values
       // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("Get")]
        public ActionResult<Dictionary<string, string>> Get(string screen)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(RF_SCREEN_MANDATORYMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            List<RF_SCREEN_MANDATORY> voTable = session.QueryOver<RF_SCREEN_MANDATORY>().Where(s=>s.SCREEN_NAME == screen && s.IS_ACTIVE ==true).List() as List<RF_SCREEN_MANDATORY>;
            Dictionary<string, string> data = voTable.OrderBy(o=> o.ID)
                                            .ToDictionary(mc => mc.FIELD_NAME,
                                                          mc => "#ffe490",
                                StringComparer.OrdinalIgnoreCase);
            return data;
        }

        
    }
}
