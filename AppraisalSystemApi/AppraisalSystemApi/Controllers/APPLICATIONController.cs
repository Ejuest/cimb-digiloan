﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AppraisalSystemApi.Models;
using NHibernate;
namespace AppraisalSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class APPLICATIONController : ControllerBase
    {
        IConfiguration _iconfiguration;
        public APPLICATIONController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        // GET api/values
       // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("Get")]
        public ActionResult<List<APPLICATION>> Get()
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(APPLICATIONMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            List<APPLICATION> voTable = session.QueryOver<APPLICATION>().List() as List<APPLICATION>;
            return voTable;
        }

        [HttpGet("GetByCode")]
        public ActionResult<List<APPLICATION>> GetByCode(string Code)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(APPLICATIONMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            List<APPLICATION> voTable = session.QueryOver<APPLICATION>().Where(f=>f.APPLICATION_NO == Code).List() as List<APPLICATION>;
            return voTable;
        }


        [HttpPost("Add")]
        public ActionResult<string> Add(APPLICATION APPLICATION)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(APPLICATIONMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            ITransaction tran = session.BeginTransaction();
            try
            {
                APPLICATION.ID = Guid.NewGuid();
                session.Save(APPLICATION);
                tran.Commit();
                return "Add New Sucess!!";
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return ex.Message;
            }

        }

        [HttpPost("Edit")]
        public ActionResult<string> Edit(APPLICATION APPLICATION)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(APPLICATIONMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            ITransaction tran = session.BeginTransaction();
            try
            {
                session.Update(APPLICATION);
                tran.Commit();
                return "Update Sucess!!";
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return ex.Message;
            }

        }

        // GET api/values
        // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("Mandatory")]
        public ActionResult<List<Parameter>> Mandatory()
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            string query = @"select col.name as name
                            FROM SYS.COLUMNS col 
                            join sys.types typ on col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id  
                            where object_id = object_id('los." + nameof(APPLICATION)+ "') and col.is_nullable =0";
            List<Parameter> voTable =SessionFactoryBuilder.GetList<Parameter>(connectionStringName, query);
            return voTable;
        }

        [HttpGet("Inbox")]
        public ActionResult<List<INBOX>> Inbox()
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            string query = @"select 
	                       Flag.FLAG_APPRAISAL_ID as type
	                      ,app.APPLICATION_NO as noapplikasi
	                      ,app.DATE_OF_APPLICATION as tglterima
	                      ,deb.CUSTOMER_NAME as namadebitur
	                      ,deb.ADDRESS_OF_GUARANTEE_1 as alamat
	                      ,deb.TYPE_OF_GUARANTEE_ID as typejaminan
	                      ,gur.NAME as namajaminan
	                      ,his.LASTTR_DATE as trackdate
	                      from flow.APPFLAG Flag
	                      join los.APPLICATION app on app.APPLICATION_NO = Flag.AP_REGNO
	                      join los.DEBITUR deb on app.ID = deb.APPLICATION_ID
	                      join los.RF_TYPE_OF_GUARANTEE gur on gur.ID = deb.TYPE_OF_GUARANTEE_ID
	                      join flow.TRACKHISTORY his on his.AP_REGNO =Flag.AP_REGNO";

            List<INBOX> voTable = SessionFactoryBuilder.GetList<INBOX>(connectionStringName, query);
            return voTable;
        }

        [HttpGet("InboxByType")]
        public ActionResult<List<INBOX>> InboxByType(string Type)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            string query = @"select 
	                       Flag.FLAG_APPRAISAL_ID as type
	                      ,app.APPLICATION_NO as noapplikasi
	                      ,app.DATE_OF_APPLICATION as tglterima
	                      ,deb.CUSTOMER_NAME as namadebitur
	                      ,deb.ADDRESS_OF_GUARANTEE_1 as alamat
	                      ,deb.TYPE_OF_GUARANTEE_ID as typejaminan
	                      ,gur.NAME as namajaminan
	                      ,his.LASTTR_DATE as trackdate
	                      from flow.APPFLAG Flag
	                      join los.APPLICATION app on app.APPLICATION_NO = Flag.AP_REGNO
	                      join los.DEBITUR deb on app.ID = deb.APPLICATION_ID
	                      join los.RF_TYPE_OF_GUARANTEE gur on gur.ID = deb.TYPE_OF_GUARANTEE_ID
	                      join flow.TRACKHISTORY his on his.AP_REGNO =Flag.AP_REGNO WHERE Flag.FLAG_APPRAISAL_ID ='" +Type +"'";

            List<INBOX> voTable = SessionFactoryBuilder.GetList<INBOX>(connectionStringName, query);
            return voTable;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

     

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
