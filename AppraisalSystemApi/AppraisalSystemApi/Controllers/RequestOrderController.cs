﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AppraisalSystemApi.Models;
using NHibernate;
namespace AppraisalSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestOrderController : ControllerBase
    {
        IConfiguration _iconfiguration;
        public RequestOrderController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        // GET api/values
        [HttpGet("GetRequestOrder")]
        public ActionResult<List<RequestOrder>> GetRequestOrder()
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(RequestOrderMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            List<RequestOrder> voTable = session.QueryOver<RequestOrder>().List() as List<RequestOrder>;
            return voTable;
        }


        // GET api/values
        [HttpPost("AddRequestOrder")]
        public ActionResult<string> AddRequestOrder(RequestOrder RequestOrder)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(RequestOrderMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            ITransaction tran = session.BeginTransaction();
            try
            {
                session.Save(RequestOrder);
                tran.Commit();
                return "Add Request Order Berhasil!!";
            }
            catch(Exception ex)
            {
                tran.Rollback();
                return ex.Message;
            }
      
        }

        // GET api/values
        [HttpPost("UpdateRequestOrder")]
        public ActionResult<string> UpdateRequestOrder(RequestOrder RequestOrder)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(RequestOrderMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            ITransaction tran = session.BeginTransaction();
            try
            {
                session.Update(RequestOrder);
                tran.Commit();
                return "Add Request Order Berhasil!!";
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return ex.Message;
            }

        }

    }
}
