﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AppraisalSystemApi.Models;
using NHibernate;
using Newtonsoft.Json;
namespace AppraisalSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DOCUMENT_CHEKINGController : ControllerBase
    {
        IConfiguration _iconfiguration;
        public DOCUMENT_CHEKINGController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        // GET api/values
       // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("Get")]
        public ActionResult<DOCUMENT_CHEKING> Get(string APPLICATION_NO)
        {
            DOCUMENT_CHEKING od = new DOCUMENT_CHEKING();
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(DOCUMENTMapping.getmap());
            maplist.Add(RF_DOCUMENTMapping.getmap());
            maplist.Add(DOCUMENT_DETAILMapping.getmap());
            maplist.Add(DOCUMENT_DETAIL_UPLOADMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);

         
                
            return od;
        }

        [HttpPost("UPDATE")]
        public ActionResult<Dictionary<string, string>> UPDATE([FromBody]DOCUMENT_CHEKING DOCUMENT_CHEKING)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(APPLICATIONMapping.getmap());
            maplist.Add(DEBITURMapping.getmap());
            maplist.Add(ASSESSMENT_HISTORYMapping.getmap());
            maplist.Add(CERTIFICATEMapping.getmap());
            maplist.Add(APPFLAGMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            ITransaction tran = session.BeginTransaction();
            try
            {
               
              

                tran.Commit();
                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Scan Status");
                vsLogMessage.Add("msg", "Save Sucess!!");
                return vsLogMessage;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                vsLogMessage.Add("errorcode", "500");
                vsLogMessage.Add("title", "Scan Status");
                vsLogMessage.Add("msg", ex.Message);
                return vsLogMessage;
            }

        }

        // GET api/values
        // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("SUSER")]
        public ActionResult<List<SUSER>> SUSER()
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            string query = @"select 
                    usr.USERID
                    ,usr.USERNAME
                    ,gr.GROUPNAME
                    from sec.SCUSER usr
                    join sec.SCGROUP gr on usr.GROUPID = gr.GROUPID";
            List<SUSER> voTable =SessionFactoryBuilder.GetList<SUSER>(connectionStringName, query);
            return voTable;
        }

      
    }
}
