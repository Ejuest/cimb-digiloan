﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AppraisalSystemApi.Models;
using NHibernate;
namespace AppraisalSystemApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RFZIPCODEController : ControllerBase
    {
        IConfiguration _iconfiguration;
        public RFZIPCODEController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        // GET api/values
       // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("Get")]
        public ActionResult<List<RFZIPCODE>> Get()
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(RFZIPCODEMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            List<RFZIPCODE> voTable = session.QueryOver<RFZIPCODE>().List() as List<RFZIPCODE>;
            return voTable;
        }

        [HttpGet("GetByArea")]
        public ActionResult<List<RFZIPCODE>> GetByArea(string Code)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(RFZIPCODEMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            List<RFZIPCODE> voTable = session.QueryOver<RFZIPCODE>().Where(f=>f.ZIP_CODE ==  Code).List() as List<RFZIPCODE>;
            return voTable;
        }


        [HttpPost("Add")]
        public ActionResult<string> Add(RFZIPCODE RFZIPCODE)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(RFZIPCODEMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            ITransaction tran = session.BeginTransaction();
            try
            {
                session.Save(RFZIPCODE);
                tran.Commit();
                return "Add New Sucess!!";
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return ex.Message;
            }

        }

        [HttpPost("Edit")]
        public ActionResult<string> Edit(RFZIPCODE RFZIPCODE)
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            List<NHibernate.Cfg.MappingSchema.HbmMapping> maplist = new List<NHibernate.Cfg.MappingSchema.HbmMapping>();
            maplist.Add(RFZIPCODEMapping.getmap());
            var session = SessionFactoryBuilder.getSesiion(maplist, connectionStringName);
            ITransaction tran = session.BeginTransaction();
            try
            {
                session.Update(RFZIPCODE);
                tran.Commit();
                return "Update Sucess!!";
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return ex.Message;
            }

        }

        // GET api/values
        // [HttpGet("API/[controller]/{apiname}", Name = "Get")]
        [HttpGet("Mandatory")]
        public ActionResult<List<Parameter>> Mandatory()
        {
            string connectionStringName = _iconfiguration.GetConnectionString("Apparsial1");
            string query = @"select col.name as name
                            FROM SYS.COLUMNS col 
                            join sys.types typ on col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id  
                            where object_id = object_id('los." + nameof(RFZIPCODE)+ "') and col.is_nullable =0";
            List<Parameter> voTable =SessionFactoryBuilder.GetList<Parameter>(connectionStringName, query);
            return voTable;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

     

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
