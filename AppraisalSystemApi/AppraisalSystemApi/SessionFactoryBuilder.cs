﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Transform;
using NHibernate.Tool.hbm2ddl;

namespace AppraisalSystemApi
{
    public class SessionFactoryBuilder
    {
        static IDictionary<string, ISession> DicSession = new Dictionary<string, ISession>();
        public static ISession getSesiion(List<NHibernate.Cfg.MappingSchema.HbmMapping> map, string connection)
       {
            var cfg = new NHibernate.Cfg.Configuration();

            cfg.DataBaseIntegration(c =>
            {
                c.Driver<NHibernate.Driver.SqlClientDriver>();
                c.Dialect<NHibernate.Dialect.MsSql2008Dialect>();
               
                c.ConnectionString = connection;

                c.LogFormattedSql = true;
                c.LogSqlInConsole = true;

            });
            string schemaadd = "-";
            if (map != null)
            {
                schemaadd = "";
                foreach (NHibernate.Cfg.MappingSchema.HbmMapping mapping in map)
                {
                    cfg.AddMapping(mapping);
                    NHibernate.Cfg.MappingSchema.HbmClass hbclass = mapping.Items[0] as NHibernate.Cfg.MappingSchema.HbmClass;
                    schemaadd += hbclass.name;
                }
            }
           
            try
            {
                ISession Oldsession = DicSession[schemaadd];
                if (Oldsession != null)
                {
                    try
                    {
                        Oldsession.Close();
                        return Oldsession.SessionFactory.OpenSession();
                    }
                    catch
                    {
                        return Oldsession.SessionFactory.OpenSession();
                    }
                   
                }
                else
                {
                  
                    var sf = cfg.BuildSessionFactory();
                    Oldsession = sf.OpenSession();
                    return Oldsession;
                }
            }
            catch(Exception ex)
            {
               if(DicSession ==null)
                    DicSession = new  Dictionary<string, ISession>();

                var sf = cfg.BuildSessionFactory();
               ISession nsession = sf.OpenSession();
                DicSession.Add(schemaadd, nsession);
                return nsession;
            }


           
        }

        public static List<T> GetList<T>(string connection, string query)
        {
            try
            {
                ISession voISession = getSesiion(null, connection);
                IQuery sqlQuery = voISession.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean(typeof(T))).SetTimeout(150);
                List<T> voTable = sqlQuery.List<T>() as List<T>;

                return voTable;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        public static List<T> GetList<T>(string connection, string query, ISession voISession)
        {
            try
            {
               
                IQuery sqlQuery = voISession.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean(typeof(T))).SetTimeout(150);
                List<T> voTable = sqlQuery.List<T>() as List<T>;

                return voTable;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
