﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class RFZIPCODE
    {
        public virtual string ZIP_CODE { get; set; }
        public virtual string ZIP_DESC { get; set; }
        public virtual string SUBDISTRICT { get; set; }
        public virtual string DISTRICT { get; set; }
        public virtual string CITY { get; set; }
        public virtual string PROVINCE { get; set; }
        public virtual bool? ACTIVE { get; set; }
        public virtual string CREATEBY { get; set; }
        public virtual DateTime? CREATEDATE { get; set; }
        public virtual string UPDATEBY { get; set; }
        public virtual DateTime? UPDATEDATE { get; set; }
    }
    public class RFZIPCODEMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<RFZIPCODE>
    {

        public RFZIPCODEMapping()
        {

            Schema("los");
            Id(x => x.ZIP_CODE, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
           
            Property(x => x.ZIP_DESC);
            Property(x => x.SUBDISTRICT);
            Property(x => x.DISTRICT);
            Property(x => x.CITY);
            Property(x => x.PROVINCE);
            Property(x => x.ACTIVE);
            Property(x => x.CREATEBY);
            Property(x => x.CREATEDATE);
            Property(x => x.UPDATEBY);
            Property(x => x.UPDATEDATE);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<RFZIPCODEMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
