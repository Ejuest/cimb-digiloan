﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public  class ORDER
    {
        public virtual APPLICATION APPLICATION { get; set; }
        public virtual APPFLAG APPFLAG { get; set; }
        public virtual DEBITUR DEBITUR { get; set; }
        public virtual List<ASSESSMENT_HISTORY> ASSESSMENT_HISTORY { get; set; }
        public virtual List<CERTIFICATE> CERTIFICATE { get; set; }
        public virtual List<DEDUPRESULT> DEDUPRESULT { get; set; }

    }

 

}
