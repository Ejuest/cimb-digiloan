﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class RequestOrder
    {
        public string NoPermohonan { get; set; }
        public DateTime? TglPermohonan { get; set; }
        public string JenisPermintaanTaksasi { get; set; }
        public string JenisFasilitas { get; set; }
        public string DesckripsiJaminan { get; set; }
        public string AlamatJaminan { get; set; }
        public string NamaPerumahan { get; set; }
        public string Cluster { get; set; }
        public string KodePos { get; set; }
        public string Kelurahan { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Propinsi { get; set; }
        public bool? Vip { get; set; }
        public string JenisPenilaian { get; set; }
        public DateTime? TglAssigment { get; set; }
        public string Valuer { get; set; }
        public string AssignOleh { get; set; }
        public bool? DistribusiManual { get; set; }
        public string PilihDistribusi { get; set; }
        public string Reason { get; set; }
        public string Remark { get; set; }
        public string NamaPIC1 { get; set; }
        public string NoWaPIC1 { get; set; }
        public string NoTlpPIC1 { get; set; }
        public string NamaPIC2 { get; set; }
        public string NoWaPIC2 { get; set; }
        public string NoTlpPIC2 { get; set; }
        public string Cabang { get; set; }
        public string MarketingOfficer { get; set; }
        public string NoTlpMarketing { get; set; }
        public string Reserve1 { get; set; }
        public string Reserve2 { get; set; }
        public string Reserve3 { get; set; }
        public string Reserve4 { get; set; }
        public string Reserve5 { get; set; }
    }
    public class RequestOrderMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<RequestOrder>
    {

        public RequestOrderMapping()
        {
            Id(x => x.NoPermohonan, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.NoPermohonan);
            Property(x => x.TglPermohonan);
            Property(x => x.JenisPermintaanTaksasi);
            Property(x => x.JenisFasilitas);
            Property(x => x.DesckripsiJaminan);
            Property(x => x.AlamatJaminan);
            Property(x => x.NamaPerumahan);
            Property(x => x.Cluster);
            Property(x => x.KodePos);
            Property(x => x.Kelurahan);
            Property(x => x.Kecamatan);
            Property(x => x.Kabupaten);
            Property(x => x.Propinsi);
            Property(x => x.Vip);
            Property(x => x.JenisPenilaian);
            Property(x => x.TglAssigment);
            Property(x => x.Valuer);
            Property(x => x.AssignOleh);
            Property(x => x.DistribusiManual);
            Property(x => x.PilihDistribusi);
            Property(x => x.Reason);
            Property(x => x.Remark);
            Property(x => x.NamaPIC1);
            Property(x => x.NoWaPIC1);
            Property(x => x.NoTlpPIC1);
            Property(x => x.NamaPIC2);
            Property(x => x.NoWaPIC2);
            Property(x => x.NoTlpPIC2);
            Property(x => x.Cabang);
            Property(x => x.MarketingOfficer);
            Property(x => x.NoTlpMarketing);
            Property(x => x.Reserve1);
            Property(x => x.Reserve2);
            Property(x => x.Reserve3);
            Property(x => x.Reserve4);
            Property(x => x.Reserve5);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<RequestOrderMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }


}
