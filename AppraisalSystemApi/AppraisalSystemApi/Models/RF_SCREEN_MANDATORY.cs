﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class RF_SCREEN_MANDATORY
    {
        public virtual string ID { get; set; }
        public virtual string SCREEN_NAME { get; set; }
        public virtual string FIELD_NAME { get; set; }
        public virtual string STAGE_ID { get; set; }
        public virtual string CREATE_BY { get; set; }
        public virtual DateTime? CREATE_AT { get; set; }
        public virtual string UPDATE_BY { get; set; }
        public virtual DateTime? UPDATE_AT { get; set; }
        public virtual bool? IS_ACTIVE { get; set; }
    }
    public class RF_SCREEN_MANDATORYMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<RF_SCREEN_MANDATORY>
    {

        public RF_SCREEN_MANDATORYMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.SCREEN_NAME);
            Property(x => x.FIELD_NAME);
            Property(x => x.STAGE_ID);
            Property(x => x.CREATE_BY);
            Property(x => x.CREATE_AT);
            Property(x => x.UPDATE_BY);
            Property(x => x.UPDATE_AT);
            Property(x => x.IS_ACTIVE);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<RF_SCREEN_MANDATORYMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
