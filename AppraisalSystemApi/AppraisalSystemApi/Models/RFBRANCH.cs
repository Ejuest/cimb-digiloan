﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class RFBRANCH
    {
        public virtual string BRANCHID { get; set; }
        public virtual string BRANCHNAME { get; set; }
        public virtual string BR_TYPE { get; set; }
        public virtual string AREAID { get; set; }
        public virtual string CCOBRANCH { get; set; }
        public virtual string CORE_CODE { get; set; }
        public virtual bool? ACTIVE { get; set; }
        public virtual string CREATEBY { get; set; }
        public virtual DateTime CREATEDATE { get; set; }
        public virtual string UPDATEBY { get; set; }
        public virtual DateTime? UPDATEDATE { get; set; }
    }
    public class RFBRANCHMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<RFBRANCH>
    {

        public RFBRANCHMapping()
        {
            Schema("los");
            Id(x => x.BRANCHID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.BRANCHNAME);
            Property(x => x.BR_TYPE);
            Property(x => x.AREAID);
            Property(x => x.CCOBRANCH);
            Property(x => x.CORE_CODE);
            Property(x => x.ACTIVE);
            Property(x => x.CREATEBY);
            Property(x => x.CREATEDATE);
            Property(x => x.UPDATEBY);
            Property(x => x.UPDATEDATE);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<RFBRANCHMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
