﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public  class APPLICATION
    {
        public virtual  Guid ID { get; set; }
        public virtual  string APPLICATION_NO { get; set; }
        public virtual  string APPLICATION_COLLID { get; set; }
        public virtual  string ASSESSMENT_TYPE_ID { get; set; }
        public virtual  string CIF_NO { get; set; }
        public virtual  DateTime? DATE_OF_APPLICATION { get; set; }
        public virtual  string REGIONAL { get; set; }
        public virtual  string APPRAISAL_MANAGER { get; set; }
        public virtual  string APPRAISAL_VALUER_KJPP { get; set; }
        public virtual  string REASON_ID { get; set; }
        public virtual  string REMARK { get; set; }
        public virtual  bool? MANUAL_DISTRIBUTION { get; set; }
        public virtual  string DISTRIBUTION_ID { get; set; }
    }


    public class INBOX
    {
    
        public virtual string type { get; set; }
        public virtual string noapplikasi { get; set; }
        public virtual DateTime? tglterima { get; set; }
        public virtual string namadebitur { get; set; }
        public virtual string alamat { get; set; }
        public virtual string typejaminan { get; set; }
        public virtual string namajaminan { get; set; }
        public virtual DateTime? trackdate { get; set; }
        public virtual string aging
        {
            get
            {
                try
                {
                    return getAging(trackdate.Value);
                }
                catch
                {
                    return "";
                }
            }
        }

        public static string getAging(DateTime date)
        {
            DateTime currentdate = DateTime.Now;
            DateTime dob = date;
            TimeSpan ts = currentdate - dob;
            DateTime age = DateTime.MinValue + ts;
            int years = age.Year - 1;
            int months = age.Month - 1;
            int days = age.Day - 2;
            int hours = ts.Hours;
            int minutes = ts.Minutes;
            int seconds = ts.Seconds;

            string sReturn = "";
            string Syear = String.Format("{0} Tahun ", years);
            if (years > 0)
                sReturn += Syear;
            string Smonths = String.Format("{0} Bulan ", months);
            if (months > 0)
                sReturn += Smonths;
            string Sdays = String.Format("{0} Hari ", days);
            if (days > 0)
                sReturn += Sdays;
            string Shours = String.Format("{0} Jam ", hours);
            if (hours > 0)
                sReturn += Shours;
            string Sminutes = String.Format("{0} Menit ", minutes);
            if (minutes > 0)
                sReturn += Sminutes;

            return sReturn;
        }

    }

    public  class APPLICATIONMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<APPLICATION>
    {

        public  APPLICATIONMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });

            Property(x => x.APPLICATION_NO);
            Property(x => x.APPLICATION_COLLID);
            Property(x => x.ASSESSMENT_TYPE_ID);
            Property(x => x.CIF_NO);
            Property(x => x.DATE_OF_APPLICATION);
            Property(x => x.REGIONAL);
            Property(x => x.APPRAISAL_MANAGER);
            Property(x => x.APPRAISAL_VALUER_KJPP);
            Property(x => x.REASON_ID);
            Property(x => x.REMARK);
            Property(x => x.MANUAL_DISTRIBUTION);
            Property(x => x.DISTRIBUTION_ID);
        }

        public  static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<APPLICATIONMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
