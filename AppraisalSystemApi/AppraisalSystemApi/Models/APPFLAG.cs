﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class APPFLAG
    {
        public virtual string AP_REGNO { get; set; }
        public virtual string FLAG_APPRAISAL_ID { get; set; }
        public virtual string AP_CURRTRCODE { get; set; }
        public virtual string AP_NEXTTRBY { get; set; }
        public virtual DateTime? AP_LASTTRDATE { get; set; }
        public virtual string AP_LASTTRCODE { get; set; }
        public virtual string AP_LASTTRBY { get; set; }
        public virtual bool? AP_VVIP { get; set; }
        public virtual bool? AP_REJECTED { get; set; }
        public virtual bool? AP_CANCELED { get; set; }
        public virtual bool? AP_APPROVED { get; set; }
        public virtual bool? AP_BOOKED { get; set; }
        public virtual string CREATEBY { get; set; }
        public virtual DateTime CREATEDATE { get; set; }
        public virtual string UPDATEBY { get; set; }
        public virtual DateTime? UPDATEDATE { get; set; }
    }
    public class APPFLAGMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<APPFLAG>
    {

        public APPFLAGMapping()
        {
            Schema("flow");
            Id(x => x.AP_REGNO, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
           
            Property(x => x.FLAG_APPRAISAL_ID);
            Property(x => x.AP_CURRTRCODE);
            Property(x => x.AP_NEXTTRBY);
            Property(x => x.AP_LASTTRDATE);
            Property(x => x.AP_LASTTRCODE);
            Property(x => x.AP_LASTTRBY);
            Property(x => x.AP_VVIP);
            Property(x => x.AP_REJECTED);
            Property(x => x.AP_CANCELED);
            Property(x => x.AP_APPROVED);
            Property(x => x.AP_BOOKED);
            Property(x => x.CREATEBY);
            Property(x => x.CREATEDATE);
            Property(x => x.UPDATEBY);
            Property(x => x.UPDATEDATE);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<APPFLAGMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }


}
