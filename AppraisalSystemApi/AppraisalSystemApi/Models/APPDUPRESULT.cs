﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class APPDUPRESULT
    {
        public virtual  string SOURCEKEY { get; set; }
        public virtual  string DUPCATCODE { get; set; }
        public virtual  string DUPCODE { get; set; }
        public virtual  string DUPKEYS { get; set; }
        public virtual  string CREATEBY { get; set; }
        public virtual  DateTime CREATEDATE { get; set; }
    }
    public class APPDUPRESULTMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<APPDUPRESULT>
    {

        public APPDUPRESULTMapping()
        {
            Schema("los");
            ComposedId(map => {
                map.Property(x => x.SOURCEKEY);
                map.Property(x => x.DUPCATCODE);
                map.Property(x => x.DUPCODE);
                map.Property(x => x.DUPKEYS);
            });
          
            Property(x => x.CREATEBY);
            Property(x => x.CREATEDATE);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<APPDUPRESULTMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
