﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class DOCUMENT_DETAIL_UPLOAD
    {
        public virtual Guid ID { get; set; }
        public virtual Guid? DOCUMENT_DETAIL_ID { get; set; }
        public virtual byte[] FILE_UPLOAD { get; set; }
    }
    public class DOCUMENT_DETAIL_UPLOADMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<DOCUMENT_DETAIL_UPLOAD>
    {

        public DOCUMENT_DETAIL_UPLOADMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.DOCUMENT_DETAIL_ID);
            Property(x => x.FILE_UPLOAD);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<DOCUMENT_DETAIL_UPLOADMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
