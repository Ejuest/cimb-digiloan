﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class RF_DISTRIBUTION
    {
        public virtual string ID { get; set; }
        public virtual string DISTRIBUTION_BY_SYSTEM_ID { get; set; }
        public virtual string GROUPID_ID { get; set; }
        public virtual string ZIPCODE_ID { get; set; }
        public virtual string REGIONAL_ID { get; set; }
        public virtual string CREATE_BY { get; set; }
        public virtual DateTime? CREATE_AT { get; set; }
        public virtual string UPDATE_BY { get; set; }
        public virtual DateTime? UPDATE_AT { get; set; }
        public virtual bool? IS_ACTIVE { get; set; }
    }
    public class RF_DISTRIBUTIONMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<RF_DISTRIBUTION>
    {

        public RF_DISTRIBUTIONMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.DISTRIBUTION_BY_SYSTEM_ID);
            Property(x => x.GROUPID_ID);
            Property(x => x.ZIPCODE_ID);
            Property(x => x.REGIONAL_ID);
            Property(x => x.CREATE_BY);
            Property(x => x.CREATE_AT);
            Property(x => x.UPDATE_BY);
            Property(x => x.UPDATE_AT);
            Property(x => x.IS_ACTIVE);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<RF_DISTRIBUTIONMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
