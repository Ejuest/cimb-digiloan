﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class DEBITUR
    {
        public virtual Guid ID { get; set; }
        public virtual Guid APPLICATION_ID { get; set; }
        public virtual string TEMPLATE_ID { get; set; }
        public virtual string CUSTOMER_NAME { get; set; }
        public virtual string GUARANTEE_ID { get; set; }
        public virtual string FACILITY_ID { get; set; }
        public virtual float? EXPOSURE_VALUE { get; set; }
        public virtual string TYPE_OF_GUARANTEE_ID { get; set; }
        public virtual string DESCRIPTION_OF_GUARANTEE { get; set; }
        public virtual string ADDRESS_OF_GUARANTEE_1 { get; set; }
        public virtual string ADDRESS_OF_GUARANTEE_2 { get; set; }
        public virtual string ADDRESS_OF_GUARANTEE_3 { get; set; }
        public virtual string ZIP_CODE_ID { get; set; }
        public virtual string CITY_ID { get; set; }
        public virtual string SUBDISTRICT_ID { get; set; }
        public virtual string DISTRICT_ID { get; set; }
        public virtual string PROVINCE_ID { get; set; }
        public virtual bool? VIP { get; set; }
        public virtual string PIC_1_NAME { get; set; }
        public virtual string NO_WA_PIC_1 { get; set; }
        public virtual string PHONE_NUMBER_PIC_1 { get; set; }
        public virtual string PIC_2_NAME { get; set; }
        public virtual string NO_WA_PIC_2 { get; set; }
        public virtual string PIC_2_TEL { get; set; }
        public virtual string BRANCH { get; set; }
        public virtual string MARKETING_OFFICER { get; set; }
        public virtual string NO_TEL_MARKETING { get; set; }
        public virtual string RESERVE_1 { get; set; }
        public virtual string RESERVE_2 { get; set; }
        public virtual string RESERVE_3 { get; set; }
        public virtual string RESERVE_4 { get; set; }
        public virtual string RESERVE_5 { get; set; }
        public virtual string DEVELOPER { get; set; }
        public virtual string PROJECTNAME { get; set; }
    }
    public class DEBITURMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<DEBITUR>
    {

        public DEBITURMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.APPLICATION_ID);
            Property(x => x.TEMPLATE_ID);
            Property(x => x.CUSTOMER_NAME);
            Property(x => x.GUARANTEE_ID);
            Property(x => x.FACILITY_ID);
            Property(x => x.EXPOSURE_VALUE);
            Property(x => x.TYPE_OF_GUARANTEE_ID);
            Property(x => x.DESCRIPTION_OF_GUARANTEE);
            Property(x => x.ADDRESS_OF_GUARANTEE_1);
            Property(x => x.ADDRESS_OF_GUARANTEE_2);
            Property(x => x.ADDRESS_OF_GUARANTEE_3);
            Property(x => x.ZIP_CODE_ID);
            Property(x => x.CITY_ID);
            Property(x => x.SUBDISTRICT_ID);
            Property(x => x.DISTRICT_ID);
            Property(x => x.PROVINCE_ID);
            Property(x => x.VIP);
            Property(x => x.PIC_1_NAME);
            Property(x => x.NO_WA_PIC_1);
            Property(x => x.PHONE_NUMBER_PIC_1);
            Property(x => x.PIC_2_NAME);
            Property(x => x.NO_WA_PIC_2);
            Property(x => x.PIC_2_TEL);
            Property(x => x.BRANCH);
            Property(x => x.MARKETING_OFFICER);
            Property(x => x.NO_TEL_MARKETING);
            Property(x => x.RESERVE_1);
            Property(x => x.RESERVE_2);
            Property(x => x.RESERVE_3);
            Property(x => x.RESERVE_4);
            Property(x => x.RESERVE_5);
            Property(x => x.DEVELOPER);
            Property(x => x.PROJECTNAME);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<DEBITURMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
