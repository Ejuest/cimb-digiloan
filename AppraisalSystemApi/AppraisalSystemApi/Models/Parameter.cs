﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class Parameter
    {
        public virtual string name { get; set; }
        public virtual string value { get; set; }
    }
}
