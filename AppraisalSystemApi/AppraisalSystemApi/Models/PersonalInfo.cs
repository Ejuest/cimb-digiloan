﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
 

namespace AppraisalSystemApi
{
    public class PersonalInfo
    {
        public virtual long ID { get; set; }
        [Required(ErrorMessage = "First Name is required.")]
        [DisplayName("First Name")]
        public virtual string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required.")]
        [DisplayName("Last Name")]
        public virtual string LastName { get; set; }
        [DisplayName("Date Of Birth")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public virtual Nullable<DateTime> DateOfBirth { get; set; }
        public virtual string City { get; set; }
        public virtual string Country { get; set; }
        [DisplayName("Mobile No")]
        public virtual string MobileNo { get; set; }
        public virtual string NID { get; set; }
        [EmailAddress]
        public virtual string Email { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual DateTime? LastModifiedDate { get; set; }
        public virtual string CreationUser { get; set; }
        public virtual string LastUpdateUser { get; set; }
        public virtual byte Status { get; set; }
    }

    public class PersonalInfoMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<PersonalInfo>
    {
        public PersonalInfoMapping()
        {
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Identity
                    , generatorMapping => generatorMapping.Params(new { sequence = "Personal_id_seq" })
                );
            });

            Property(x => x.FirstName);
            Property(x => x.LastName);
            Property(x => x.DateOfBirth);
            Property(x => x.City);
            Property(x => x.Country);
            Property(x => x.MobileNo);
            Property(x => x.NID);
            Property(x => x.Email);
            Property(x => x.CreatedDate);
            Property(x => x.LastModifiedDate);
            Property(x => x.CreationUser);
            Property(x => x.LastUpdateUser);
            Property(x => x.Status);

        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<PersonalInfoMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }
}
