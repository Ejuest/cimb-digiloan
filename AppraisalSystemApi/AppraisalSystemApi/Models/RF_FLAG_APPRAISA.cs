﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class RF_FLAG_APPRAISAL
    {
        public virtual string ID { get; set; }
        public virtual string NAME { get; set; }
        public virtual bool? ACTIVE { get; set; }
        public virtual string CREATE_BY { get; set; }
        public virtual DateTime? CREATE_AT { get; set; }
        public virtual string UPDATE_BY { get; set; }
        public virtual DateTime? UPDATE_DATE { get; set; }
    }
    public class RF_FLAG_APPRAISALMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<RF_FLAG_APPRAISAL>
    {

        public RF_FLAG_APPRAISALMapping()
        {
            Schema("flow");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.NAME);
            Property(x => x.ACTIVE);
            Property(x => x.CREATE_BY);
            Property(x => x.CREATE_AT);
            Property(x => x.UPDATE_BY);
            Property(x => x.UPDATE_DATE);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<RF_FLAG_APPRAISALMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
