﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class DokumenRequestOrder
    {
        public string NoPermohonan { get; set; }
        public string JenisDokument { get; set; }
        public string NoDokument { get; set; }
        public string Nib { get; set; }
        public string AtasNama { get; set; }
    }
    public class DokumenRequestOrderMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<DokumenRequestOrder>
    {

        public DokumenRequestOrderMapping()
        {

            ComposedId(map => {
                map.Property(x => x.NoPermohonan);
                map.Property(x => x.JenisDokument);
                map.Property(x => x.NoDokument);
            });
            Property(x => x.NoPermohonan);
            Property(x => x.JenisDokument);
            Property(x => x.NoDokument);
            Property(x => x.Nib);
            Property(x => x.AtasNama);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<DokumenRequestOrderMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }


}
