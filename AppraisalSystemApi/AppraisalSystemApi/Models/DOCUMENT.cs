﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class DOCUMENT
    {
        public virtual Guid ID { get; set; }
        public virtual Guid APPLICATION_ID { get; set; }
        public virtual string REASON_ID { get; set; }
        public virtual string REMARK { get; set; }
    }
    public class DOCUMENTMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<DOCUMENT>
    {

        public DOCUMENTMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.APPLICATION_ID);
            Property(x => x.REASON_ID);
            Property(x => x.REMARK);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<DOCUMENTMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
