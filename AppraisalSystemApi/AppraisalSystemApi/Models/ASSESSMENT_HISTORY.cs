﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class ASSESSMENT_HISTORY
    {
        public virtual Guid ID { get; set; }
        public virtual Guid? APPLICATION_ID { get; set; }
        public virtual string ASSESSMENT_TYPE_ID { get; set; }
        public virtual DateTime? ASSIG_DATE { get; set; }
        public virtual string VALUER { get; set; }
        public virtual string ASSIG_BY { get; set; }
    }
    public class ASSESSMENT_HISTORYMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<ASSESSMENT_HISTORY>
    {

        public ASSESSMENT_HISTORYMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.APPLICATION_ID);
            Property(x => x.ASSESSMENT_TYPE_ID);
            Property(x => x.ASSIG_DATE);
            Property(x => x.VALUER);
            Property(x => x.ASSIG_BY);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<ASSESSMENT_HISTORYMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
