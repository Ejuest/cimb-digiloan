﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class DEDUPRESULT
    {
        public virtual string Kriteria { get; set; }
        public virtual string Matchlevel { get; set; }
        public virtual string Nama { get; set; }
        public virtual string NoReferensi { get; set; }
        public virtual string Status { get; set; }
        public virtual string Cabang { get; set; }
        public virtual bool Linked { get; set; }
    }
}
