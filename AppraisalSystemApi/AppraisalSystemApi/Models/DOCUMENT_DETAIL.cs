﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class DOCUMENT_DETAIL
    {
        public virtual Guid ID { get; set; }
        public virtual Guid APPLICATION_ID { get; set; }
        public virtual Guid DOCUMENT_DATA_ID { get; set; }
        public virtual string DOCUMENT_ID { get; set; }
        public virtual bool? VALID { get; set; }
        public virtual bool? MANDATORY { get; set; }
        public virtual DateTime? UPLOAD_AT { get; set; }
    }
    public class DOCUMENT_DETAILMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<DOCUMENT_DETAIL>
    {

        public DOCUMENT_DETAILMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.APPLICATION_ID);
            Property(x => x.DOCUMENT_DATA_ID);
            Property(x => x.DOCUMENT_ID);
            Property(x => x.VALID);
            Property(x => x.MANDATORY);
            Property(x => x.UPLOAD_AT);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<DOCUMENT_DETAILMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
