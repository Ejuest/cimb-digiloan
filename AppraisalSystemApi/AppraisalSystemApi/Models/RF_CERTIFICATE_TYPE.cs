﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class RF_CERTIFICATE_TYPE
    {
        public virtual string ID { get; set; }
        public virtual string NAME { get; set; }
        public virtual string CREATE_BY { get; set; }
        public virtual DateTime? CREATE_AT { get; set; }
        public virtual string UPDATE_BY { get; set; }
        public virtual DateTime? UPDATE_AT { get; set; }
        public virtual bool? IS_ACTIVE { get; set; }
    }
    public class RF_CERTIFICATE_TYPEMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<RF_CERTIFICATE_TYPE>
    {

        public RF_CERTIFICATE_TYPEMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
          
            Property(x => x.NAME);
            Property(x => x.CREATE_BY);
            Property(x => x.CREATE_AT);
            Property(x => x.UPDATE_BY);
            Property(x => x.UPDATE_AT);
            Property(x => x.IS_ACTIVE);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<RF_CERTIFICATE_TYPEMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
