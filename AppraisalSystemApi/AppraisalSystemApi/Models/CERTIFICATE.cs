﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppraisalSystemApi.Models
{
    public class CERTIFICATE
    {
        public virtual Guid ID { get; set; }
        public virtual Guid APPLICATION_ID { get; set; }
        public virtual string CERTIFICATE_TYPE_ID { get; set; }
        public virtual string CERTIFICATE_NO { get; set; }
        public virtual string NIB { get; set; }
        public virtual string NAME_OF_HOLDERS { get; set; }
        public virtual float? LAND_AREA_M2 { get; set; }
        public virtual DateTime? EXP_DATE { get; set; }
        public virtual DateTime? CERT_DATE { get; set; }
        public virtual string GS_SU_NO { get; set; }
        public virtual DateTime? GS_SU_DATE { get; set; }
        public virtual string REL_WITH_CUST { get; set; }
    }
    public class CERTIFICATEMapping : NHibernate.Mapping.ByCode.Conformist.ClassMapping<CERTIFICATE>
    {

        public CERTIFICATEMapping()
        {
            Schema("los");
            Id(x => x.ID, id =>
            {
                id.Generator(
                    NHibernate.Mapping.ByCode.Generators.Assigned
                );
            });
            Property(x => x.APPLICATION_ID);
            Property(x => x.CERTIFICATE_TYPE_ID);
            Property(x => x.CERTIFICATE_NO);
            Property(x => x.NIB);
            Property(x => x.NAME_OF_HOLDERS);
            Property(x => x.LAND_AREA_M2);
            Property(x => x.EXP_DATE);
            Property(x => x.CERT_DATE);
            Property(x => x.GS_SU_NO);
            Property(x => x.GS_SU_DATE);
            Property(x => x.REL_WITH_CUST);
        }

        public static NHibernate.Cfg.MappingSchema.HbmMapping getmap()
        {
            var mapper = new NHibernate.Mapping.ByCode.ModelMapper();
            mapper.AddMapping<CERTIFICATEMapping>();
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }

}
