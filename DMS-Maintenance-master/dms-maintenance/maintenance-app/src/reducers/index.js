/**
 * App Reducers
 */
import { combineReducers } from 'redux';
import settings from './settings';
import sidebarReducer from './SidebarReducer';
import authUserReducer from './AuthUserReducer';
import CertificateReducer from  './CertificateReducer';
import DedupReducer from  './DedupReducer';
const reducers = combineReducers({
   settings,
   sidebar: sidebarReducer,
   authUser: authUserReducer,
   CertificateReducer: CertificateReducer,
   DedupReducer: DedupReducer,
});

export default reducers;
