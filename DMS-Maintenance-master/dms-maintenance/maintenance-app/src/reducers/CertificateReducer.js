 
//action types
import {
    ADD,
    DELETE,
    UPDATE
 } from '../actions/types';
 
 const INIT_STATE = {
    dataCert :JSON.stringify(localStorage.getItem('dataCert'))
};
 
 export default (state =INIT_STATE, action) => {
    switch (action.type) {
       // add product to cart 
       case ADD:
          let client = action.payload;
        //   return {
        //      ...state,
        //      dataCert: [...state.dataCert, client]
             
        // }
           let newdata= [
          
                  client['DokType'], 
                 client['NoDokumen'],
                 client['NIB'],
                 client['AtasNama'],
                client['NoDokumen'],
             
          ];
          state.push(newdata);
          return state;
       // remove client to cart	
       case DELETE:
          let removeClient = action.payload;
          let newData = state.dataCert.filter((clientItem) => clientItem[1] !== removeClient[1])
          return {
             ...state,
             dataCert: newData
          }
       // update client
       case UPDATE:
          let updateClient = action.payload;
          let newclientsData = [];
          for (const item of state.clientsData) {
             if (item.id === updateClient.ID) {
                item.name = updateClient.data.name;
                item.e_mail = updateClient.data.email;
                item.phone_number = updateClient.data.phoneNumber;
                item.country = updateClient.data.location;
             }
             newclientsData.push(item)
          }
          return {
             ...state,
             clientsData: newclientsData
          }
       // default case	
       default:
          return { ...state }
    }
 }