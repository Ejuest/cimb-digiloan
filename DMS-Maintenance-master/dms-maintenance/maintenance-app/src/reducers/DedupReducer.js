 
//action types
import {
    ADD,
    DELETE,
    UPDATE
 } from '../actions/types';
 
 
 
 export default (state =[] , action) => {
    switch (action.type) {
       // add product to cart 
       case ADD:
          let client = action.payload;
        //   return {
        //      ...state,
        //      dataCert: [...state.dataCert, client]
             
        // }
          return [
            ...state,
            {
                "Type Sertifikat" : client['DokType'], 
                "No Sertifikat" :client['NoDokumen'],
                "NIB" :client['NIB'],
                "Nama Pemegang Hak":client['AtasNama'],
                "Action" :client['NoDokumen'],
            }
          ];
          

       // remove client to cart	
       case DELETE:
          let removeClient = action.payload;
          let newData = state.dataCert.filter((clientItem) => clientItem[1] !== removeClient[1])
          return {
             ...state,
             dataCert: newData
          }
       // update client
       case UPDATE:
          let updateClient = action.payload;
          let newclientsData = [];
          for (const item of state.clientsData) {
             if (item.id === updateClient.ID) {
                item.name = updateClient.data.name;
                item.e_mail = updateClient.data.email;
                item.phone_number = updateClient.data.phoneNumber;
                item.country = updateClient.data.location;
             }
             newclientsData.push(item)
          }
          return {
             ...state,
             clientsData: newclientsData
          }
       // default case	
       default:
          return { ...state }
    }
 }