import axios from 'axios';

const BASE_URL_API = 'http://43.231.129.198/';
const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';
// const PROXY_URL = '';
const TIMEOUT = null;
const CLIENT_ID = '7485F852-624B-4D27-8A9F-E0BB21BEBAC9';
const DEBUG_ADDRESS = '128.199.253.87';
export const getTokenApi = () => {
   return axios.create({
      baseURL: PROXY_URL+BASE_URL_API,
      timeout: TIMEOUT,
      headers: {
         'Content-Type': 'application/json',
         'X-CIMB-DebugAddress': DEBUG_ADDRESS
      }
   });
}

export const getApi = (token) => {
   return axios.create({
      baseURL: PROXY_URL+BASE_URL_API,
      timeout: TIMEOUT,
      headers: {
         'X-CIMB-ClientId': CLIENT_ID,
         'X-CIMB-DebugAddress': DEBUG_ADDRESS,
         'X-CIMB-Signature': token.signature,
         'X-CIMB-Timestamp': token.requestDate
      }
   });
}

export const getApiCustom = () => {
   return axios.create({
      baseURL: "http://localhost:54203/weatherforecast",
      timeout: TIMEOUT,
      
      // headers: {
      //    'X-CIMB-ClientId': CLIENT_ID,
      //    'X-CIMB-DebugAddress': DEBUG_ADDRESS,
      //    // 'X-CIMB-Signature': token.signature,
      //    // 'X-CIMB-Timestamp': token.requestDate
      // }
   });
}

export const AparsialApi = () => {
   return axios.create({
      baseURL: "https://localhost:44382/api/",
      timeout: TIMEOUT,
      
      // headers: {
      //    'X-CIMB-ClientId': CLIENT_ID,
      //    'X-CIMB-DebugAddress': DEBUG_ADDRESS,
      //    // 'X-CIMB-Signature': token.signature,
      //    // 'X-CIMB-Timestamp': token.requestDate
      // }
   });
}

export default
   axios.create({
      baseURL: 'https://reactify.theironnetwork.org/data/',
      timeout: null
   });