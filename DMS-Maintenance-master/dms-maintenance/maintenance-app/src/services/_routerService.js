// routes
import Widgets from 'Routes/widgets';
import Dashboard from 'Routes/dashboard';
import Main from 'Routes/main';

export default [
   {
      path: 'dashboard',
      component: Dashboard
   },
   {
      path: 'widgets',
      component: Widgets
   },
   {
      path: 'main',
      component: Main
   }
]