/**
 * App Info Theme
 */
import { createMuiTheme } from '@material-ui/core/styles';
import AppConfig from 'Constants/AppConfig';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: AppConfig.themeColors.info
        },
        secondary: {
            main: AppConfig.themeColors.primary
        }
    },
    overrides: {
        MUIDataTableHeadCell: {
            fixedHeaderCommon: {
                backgroundColor: AppConfig.themeColors.greyLighten,
                zIndex: 1
            }
        }
    },
});

export default theme;