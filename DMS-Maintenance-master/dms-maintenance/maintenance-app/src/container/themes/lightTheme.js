/**
 * App Light Theme
 */
import { createMuiTheme } from '@material-ui/core/styles';
import AppConfig from 'Constants/AppConfig';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: AppConfig.themeColors.primary
    },
    secondary: {
      main: AppConfig.themeColors.warning
    }
  },
  overrides: {
    MUIDataTableHeadCell: {
      fixedHeaderCommon: {
        backgroundColor: AppConfig.themeColors.greyLighten,
        zIndex: 1
      }
    }
  },
});

export default theme;