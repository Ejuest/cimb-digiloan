/**
 * App Success Theme
 */
import { createMuiTheme } from '@material-ui/core/styles';
import AppConfig from 'Constants/AppConfig';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: AppConfig.themeColors.success
        },
        secondary: {
            main: AppConfig.themeColors.primary
        }
    },
    overrides: {
        MUIDataTableHeadCell: {
            fixedHeaderCommon: {
                backgroundColor: AppConfig.themeColors.greyLighten,
                zIndex: 1
            }
        }
    },
});

export default theme;