/**
 * Miscellaneous Widgets Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { upperCase } from 'lodash';
import {
	CustomCardWidget
} from "Components/Widgets";

// api
import { getApi } from 'Api';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { NotificationManager } from 'react-notifications';
import { Alert } from 'reactstrap';

class ListParameterDedup extends Component {

	state = {
		listTables: [
			"EnumDupCategory",
			"EnumDupOperator",
			"EnumDupType",
			"MasterDupCleansing",
			"MasterDupCriteria",
			"MasterDupCriteriaDetail",
		],
		loading: true,
		alertVisible: false,
		alertMessage: "",
		linkPage: "", 
		namePage: "",
	}
	
	componentDidMount(){
		let { linkPage } = this.props;
		let namePage = null;  
		switch(linkPage){
			case 'existing-parameter':
				linkPage = "parameter-dedup-maker/"+linkPage;
				namePage="Existing Parameter";
				break;
			case 'pending-approval':
				linkPage = "parameter-dedup-maker/"+linkPage;
				namePage="Pending Approval";
				break;
			case 'approval':
				linkPage = "parameter-dedup-approval/"+linkPage;
				namePage="Approval";
				break;
		}
		this.setState({
			loading: false,
			linkPage,
			namePage
		});
	}

	render() {
		const { listTables, loading, linkPage, namePage } = this.state;
		return (
			<div className="general-widgets-wrapper">
				<PageTitleBar title={`Parameter Dedup - ${namePage}`} match={this.props.match} />
				<div className="dash-cards">
					<div className="row">
					{loading &&
						<RctSectionLoader />
					}
					{listTables.length > 0 ? 
						listTables.map((table, key) => (
							<div key = {key} className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
								<CustomCardWidget
									text = {upperCase(table)+'\u00A0'} 
									bgColor="#FDFEFF"
									to={`/app/main/${linkPage}/${table}`} />
							</div>
						))
						: ""
					}
					</div>
				</div>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const token = JSON.parse(authUser.token);
	return { token };
}

export default connect(mapStateToProps)(ListParameterDedup);