import React from "react";
import { connect } from 'react-redux';
import { getApi } from 'Api';
import { NotificationManager } from 'react-notifications';
import {
	Select,
	TextField,
	Checkbox,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	InputLabel,
	MenuItem,
	Button,
} from "@material-ui/core";
import { Alert } from 'reactstrap';

import { getNotifTimeout } from "Helpers/helpers";

// page title bar
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";

// rct card box
import RctCollapsibleCard from "Components/RctCollapsibleCard/RctCollapsibleCard";

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// redux action
import { logoutUser } from 'Actions';

class AddEnumDupOperator extends React.Component {
	state = {
		formData: {
			id: "",
			description: "",
		},
		formError: {
			id: false,
			description: false,
		},
		isEdit: false,
		loading: true,
		alertVisible: false,
		alertMessage: "",
	};

	async componentDidMount() {
		const { token } = this.props;
		const { id } = this.props.match.params;
		if (id) {
			this.setState({ isEdit: true });
			const reqData = {
				operatorId: id
			}
			try {
				const res = await getApi(token).post('CIMB-Appraisal-Dedup/api/json/reply/GetEnumDupOperatorById', reqData);
				const operator = res.data.operator;
				if (operator) {
					// console.log(res.data);
					if (res.data.responseCode == "00"){
						this.setState({
							formData: {
								id: operator.id,
								description: operator.description,
								createBy: operator.createBy,
								createDate: operator.createDate
							},
							loading: false,
						});
					}else{
						console.error("error GetEnumDupOperatorById", res.data.responseDesc);
					}
				} else {
					//Operator not found
					this.props.history.goBack();
				}
			} catch (err) {
				console.error("error GetEnumDupOperatorById", err);
				const { formError } = this.state;
				formError.save = true;
				this.setState({
					loading: false,
					formError
				}, this.onShowAlert(err.message));
				if (err.response && err.response.status == 401) {
					this.props.logoutUser(true);
				}
			}
		}else{
			this.setState({loading: false});
		}
	}
	handleChange = ({ target }) => {
		const { formData } = this.state;
		const { checked, name, type, value } = target;
		const val = (type === 'checkbox') ? checked : value;
		formData[name] = val;
		this.setState({ formData });
	};

	isCanSave = () => {
		const { formData } = this.state;

		this.setState({
			formError: {
				id: formData.id == '',
				description: formData.description == '',
			}
		})

		return (
			formData.id !== '' &&
			formData.description !== ''
		)
	}

	handleSave = e => {
		e.preventDefault();
		if (this.isCanSave()) {
			this.setState({ loading: true });
			const { formData, isEdit } = this.state;
			if(!isEdit){
				formData.createBy = this.props.user.userId;
			}
			formData.updateBy = this.props.user.userId;
			// console.log('formData', formData);
			getApi(this.props.token).post("/CIMB-Appraisal-Dedup/api/json/reply/SaveOrUpdateEnumDupOperator", formData)
				.then((res) => {
					if (res.data.responseCode == "00") {
						this.setState({ loading: false });
						this.props.history.push("/app/main/parameter-dedup-maker/pending-approval/EnumDupOperator");
						NotificationManager.success('Data Created Successfully', "Success", getNotifTimeout());
					} else {
						this.setState({ loading: false });
						NotificationManager.error('Failed to Create Data. ' + res.data.responseDesc, "Error", getNotifTimeout());
					}
				}).catch(err => {
					console.error("error SaveOrUpdateEnumDupOperator", err);
					this.setState({ loading: false });
					NotificationManager.error('Failed to Create Data.', "Error", getNotifTimeout());
				});
		}
	}

	handleBack = e => {
		e.preventDefault();
		this.props.history.goBack();
	}

	onShowAlert = (message) => {
		// console.log("message", message);
		this.setState({
			alertVisible: true,
			alertMessage: message
		});
	}

	onDismissAlert = (key) => {
		this.setState({ [key]: false });
	}

	render() {
		const { formData, formError, isEdit, loading, alertVisible, alertMessage } = this.state;
		return (
			<div className="textfields-wrapper">
				<PageTitleBar
					title="Parameter Dedup - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard>
					{loading &&
						<RctSectionLoader />
					}
					<Alert color="danger" isOpen={alertVisible} toggle={() => this.onAlertDismiss("alertVisible")}>
						<b>Error! </b>
						{alertMessage}
					</Alert>
					<div className="row d-flex justify-content-between py-10 px-10">
						<div>
							<h2>{isEdit ? "Edit EnumDupOperator" : "New EnumDupOperator"}</h2>
						</div>
						<div>
							<Button onClick={this.handleSave}
								disabled={formError.save}
								variant="contained"
								className={`btn-primary mr-10 mb-10 btn-icon ${formError.save ? "" : "text-white"}`}
							><i className="zmdi zmdi-check"></i> Save</Button>
							<Button onClick={this.handleBack} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Cancel</Button>
						</div>
					</div>
					<form noValidate autoComplete="off">
						<div className="row">
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								<div className="form-group">
									<TextField
										required
										disabled={isEdit}
										id="id"
										name="id"
										fullWidth
										label="Operator ID"
										value={formData.id}
										error={formError.id}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="description"
										name="description"
										fullWidth
										label="Description"
										value={formData.description}
										error={formError.description}
										onChange={this.handleChange}
									/>
								</div>
							</div>
						</div>
					</form>

				</RctCollapsibleCard>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps, {
	logoutUser
})(AddEnumDupOperator);