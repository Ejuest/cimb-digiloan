/**
 * Parameter Dedup Maker Page
 */
import React, { Component } from 'react';
import {
	CustomCardWidget
} from "Components/Widgets";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

export default class ParameterDedupMaker extends Component {
	render() {
		return (
			<div className="general-widgets-wrapper">
				<PageTitleBar title="Parameter Dedup Maker" match={this.props.match} />
				<div className="dash-cards">
					<div className="row">
						<div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
							<CustomCardWidget
								text="EXISTING PARAMETER"
								bgColor="#E5BE1D"
								to="/app/main/parameter-dedup-maker/existing-parameter" />
						</div>
						<div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
							<CustomCardWidget
								text="PENDING APPROVAL&nbsp;"
								bgColor="#01E7B9"
								to="/app/main/parameter-dedup-maker/pending-approval" />
						</div>
					</div>
				</div>
			</div>
		);
	}
}
