/**
 * User Management Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { startCase } from 'lodash';

// Notification
import { NotificationManager } from 'react-notifications';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// mui-data-table custom toolbar
import CustomToolbar from "Components/MUIDataTable/CustomToolbar";

class ExistingParameterDedup extends Component {

	state = {
		rawData: [],
		data: [],
		tableName: "",
		selectedData: null, // selected data to perform operations
		loading: true, // loading activity,
		rowHeader: []
	}

	componentDidMount = () => {
		const token = this.props.token;
		const { tableName } = this.props.match.params;
		getApi(token).post(`CIMB-Appraisal-Dedup/api/json/reply/Get${tableName}Maker`)
			.then(res => {
				// console.log("data", res.data);
				const data = [];
				const rowHeader = [];
				let contents = [];
				switch (tableName) {
					case 'EnumDupCategory':
						contents = res.data.dupCategory;
					break;
					case 'EnumDupOperator':
						contents = res.data.dupOperator;
					break;
					case 'EnumDupType':
						contents = res.data.dupType;
					break;
					case 'MasterDupCleansing':
						contents = res.data.dupCleansing;
					break;
					case 'MasterDupCriteria':
						contents = res.data.dupCriteria;
					break;
					case 'MasterDupCriteriaDetail':
						contents = res.data.dupCriteriaDetail;
					break;
				}
				// console.log("content:", contents);
				if (contents && contents.length !== 0) {
					Object.keys(contents[0]).forEach(function (key) {
						if (key != "__type" && (typeof contents[0][key] !== 'object' || contents[0][key] == null)) {
							rowHeader.push(startCase(key));
						}
					});
					//regex for checking ASP DATE
					const regexASPDate = /^\/Date\((\d+)(?:-(\d+))?\)/;

					contents.forEach((content, idx) => {
						const newData = [];
						Object.keys(content).forEach(function (key) {
							// console.log(key, content[key]);
							// console.log(content[key], typeof content[key]);
							if (key !== "__type") {
								if (typeof content[key] === 'boolean') {
									newData.push(content[key] ? "Yes" : "No");
								} else if (typeof content[key] === 'number') {
									newData.push(content[key]);
								} else if (content[key] == null) {
									newData.push("-");
								} else if (typeof content[key] === 'string') {
									regexASPDate.test(content[key]) ?
										newData.push(convertDateASP(content[key], "DD-MM-YYYY")) :
										newData.push(content[key]);
								}
							}
						});
						newData.push(idx);
						data.push(newData);
					});

					//ADD ACTION
					rowHeader.push({
						name: "Action",
						options: {
							filter: false,
							sort: false,
							customBodyRender: (value, tableMeta, updateValue) => {
								return (
									<div>
										<Button onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-edit"></i> Edit</Button>
										<Button onClick={() => this.onClickDelete(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-delete"></i> Delete</Button>
									</div>
								);
							}
						}
					});
				}


				this.setState({
					tableName: tableName,
					data: data,
					loading: false,
					rowHeader,
					rawData: contents
				});
			})
			.catch(err => console.error(`error Get${tableName}Maker`, err));
	}

	customToolbarOnClick = () => {
		const { history, match } = this.props;
		history.push(`${match.url}/add-data`);
	}

	onClickEdit = (idx) => {
		const { rawData, tableName } = this.state;
		switch (tableName) {
			case 'MasterDupCriteria':
				this.props.history.push(`/app/main/parameter-dedup-maker/existing-parameter/${tableName}/${rawData[idx].category}/${rawData[idx].code}`)
			break;
			case 'MasterDupCriteriaDetail':
				this.props.history.push(`/app/main/parameter-dedup-maker/existing-parameter/${tableName}/${rawData[idx].criteriaId}/${rawData[idx].criteriaCode}/${rawData[idx].criteriaSequence}`)
			break;
			default:
				this.props.history.push(`/app/main/parameter-dedup-maker/existing-parameter/${tableName}/${rawData[idx].id}`)
			break;
		}
	}

	onClickDelete = (idx) => {
		const { rawData } = this.state;
		this.refs.deleteConfirmationDialog.open();
		this.setState({ selectedData: rawData[idx] });
	}

	deleteData = () => {
		this.refs.deleteConfirmationDialog.close();
		this.setState({ loading: true });
		
		const { selectedData, tableName } = this.state;
		let { description } = selectedData;
		
		const reqData = {
			by: this.props.user.userId
		};

		switch(tableName){
			case 'MasterDupCriteria':
				reqData.category = selectedData.category;
				reqData.code = selectedData.code;
			break;
			case 'MasterDupCriteriaDetail':
				reqData.criteria= selectedData.criteriaId;
				reqData.code = selectedData.criteriaCode;
				reqData.sequence = selectedData.criteriaSequence;
				description = selectedData.criteriaCode;
			break;
			default:
				reqData.id = selectedData.id;
			break;
		}
		
		const token = this.props.token;
		getApi(token).post(`CIMB-Appraisal-Dedup/api/json/reply/Delete${tableName}Maker`, reqData)
			.then(res => {
				// console.log("res:", res.data);
				if(res.data.responseCode == "00"){
					this.setState({
						selectedData: null
					})
					NotificationManager.success(`${tableName} ${description} Deleted.`, "Success", getNotifTimeout());
					//RELOAD
					this.componentDidMount();
				}else{
					console.error(`error Delete${tableName}Maker`, res.data.responseDesc);
					NotificationManager.error(`Error ${res.data.responseCode}. ${res.data.responseDesc}`, "Error", getNotifTimeout());
					this.setState({ loading: false });
				}
			}).catch(err => {
				console.error(`error Delete${tableName}Maker`, err);
				NotificationManager.error(`Delete ${tableName} ${description} Failed.`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	render() {
		const { data, tableName, loading, selectedData, rowHeader } = this.state;
		const columns = rowHeader;
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none',
			customToolbar: () => {
				return (
					<CustomToolbar tooltipTitle="Add Data" onClick={this.customToolbarOnClick} />
				);
			}
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="Parameter Dedup - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					<MUIDataTable
						title={`Existing Parameter - ${tableName}`}
						data={data}
						columns={columns}
						options={options}
					/>
					{loading &&
						<RctSectionLoader />
					}
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="deleteConfirmationDialog"
					title={`Are you sure want to delete 
						${selectedData ? 
							selectedData.description ? 
								selectedData.description : 
								selectedData.criteriaCode ?
									selectedData.criteriaCode : ""
							: ""
						} ?`}
					onConfirm={() => this.deleteData()}
				/>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps)(ExistingParameterDedup);