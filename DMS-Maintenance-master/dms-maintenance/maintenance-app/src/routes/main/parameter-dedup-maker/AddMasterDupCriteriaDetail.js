import React from "react";
import { connect } from 'react-redux';
import { getApi } from 'Api';
import { NotificationManager } from 'react-notifications';
import {
    Select,
    TextField,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormHelperText,
    FormLabel,
    InputLabel,
    MenuItem,
    Button,
} from "@material-ui/core";
import { Alert } from 'reactstrap';

import { getNotifTimeout } from "Helpers/helpers";

// page title bar
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";

// rct card box
import RctCollapsibleCard from "Components/RctCollapsibleCard/RctCollapsibleCard";

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// redux action
import { logoutUser } from 'Actions';

class AddMasterDupCriteriaDetail extends React.Component {
    state = {
        formData: {
            criteriaId: "",
            criteriaCode: "",
            criteriaSequence: 0,
            targetFieldName: "",
            operator: "",
            dataFieldName: "",
            staticValue: "",
            cleanseTemplate: "",
            active: false,
        },
        formError: {
            criteriaId: false,
            criteriaCode: false,
            criteriaSequence: false,
        },
        isEdit: false,
        loading: true,
        alertVisible: false,
        alertMessage: "",
        dupOperators: [],
    };

    async componentDidMount() {
        const { token } = this.props;
        const { criteriaId, criteriaCode, criteriaSequence } = this.props.match.params;
        if (criteriaId && criteriaCode && criteriaSequence) {
            this.setState({ isEdit: true });
            const reqData = {
                criteriaId,
                criteriaCode,
                criteriaSequence
            }
            try {
                const res = await getApi(token).post('CIMB-Appraisal-Dedup/api/json/reply/GetMasterDupCriteriaDetailById', reqData);
                const criteriaDetail = res.data.criteriaDetail;
                if (criteriaDetail) {
                    if (res.data.responseCode == "00") {
                        this.setState({
                            formData: {
                                criteriaId: criteriaDetail.criteriaId,
                                criteriaCode: criteriaDetail.criteriaCode,
                                criteriaSequence: criteriaDetail.criteriaSequence,
                                targetFieldName: criteriaDetail.targetFieldName,
                                operator: criteriaDetail.operator,
                                dataFieldName: criteriaDetail.dataFieldName,
                                staticValue: criteriaDetail.staticValue,
                                cleanseTemplate: criteriaDetail.cleanseTemplate,
                                active: criteriaDetail.active,
                                createBy: criteriaDetail.createBy,
                                createDate: criteriaDetail.createDate
                            }
                        }, this.getDupOperator);
                    } else {
                        console.error("error GetMasterDupCriteriaDetailById", res.data.responseDesc);
                    }
                } else {
                    //CriteriaDetail not found
                    this.props.history.goBack();
                }
            } catch (err) {
                console.error("error GetMasterDupCriteriaDetailById", err);
                const { formError } = this.state;
                formError.save = true;
                this.setState({
                    loading: false,
                    formError
                }, this.onShowAlert(err.message));
                if (err.response && err.response.status == 401) {
                    this.props.logoutUser(true);
                }
            }
        } else {
            this.getDupOperator();
        }
    }

    async getDupOperator(){
        const {token} = this.props;
        try{
            const res = await getApi(token).post('CIMB-Appraisal-Dedup/api/json/reply/GetEnumDupOperatorMaker');
            if(res.data.responseCode == "00"){
                this.setState({
                    dupOperators: res.data.dupOperator ? res.data.dupOperator : [],
                    loading: false
                });
            }
        }catch(err){
            console.error("error getDupOperator");
            const { formError } = this.state;
			formError.save = true;
			this.setState({
				loading: false,
				formError
			}, this.onShowAlert(err.message));
			if (err.response && err.response.status == 401) {
				this.props.logoutUser(true);
			}
        }
    }

    handleChange = ({ target }) => {
        const { formData } = this.state;
        const { checked, name, type, value } = target;
        const val = (type === 'checkbox') ? checked : value;
        formData[name] = val;
        this.setState({ formData });
    };

    isCanSave = () => {
        const { formData } = this.state;

        this.setState({
            formError: {
                criteriaId: formData.criteriaId === '',
                criteriaCode: formData.criteriaCode === '',
                criteriaSequence: formData.criteriaSequence === '',
            }
        })

        return (
            formData.criteriaId !== '' &&
            formData.criteriaCode !== '' &&
            formData.criteriaSequence !== ''
        )
    }

    handleSave = e => {
        e.preventDefault();
        if (this.isCanSave()) {
            this.setState({ loading: true });
            const { formData, isEdit } = this.state;
            if (!isEdit) {
                formData.createBy = this.props.user.userId;
                formData.createDate = "";
            }
            formData.updateBy = this.props.user.userId;
            formData.updateDate = "";
            console.log('formData', formData);
            getApi(this.props.token).post("/CIMB-Appraisal-Dedup/api/json/reply/SaveOrUpdateMasterDupCriteriaDetail", formData)
                .then((res) => {
                    if (res.data.responseCode == "00") {
                        this.setState({ loading: false });
                        this.props.history.push("/app/main/parameter-dedup-maker/pending-approval/MasterDupCriteriaDetail");
                        NotificationManager.success('Data Created Successfully', "Success", getNotifTimeout());
                    } else {
                        this.setState({ loading: false });
                        NotificationManager.error('Failed to Create Data. ' + res.data.responseDesc, "Error", getNotifTimeout());
                    }
                }).catch(err => {
                    console.error("error SaveOrUpdateMasterDupCriteriaDetail", err);
                    this.setState({ loading: false });
                    NotificationManager.error('Failed to Create Data.', "Error", getNotifTimeout());
                });
        }
    }

    handleBack = e => {
        e.preventDefault();
        this.props.history.goBack();
    }

    onShowAlert = (message) => {
        // console.log("message", message);
        this.setState({
            alertVisible: true,
            alertMessage: message
        });
    }

    onDismissAlert = (key) => {
        this.setState({ [key]: false });
    }

    render() {
        const { formData, formError, isEdit, loading, alertVisible, alertMessage, dupOperators } = this.state;
        return (
            <div className="textfields-wrapper">
                <PageTitleBar
                    title="Parameter Dedup - Maker"
                    match={this.props.match}
                />
                <RctCollapsibleCard>
                    {loading &&
                        <RctSectionLoader />
                    }
                    <Alert color="danger" isOpen={alertVisible} toggle={() => this.onAlertDismiss("alertVisible")}>
                        <b>Error! </b>
                        {alertMessage}
                    </Alert>
                    <div className="row d-flex justify-content-between py-10 px-10">
                        <div>
                            <h2>{isEdit ? "Edit MasterDupCriteriaDetail" : "New MasterDupCriteriaDetail"}</h2>
                        </div>
                        <div>
                            <Button onClick={this.handleSave}
                                disabled={formError.save}
                                variant="contained"
                                className={`btn-primary mr-10 mb-10 btn-icon ${formError.save ? "" : "text-white"}`}
                            ><i className="zmdi zmdi-check"></i> Save</Button>
                            <Button onClick={this.handleBack} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Cancel</Button>
                        </div>
                    </div>
                    <form noValidate autoComplete="off">
                        <div className="row">
                            <div className="col-sm-6 col-md-6 col-xl-6 d-block">
                                <div className="form-group">
                                    <TextField
                                        required
                                        disabled={isEdit}
                                        id="criteriaId"
                                        name="criteriaId"
                                        fullWidth
                                        label="Criteria Id"
                                        value={formData.criteriaId}
                                        error={formError.criteriaId}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        required
                                        disabled={isEdit}
                                        id="criteriaCode"
                                        name="criteriaCode"
                                        fullWidth
                                        label="Criteria Code"
                                        value={formData.criteriaCode}
                                        error={formError.criteriaCode}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        required
                                        disabled={isEdit}
                                        id="criteriaSequence"
                                        name="criteriaSequence"
                                        type="number"
										fullWidth
                                        label="Criteria Sequence"
                                        value={formData.criteriaSequence}
                                        error={formError.criteriaSequence}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        id="targetFieldName"
                                        name="targetFieldName"
                                        fullWidth
                                        label="Target Field Name"
                                        value={formData.targetFieldName}
                                        error={formError.targetFieldName}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
									<FormControl fullWidth>
										<InputLabel htmlFor="operator">Operator</InputLabel>
										<Select
											value={formData.operator}
											onChange={this.handleChange}
											inputProps={{ name: 'operator', id: 'operator', }}>
											<MenuItem value=""><em>None</em></MenuItem>
											{(dupOperators) ? dupOperators.map((operator, key) => (
												<MenuItem key={key} value={operator.id}>{operator.description}</MenuItem>
											)) : ""
											}
										</Select>
									</FormControl>
								</div>
                            </div>
                            <div className="col-sm-6 col-md-6 col-xl-6 d-block">
                                <div className="form-group">
                                    <TextField
                                        id="dataFieldName"
                                        name="dataFieldName"
                                        fullWidth
                                        label="Data Field Name"
                                        value={formData.dataFieldName}
                                        error={formError.dataFieldName}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        id="staticValue"
                                        name="staticValue"
                                        fullWidth
                                        label="Static Value"
                                        value={formData.staticValue}
                                        error={formError.staticValue}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        id="cleanseTemplate"
                                        name="cleanseTemplate"
                                        fullWidth
                                        label="Cleanse Template"
                                        value={formData.cleanseTemplate}
                                        error={formError.cleanseTemplate}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <FormControlLabel
                                        className="mb-0 mt-2"
                                        control={
                                            <Checkbox
                                                id="active"
                                                name="active"
                                                color="primary"
                                                checked={formData.active}
                                                onChange={this.handleChange}
                                            />
                                        }
                                        label="Active"
                                    />
                                </div>
                            </div>
                        </div>
                    </form>
                </RctCollapsibleCard>
            </div>
        );
    }
}

// map state to props
const mapStateToProps = ({ authUser }) => {
    const user = JSON.parse(authUser.user);
    const token = JSON.parse(authUser.token);
    return { user, token };
}

export default connect(mapStateToProps, {
    logoutUser
})(AddMasterDupCriteriaDetail);