import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import { getApi } from "Api";
import { NotificationManager } from "react-notifications";
import {
    Select,
    TextField,
    Checkbox,
    FormControl,
    FormControlLabel,
    InputLabel,
    MenuItem,
    Button,
} from "@material-ui/core";
import { Alert } from "reactstrap";

import { getNotifTimeout } from "Helpers/helpers";

// page title bar
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";

// rct card box
import RctCollapsibleCard from "Components/RctCollapsibleCard/RctCollapsibleCard";

// rct section loader
import RctSectionLoader from "Components/RctSectionLoader/RctSectionLoader";

// redux action
import { logoutUser } from "Actions";

class AddMasterDupCriteria extends React.Component {
    state = {
        formData: {
            category: "",
            code: "",
            description: "",
            maxHit: 0,
            autoReject: false,
            reject: "",
            targetTableName: "",
            targetFieldKeyList: "",
            dataTableName: "",
            dataCondition: "",
            dupType: "",
            active: false,
        },
        formError: {
            id: false,
            code: false,
            description: false,
        },
        dupCategories: [],
        dupTypes: [],
        isEdit: false,
        loading: true,
        alertVisible: false,
        alertMessage: "",
    };

    async componentDidMount() {
        const { token } = this.props;
        const { category, code } = this.props.match.params;

        if (category && code) {
            this.setState({ isEdit: true });
            const reqData = {
                category,
                code
            }
            try {
                const res = await getApi(token).post('CIMB-Appraisal-Dedup/api/json/reply/GetMasterDupCriteriaById', reqData);
                const criteria = res.data.criteria;
                if (criteria) {
                    // console.log(res.data);
                    if (res.data.responseCode == "00") {
                        this.setState({
                            formData: {
                                category: criteria.category,
                                code: criteria.code,
                                description: criteria.description,
                                maxHit: criteria.maxHit,
                                autoReject: criteria.autoReject,
                                reject: criteria.reject,
                                targetTableName: criteria.targetTableName,
                                targetFieldKeyList: criteria.targetFieldKeyList,
                                dataTableName: criteria.dataTableName,
                                dataCondition: criteria.dataCondition,
                                dupType: criteria.dupType,
                                active: criteria.active,
                                createBy: criteria.createBy,
                                createDate: criteria.createDate
                            }
                        }, this.getDupType);
                    } else {
                        throw new Error(res.data.responseCode + ": " + res.data.responseDesc);
                    }
                } else {
                    //Criteria not found
                    this.props.history.goBack();
                }
            } catch (err) {
                console.error("error GetMasterDupCriteriaById", err);
                const { formError } = this.state;
                formError.save = true;
                this.setState({
                    loading: false,
                    formError
                }, this.onShowAlert(err.message));
                if (err.response && err.response.status == 401) {
                    this.props.logoutUser(true);
                }
            }
        } else {
            this.getDupType();
        }
    }

    async getDupType() {
        const { token } = this.props;
        try {
            const [resDupCategories, resDupTypes] = await axios.all([
                getApi(token).post('CIMB-Appraisal-Dedup/api/json/reply/GetEnumDupCategoryMaker'),
                getApi(token).post('CIMB-Appraisal-Dedup/api/json/reply/GetEnumDupTypeMaker')
            ]);
            if (resDupCategories.data.responseCode == "00" &&
                resDupTypes.data.responseCode == "00") {
                this.setState({
                    dupCategories: resDupCategories.data.dupCategory ? resDupCategories.data.dupCategory : [],
                    dupTypes: resDupTypes.data.dupType ? resDupTypes.data.dupType : [],
                    loading: false
                });
            }
        } catch (err) {
            console.error("error getDupCategories/getDupTypes");
            const { formError } = this.state;
            formError.save = true;
            this.setState({
                loading: false,
                formError
            }, this.onShowAlert(err.message));
            if (err.response && err.response.status == 401) {
                this.props.logoutUser(true);
            }
        }
    }
    handleChange = ({ target }) => {
        const { formData } = this.state;
        const { checked, name, type, value } = target;
        const val = (type === 'checkbox') ? checked : value;
        formData[name] = val;
        this.setState({ formData });
    };

    isCanSave = () => {
        const { formData } = this.state;

        this.setState({
            formError: {
                category: formData.category == '',
                code: formData.code == '',
                description: formData.description == '',
            }
        })

        return (
            formData.id !== '' &&
            formData.code !== '' &&
            formData.description !== ''
        )
    }

    handleSave = e => {
        e.preventDefault();
        if (this.isCanSave()) {
            this.setState({ loading: true });
            const { formData, isEdit } = this.state;
            if (!isEdit) {
                formData.createBy = this.props.user.userId;
                formData.createDate = "";
            }
            formData.updateBy = this.props.user.userId;
            formData.updateDate = "";
            // console.log('formData', formData);
            getApi(this.props.token).post("/CIMB-Appraisal-Dedup/api/json/reply/SaveOrUpdateMasterDupCriteria", formData)
                .then((res) => {
                    if (res.data.responseCode == "00") {
                        this.setState({ loading: false });
                        this.props.history.push("/app/main/parameter-dedup-maker/pending-approval/MasterDupCriteria");
                        NotificationManager.success('Data Created Successfully', "Success", getNotifTimeout());
                    } else {
                        this.setState({ loading: false });
                        NotificationManager.error('Failed to Create Data. ' + res.data.responseDesc, "Error", getNotifTimeout());
                    }
                }).catch(err => {
                    console.error("error SaveOrUpdateMasterDupCriteria", err);
                    this.setState({ loading: false });
                    NotificationManager.error('Failed to Create Data.', "Error", getNotifTimeout());
                });
        }
    }

    handleBack = e => {
        e.preventDefault();
        this.props.history.goBack();
    }

    onShowAlert = (message) => {
        // console.log("message", message);
        this.setState({
            alertVisible: true,
            alertMessage: message
        });
    }

    onDismissAlert = (key) => {
        this.setState({ [key]: false });
    }

    render() {
        const { formData, formError, isEdit, loading, alertVisible, alertMessage, dupCategories, dupTypes } = this.state;
        return (
            <div className="textfields-wrapper">
                <PageTitleBar
                    title="Parameter Dedup - Maker"
                    match={this.props.match}
                />
                <RctCollapsibleCard>
                    {loading &&
                        <RctSectionLoader />
                    }
                    <Alert color="danger" isOpen={alertVisible} toggle={() => this.onAlertDismiss("alertVisible")}>
                        <b>Error! </b>
                        {alertMessage}
                    </Alert>
                    <div className="row d-flex justify-content-between py-10 px-10">
                        <div>
                            <h2>{isEdit ? "Edit MasterDupCriteria" : "New MasterDupCriteria"}</h2>
                        </div>
                        <div>
                            <Button onClick={this.handleSave}
                                disabled={formError.save}
                                variant="contained"
                                className={`btn-primary mr-10 mb-10 btn-icon ${formError.save ? "" : "text-white"}`}
                            ><i className="zmdi zmdi-check"></i> Save</Button>
                            <Button onClick={this.handleBack} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Cancel</Button>
                        </div>
                    </div>
                    <form noValidate autoComplete="off">
                        <div className="row">
                            <div className="col-sm-6 col-md-6 col-xl-6 d-block">
                                <div className="form-group">
                                    <FormControl
                                        required
                                        fullWidth
                                        disabled={isEdit}
                                        error={formError.category}
                                    >
                                        <InputLabel htmlFor="category">Category</InputLabel>
                                        <Select
                                            value={formData.category}
                                            onChange={this.handleChange}
                                            inputProps={{ name: 'category', id: 'category', }}>
                                            <MenuItem value=""><em>None</em></MenuItem>
                                            {(dupCategories) ? dupCategories.map((category, key) => (
                                                <MenuItem key={key} value={category.id}>{category.description}</MenuItem>
                                            )) : ""
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="form-group">
                                    <TextField
                                        required
                                        disabled={isEdit}
                                        id="code"
                                        name="code"
                                        fullWidth
                                        label="Code"
                                        value={formData.code}
                                        error={formError.code}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        required
                                        id="description"
                                        name="description"
                                        fullWidth
                                        label="Description"
                                        value={formData.description}
                                        error={formError.description}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        id="maxHit"
                                        name="maxHit"
                                        type="number"
                                        fullWidth
                                        label="Max Hit"
                                        value={formData.maxHit}
                                        error={formError.maxHit}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <FormControlLabel
                                        className="mb-0 mt-2"
                                        control={
                                            <Checkbox
                                                id="autoReject"
                                                name="autoReject"
                                                color="primary"
                                                checked={formData.autoReject}
                                                onChange={this.handleChange}
                                            />
                                        }
                                        label="Auto Reject"
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        id="reject"
                                        name="reject"
                                        fullWidth
                                        label="Reject"
                                        value={formData.reject}
                                        error={formError.reject}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </div>
                            <div className="col-sm-6 col-md-6 col-xl-6 d-block">
                                <div className="form-group">
                                    <TextField
                                        id="targetTableName"
                                        name="targetTableName"
                                        fullWidth
                                        label="Target Table Name"
                                        value={formData.targetTableName}
                                        error={formError.targetTableName}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        id="dataTableName"
                                        name="dataTableName"
                                        fullWidth
                                        label="Data Table Name"
                                        value={formData.dataTableName}
                                        error={formError.dataTableName}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        id="dataCondition"
                                        name="dataCondition"
                                        fullWidth
                                        label="Data Condition"
                                        value={formData.dataCondition}
                                        error={formError.dataCondition}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <FormControl fullWidth>
                                        <InputLabel htmlFor="dupType">Dup Type</InputLabel>
                                        <Select
                                            value={formData.dupType}
                                            onChange={this.handleChange}
                                            inputProps={{ name: 'dupType', id: 'dupType', }}>
                                            <MenuItem value=""><em>None</em></MenuItem>
                                            {(dupTypes) ? dupTypes.map((dupType, key) => (
                                                <MenuItem key={key} value={dupType.id}>{dupType.description}</MenuItem>
                                            )) : ""
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="form-group">
                                    <FormControlLabel
                                        className="mb-0 mt-2"
                                        control={
                                            <Checkbox
                                                id="active"
                                                name="active"
                                                color="primary"
                                                checked={formData.active}
                                                onChange={this.handleChange}
                                            />
                                        }
                                        label="Active"
                                    />
                                </div>
                            </div>
                        </div>
                    </form>

                </RctCollapsibleCard>
            </div>
        );
    }
}

// map state to props
const mapStateToProps = ({ authUser }) => {
    const user = JSON.parse(authUser.user);
    const token = JSON.parse(authUser.token);
    return { user, token };
}

export default connect(mapStateToProps, {
    logoutUser
})(AddMasterDupCriteria);