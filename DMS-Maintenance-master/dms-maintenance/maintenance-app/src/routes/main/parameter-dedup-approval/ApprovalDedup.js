/**
 * Pending Approval Parameter Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';
import { startCase } from 'lodash';
// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class ApprovalDedup extends Component {

	state = {
		rawData: [],
		data: [],
		selectedRawData: null,
		loading: true, // loading activity,
		tableName: "",
		rowHeader: []
	}

	componentDidMount = () => {
		const token = this.props.token;
		const { tableName } = this.props.match.params;
		this.setState({ tableName });

		getApi(token).post(`CIMB-Appraisal-Dedup/api/json/reply/Get${tableName}Checker`)
			.then(res => {
				console.log("data", res.data);
				if (res.data.responseCode == "00") {
					const data = [];
					const rowHeader = [];
					let contents = [];
					switch (tableName) {
						case 'EnumDupCategory':
							contents = res.data.dupCategory;
						break;
						case 'EnumDupOperator':
							contents = res.data.dupOperator;
						break;
						case 'EnumDupType':
							contents = res.data.dupType;
						break;
						case 'MasterDupCleansing':
							contents = res.data.dupCleansing;
						break;
						case 'MasterDupCriteria':
							contents = res.data.dupCriteria;
						break;
						case 'MasterDupCriteriaDetail':
							contents = res.data.dupCriteriaDetail;
						break;
					}

					if (contents && contents.length !== 0) {
						Object.keys(contents[0]).forEach(function (key) {
							if (key != "__type" && (typeof contents[0][key] !== 'object' || contents[0][key] == null)) {
								rowHeader.push(startCase(key));
							}
						});
						//regex for checking ASP DATE
						const regexASPDate = /^\/Date\((\d+)(?:-(\d+))?\)/;

						contents.forEach((content, idx) => {
							const newData = [];
							Object.keys(content).forEach(function (key) {
								if (key !== "__type") {
									if (typeof content[key] === 'boolean') {
										newData.push(content[key] ? "Yes" : "No");
									} else if (typeof content[key] === 'number') {
										newData.push(content[key]);
									} else if (content[key] == null) {
										newData.push("-");
									} else if (typeof content[key] === 'string') {
										regexASPDate.test(content[key]) ?
											newData.push(convertDateASP(content[key], "DD-MM-YYYY")) :
											newData.push(content[key]);
									}
								}
							});
							newData.push(idx);
							data.push(newData);
						});

						//ADD ACTION
						rowHeader.push({
							name: "Action",
							options: {
								filter: false,
								sort: false,
								customBodyRender: (value, tableMeta, updateValue) => {
									return (
										<div>
											<Button onClick={() => this.onClickApprove(value)} variant="contained" className="btn-success mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-check"></i> Approve</Button>
											<Button onClick={() => this.onClickReject(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Reject</Button>
										</div>
									);
								}
							}
						});
					}

					this.setState({
						rawData: contents,
						data,
						rowHeader,
						loading: false
					});
				} else {
					console.error(`error Get${tableName}Pending`, res.data.responseCode + ": " + res.data.responseDesc);
					this.setState({ loading: false });
				}
			}).catch(err => {
				console.error(`error Get${tableName}Pending`, err);
				this.setState({ loading: false });
			});
	}

	onClickApprove = (idx) => {
		const { rawData, tableName } = this.state;
		const selectedRawData = rawData[idx];
		const { tempId, description } = selectedRawData;
		this.setState({ 
			loading: true,
			selectedRawData 
		});
		const reqData = {
			tempId: tempId,
			by: this.props.user.userId
		};
		const token = this.props.token;
		getApi(token).post(`CIMB-Appraisal-Dedup/api/json/reply/Approve${tableName}Checker`, reqData)
			.then(res => {
				// console.log("res:", res.data);
				if (res.data.responseCode == "00") {
					NotificationManager.success(`${tableName} ${description? description : tempId} Approved.`, "Success", getNotifTimeout());
					//RELOAD
					this.componentDidMount();
				} else {
					NotificationManager.error(`Approve ${tableName} ${description? description : tempId} Failed.`, "Error", getNotifTimeout());
					this.setState({ loading: false });
				}
			}).catch(err => {
				console.error(`error Approve${tableName}Checker`, err);
				NotificationManager.error(`Approve ${tableName} ${description? description : tempId} Failed.`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	onClickReject = (idx) => {
		const { rawData } = this.state;
		this.refs.rejectConfirmationDialog.open();
		this.setState({ selectedRawData: rawData[idx] });
	}

	rejectParam = () => {
		const { selectedRawData, tableName } = this.state;
		const { description, tempId } = selectedRawData;
		this.refs.rejectConfirmationDialog.close();
		this.setState({ loading: true });
		const reqData = {
			tempId: tempId,
			by: this.props.user.userId
		};
		const token = this.props.token;
		getApi(token).post(`CIMB-Appraisal-Dedup/api/json/reply/Reject${tableName}Checker`, reqData)
			.then(res => {
				// console.log("res:", res.data);
				NotificationManager.success(`${tableName} ${description? description : tempId} Rejected.`, "Success", getNotifTimeout());
				//RELOAD
				this.componentDidMount();
			}).catch(err => {
				console.error(`error Reject${tableName}Checker`, err);
				NotificationManager.error(`Reject ${tableName} ${description? description : tempId} Failed.`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	render() {
		const { data, loading, selectedRawData, tableName, rowHeader } = this.state;
		const columns = rowHeader;
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="Parameter Dedup Approval"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={`Approval - ${tableName}`}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="rejectConfirmationDialog"
					title={`Are you sure want to reject ${tableName} ${selectedRawData ? selectedRawData.description ? selectedRawData.description : selectedRawData.tempId : ""}?`}
					onConfirm={this.rejectParam}
				/>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps)(ApprovalDedup);
