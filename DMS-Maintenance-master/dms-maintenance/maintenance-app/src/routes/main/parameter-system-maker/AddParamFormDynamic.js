/**
 * Material Text Field
 */
import React from "react";
import { connect } from 'react-redux';
import { getApi } from 'Api';
import { NotificationManager } from 'react-notifications';
import { camelCase } from 'lodash';
import {
	Select,
	TextField,
	Checkbox,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	InputLabel,
	MenuItem,
	Button,
} from "@material-ui/core";

import { getNotifTimeout } from "Helpers/helpers";

// page title bar
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";

// rct card box
import RctCollapsibleCard from "Components/RctCollapsibleCard/RctCollapsibleCard";

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// redux action
import { logoutUser } from 'Actions';

function FormField({ field, onChange, formData, selectListData }) {
	const fieldType = field.fieldType.toLowerCase();
	switch (fieldType) {
		case "text":
			return (
				<div className="form-group">
					<TextField
						// required
						id={field.fieldName}
						name={field.fieldName}
						fullWidth
						label={field.description}
						value={formData[field.fieldName]}
						// error={formError.tableName}
						onChange={onChange}
					/>
				</div>);
		case "bool":
			return (
				<div className="form-group">
					<FormControlLabel
						className="mb-0 mt-2"
						control={
							<Checkbox
								required
								id={field.fieldName}
								name={field.fieldName}
								color="primary"
								checked={formData[field.fieldName]}
								onChange={onChange}
							/>
						}
						label={field.description}
					/>
				</div>);
		case "ddl":
			return (
				<div className="form-group">
					<FormControl
						// required
						fullWidth
					>
						<InputLabel htmlFor={field.fieldName}>{field.description}</InputLabel>
						<Select
							value={formData[field.fieldName]}
							onChange={onChange}
							inputProps={{ name: field.fieldName, id: field.fieldName, }}>
							<MenuItem value=""><em>None</em></MenuItem>
							{(selectListData[field.fieldName]) ? selectListData[field.fieldName].map((item, key) => (
								<MenuItem key={key} value={item.code}>{item.description}</MenuItem>
							)) : ""
							}
						</Select>
					</FormControl>
				</div>
			)
		default:
			return null;
	}
}

class AddParamFormDynamic extends React.Component {
	state = {
		fields: [],
		formData: {},
		formError: {},
		isEdit: false,
		tableName: "",
		selectListData: {},
		loading: true
	};

	componentDidMount = () => {
		const token = this.props.token;
		const { tableName } = this.props.match.params;
		const reqData = {
			tableName: tableName
		}
		getApi(token).post('CIMB-Appraisal-Parameter/api/json/reply/GetParamFormDynamic', reqData)
			.then(res => {
				const { formData, formError, selectListData } = this.state;
				const fields = res.data.content.fields;
				// console.log("fields:", fields);
				fields.forEach((field, idx) => {
					field.fieldName = camelCase(field.fieldName);
					switch (field.fieldType) {
						case "bool":
							formData[field.fieldName] = false;
							break;
						case "ddl":
							const reqTableReff = {
								tableName: field.tableReff
							}
							getApi(token).post('CIMB-Appraisal-Parameter/api/json/reply/GetParamFormDynamicReff', reqTableReff)
								.then(resTableReff => {
									selectListData[field.fieldName] = resTableReff.data.content;
									// console.log("selectListData", selectListData);
									this.setState({ selectListData });
								}).catch(err =>{
									console.error("error GetParamFormDynamicReff", err);
								});
						default:
							formData[field.fieldName] = "";
							break;
					}
				});
				// console.log("formData", formData);
				this.setState({
					fields: fields,
					tableName: tableName,
					formData: formData,
					loading: false
				});
			})
			.catch(err => {
				console.error("error GetParamFormDynamic", err);
				this.setState({
					loading: false
				});
			});
	}

	handleChange = ({ target }) => {
		const { formData } = this.state;
		const { checked, name, type, value } = target;
		const val = (type === 'checkbox') ? checked : value;
		formData[name] = val;
		this.setState({ formData });
	};

	isCanSave = () => {
		const { formData } = this.state;

		return (
			// formData.tableName !== "" &&
			// formData.fieldPos !== "" &&
			// formData.fieldName !== "" &&
			// formData.fieldType !== "" &&
			// formData.fieldDesc !== "" &&
			// formData.fieldReff !== "" &&
			// formData.fieldAuto !== "" &&
			// formData.fieldKey !== "" &&
			// formData.fieldMan !== "" &&
			// formData.groupBy !== "" &&
			// formData.status !== "" &&
			// formData.createdBy !== ""
			true
		)
	}

	handleSave = e => {
		e.preventDefault();
		if (this.isCanSave()) {
			const { formData, tableName, isEdit, selectListData } = this.state;
			const contentData = formData;
			// console.log(selectListData);
			this.setState({loading: true});
			if (isEdit) {
				contentData.updateBy = this.props.user.userId;
			} else {
				contentData.createBy = this.props.user.userId;
			}
			
			const reqData = {
				tableName,
				userId: this.props.user.userId,
				status: isEdit ? "update" : "save",
				content: JSON.stringify(contentData)
			}

			console.log('reqData', reqData);
			getApi(this.props.token).post("/CIMB-Appraisal-Parameter/api/json/reply/UpdateParamTable", reqData)
				.then((res) => {
					this.setState({loading: false});
					if (res.data.responseCode == "00") {
						this.props.history.push("/app/main/parameter-system-maker/pending-approval");
						NotificationManager.success('Parameter Created Successfully', "Success", getNotifTimeout());
					} else {
						console.error("error UpdateParamTable", res.data.responseCode + ": " + res.data.responseDesc);
						NotificationManager.error('Failed to Create Parameter. ' + res.data.responseDesc, "Error", getNotifTimeout());
					}
				}).catch(err => {
					this.setState({loading: false});
					console.error("error UpdateParamTable", err);
					NotificationManager.error('Failed to Create Parameter', "Error", getNotifTimeout());
				});
		}
	}

	handleBack = e => {
		e.preventDefault();
		this.props.history.goBack();
	}

	render() {
		const { formData, formError, fields, selectListData, loading, tableName } = this.state;

		return (
			<div className="textfields-wrapper">
				<PageTitleBar
					title="Parameter System - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard>
					<div className="row d-flex justify-content-between py-10 px-10">
						<div>
							<h2>Add Data - {tableName}</h2>
						</div>
						<div>
							<Button onClick={this.handleSave} variant="contained" className="btn-primary mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-check"></i> Save</Button>
							<Button onClick={this.handleBack} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Cancel</Button>
						</div>
					</div>
					{loading &&
						<RctSectionLoader />
					}
					<form noValidate autoComplete="off">
						<div className="row">
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								{fields.map((field, idx) =>
									idx <= (fields.length / 2) ? (
										<FormField key={idx} field={field} formData={formData} selectListData={selectListData} onChange={this.handleChange} formData={formData} />
									) : "")
								}
							</div>
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								{fields.map((field, idx) =>
									idx > (fields.length / 2) ? (
										<FormField key={idx} field={field} formData={formData} selectListData={selectListData} onChange={this.handleChange} formData={formData} />
									) : "")
								}
							</div>
						</div>
					</form>
				</RctCollapsibleCard>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps, {
	logoutUser
})(AddParamFormDynamic);