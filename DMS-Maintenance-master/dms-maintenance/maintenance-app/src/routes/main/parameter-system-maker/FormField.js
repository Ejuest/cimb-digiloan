import React from "react";
import {
	Select,
	TextField,
	Checkbox,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	InputLabel,
	MenuItem,
	Button,
} from "@material-ui/core";

export default class FormField extends React.Component {
	
	render(){
		const {field, onChange, formData} = this.props;
		switch (field.fieldType) {
			case "text":
				return (
					<div className="form-group">
						<TextField
							required
							id={field.fieldName}
							name={field.fieldName}
							fullWidth
							label={field.description}
							defaultValue={formData[field.fieldName]}
							// error={formError.tableName}
							onChange={onChange}
						/>
					</div>);
			case "bool":
				return (
					<div className="form-group">
						<FormControlLabel
							className="mb-0 mt-2"
							control={
								<Checkbox
									required
									id={field.fieldName}
									name={field.fieldName}
									color="primary"
									checked={formData[field.fieldName]}
									onChange={onChange}
								/>
							}
							label={field.description}
						/>
					</div>);
			case "ddl":
				return (
					<div className="form-group">
						<FormControl
							required
							fullWidth
						>
							<InputLabel htmlFor="group">Group</InputLabel>
							<Select
								value={formData[field.fieldName]}
								onChange={onChange}
								inputProps={{ name: field.fieldName, id: field.fieldName, }}>
								<MenuItem value=""><em>None</em></MenuItem>
								{(groups) ? groups.map((group, key) => (
									<MenuItem key={key} value={group.id}>{group.name}</MenuItem>
								)) : ""
								}
							</Select>
						</FormControl>
					</div>
				)
			default:
				return null;
		}
	}
}