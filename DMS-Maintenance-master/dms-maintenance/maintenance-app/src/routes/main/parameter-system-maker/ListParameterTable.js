/**
 * Miscellaneous Widgets Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { upperCase } from 'lodash';
import {
	CustomCardWidget
} from "Components/Widgets";

// api
import { getApi } from 'Api';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { NotificationManager } from 'react-notifications';
import { Alert } from 'reactstrap';
// pagination
import Pagination from 'rc-pagination';
// custom css
import 'rc-pagination/assets/index.css';
import './css/rc-pagination-custom.css';

class ListParameterTable extends Component {

	state = {
		listTables: [],
		loading: true,
		alertVisible: false,
		alertMessage: "",
		curPage: 1,
		curListTables: [],
	}

	componentDidMount = () => {
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Parameter/api/json/reply/GetParamTableList')
			.then(res => {
				if (res.data.responseCode == "00") {
					let listTables = res.data.tableNames;
					// console.log("listTables:", listTables);
					this.setState({
						listTables: listTables,
						loading: false,
						curListTables: listTables.slice(0, 10)
					});
				} else {
					this.setState({
						loading: false,
					}, this.onShowAlert(res.data.responseDesc));
				}
			}).catch(err => {
				console.error("error GetParamTableList", err);
				this.setState({
					loading: false
				}, this.onShowAlert(err.message));
			});
	}

	onShowAlert = (message) => {
		// console.log("message", message);
		this.setState({
			alertVisible: true,
			alertMessage: message
		});
	}

	onDismissAlert = (key) => {
		this.setState({ [key]: false });
	}

	onChangePage = (current, pageSize) => {
		const { listTables } = this.state;
		this.setState({
			curPage: current,
			curListTables: listTables.slice(pageSize * (current - 1), pageSize * (current))
		});
	}

	render() {
		const { listTables, curListTables, loading, alertVisible, alertMessage, curPage } = this.state;
		return (
			<div className="general-widgets-wrapper">
				<PageTitleBar title="Parameter System Maker" match={this.props.match} />
				<div className="dash-cards">
					<Alert color="danger" isOpen={alertVisible} toggle={() => this.onAlertDismiss("alertVisible")}>
						<b>Error! </b>
						List Parameter Table cannot be loaded.&nbsp;
						{alertMessage}
					</Alert>
					<div className="row">
						{loading &&
							<RctSectionLoader />
						}
						{curListTables.length > 0 ?
							curListTables.map((table, key) => (
								<div key={key} className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
									<CustomCardWidget
										text={upperCase(table) + '\u00A0'}
										bgColor="#FDFEFF"
										to={`/app/main/parameter-system-maker/existing-parameter/${table}`} />
								</div>
							))
							: ""
						}
					</div>
					<div>
						<Pagination
							current={curPage}
							showTotal={(total, range) => `${range[0]} - ${range[1]} of ${total} items`}
							total={listTables.length}
							onChange={this.onChangePage}
							showTitle={false}
						/>
					</div>
				</div>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const token = JSON.parse(authUser.token);
	return { token };
}

export default connect(mapStateToProps)(ListParameterTable);