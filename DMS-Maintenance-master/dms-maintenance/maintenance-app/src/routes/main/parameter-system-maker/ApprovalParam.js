/**
 * Approval Parameter Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class ApprovalParam extends Component {

	state = {
		params: null,
		data: [],
		selectedParam: null,
		loading: true, // loading activity
	}

	componentDidMount = () => {
		const token = this.props.token;
		const reqData = {
			userId: this.props.user.userId
		}
		getApi(token).post('CIMB-Appraisal-Parameter/api/json/reply/GetParamTablePending', reqData)
			.then(res => {
				if (res.data.responseCode == "00") {
					console.log("resParamPending:", res);

					// let groups = res.data.groups;
					// const data = groups.map((group, index) => [
					// 	group.id,
					// 	group.name,
					// 	group.active ? "Yes" : "No",
					// 	group.createBy ? group.createBy : "-",
					// 	convertDateASP(group.createDate, "DD-MM-YYYY"),
					// 	index
					// ]);
					this.setState({
						// groups: groups,
						// data: data,
						loading: false
					});
				} else {
					console.error("error GetParamTablePending", res.data.responseCode + ": " + res.data.responseDesc);
					this.setState({ loading: false });
				}
			}).catch(err => {
				console.error("error GetParamTablePending", err);
				this.setState({ loading: false });
			});
	}

	onClickApprove = (idx) => {
		const { groups } = this.state;
		const selectedGroup = groups[idx];
		const { tempId, name } = selectedGroup;
		this.setState({ loading: true });
		const reqData = {
			id: tempId,
			userId: this.props.user.userId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/ApproveParamTablePending', reqData)
			.then(res => {
				console.log("res:", res.data);
				if (res.data.responseCode == "00") {
					NotificationManager.success(`Param ${name} Approved.`, "Success", getNotifTimeout());
					//RELOAD
					this.componentDidMount();
				} else {
					throw new Error(res.data.responseCode+" "+res.data.responseDesc);
				}
			}).catch(err => {
				console.error("error ApproveParamTablePending", err);
				NotificationManager.error(`Approve Param ${name} Failed. ${err.message}`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	onClickReject = (idx) => {
		const { params: params } = this.state;
		this.refs.rejectConfirmationDialog.open();
		this.setState({ selectedParam: params[idx] });
	}

	rejectParam = () => {
		const { selectedParam } = this.state;
		const { name, tempId } = selectedParam;
		this.refs.rejectConfirmationDialog.close();
		this.setState({ loading: true });
		const reqData = {
			tempId: tempId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Parameter/api/json/reply/RejectParamTablePending', reqData)
			.then(res => {
				// console.log("res:", res.data);
				NotificationManager.success(`Parameter ${name} Rejected.`, "Success", getNotifTimeout());
				//RELOAD
				this.componentDidMount();
			}).catch(err => {
				console.err("error RejectParamTablePending", err);
				NotificationManager.error(`Reject Parameter ${name} Failed.`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	render() {
		const { data, loading, selectedParam } = this.state;
		const columns = [
			"ID",
			"Table Name",
			"Status",
			"Field Name",
			"Field Value",
			"Field Value Before",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
								<Button onClick={() => this.onClickApprove(value)} variant="contained" className="btn-success mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-check"></i> Approve</Button>
								<Button onClick={() => this.onClickReject(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Reject</Button>
							</div>
						);
					}
				}
			}
		];
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="Parameter System - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={"Approval Parameter"}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="rejectConfirmationDialog"
					title={`Are you sure want to reject Parameter ${selectedParam ? selectedParam.name : ""}?`}
					onConfirm={this.rejectParam}
				/>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps)(ApprovalParam);
