/**
 * Material Text Field
 */
import React from "react";
import { connect } from 'react-redux';
import { getApi } from 'Api';
import { NotificationManager } from 'react-notifications';
import {
	Select,
	TextField,
	Checkbox,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	InputLabel,
	MenuItem,
	Button,
} from "@material-ui/core";

// page title bar
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";

// rct card box
import RctCollapsibleCard from "Components/RctCollapsibleCard/RctCollapsibleCard";

// redux action
import { logoutUser } from 'Actions';

class AddNewParamForm extends React.Component {
	state = {
		formData: {
			tableName: "",
			fieldPos: "",
			fieldName: "",
			fieldType: "",
			fieldDesc: "",
			fieldReff: "",
			fieldAuto: "",
			fieldKey: "",
			fieldMan: "",
			groupBy: "",
			status: "",
			createdBy: "",
		},
		formError: {
			tableName: false,
			fieldPos: false,
			fieldName: false,
			fieldType: false,
			fieldDesc: false,
			fieldReff: false,
			fieldAuto: false,
			fieldKey: false,
			fieldMan: false,
			groupBy: false,
			status: false,
			createdBy: false,
		},
		isEdit: false,
	};

	componentDidMount = () => {

	}

	handleChange = event => {
		const { formData } = this.state;
		formData[event.target.name] = event.target.value;
		this.setState({ formData });
	};

	isCanSave = () => {
		const { formData } = this.state;

		this.setState({
			formError: {
				tableName: formData.tableName == "",
				fieldPos: formData.fieldPos == "",
				fieldName: formData.fieldName == "",
				fieldType: formData.fieldType == "",
				fieldDesc: formData.fieldDesc == "",
				fieldReff: formData.fieldReff == "",
				fieldAuto: formData.fieldAuto == "",
				fieldKey: formData.fieldKey == "",
				fieldMan: formData.fieldMan == "",
				groupBy: formData.groupBy == "",
				status: formData.status == "",
				createdBy: formData.createdBy == "",
			}
		})

		return (
			// formData.tableName !== "" &&
			// formData.fieldPos !== "" &&
			// formData.fieldName !== "" &&
			// formData.fieldType !== "" &&
			// formData.fieldDesc !== "" &&
			// formData.fieldReff !== "" &&
			// formData.fieldAuto !== "" &&
			// formData.fieldKey !== "" &&
			// formData.fieldMan !== "" &&
			// formData.groupBy !== "" &&
			// formData.status !== "" &&
			// formData.createdBy !== ""
			true
		)
	}

	handleSave = e => {
		e.preventDefault();
		console.log("groups", this.state.groups);
		if (this.isCanSave()) {
			const { formData } = this.state;

			console.log('formData', formData);
			// getApi(this.props.token).post("/CIMB-Appraisal-Security/api/json/reply/SaveOrUpdateUsersMaker", formData)
			// .then((res) => {
			//     if(res.data.responseCode == "00"){
			//         this.props.history.push("/app/main/user-management-maker/list-user");
			//         NotificationManager.success('User Created Successfully');
			//     }else{
			//         NotificationManager.error('Failed to Create User. '+res.data.responseDesc);
			//     }
			// }).catch(err => {
			//     console.log("error", err);
			//     NotificationManager.error('Failed to Create User');
			// });
		}
	}

	handleBack = e => {
		e.preventDefault();
		this.props.history.goBack();
	}

	render() {
		const { formData, formError, isEdit } = this.state;
		return (
			<div className="textfields-wrapper">
				<PageTitleBar
					title="Parameter System - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard>
					<div className="row d-flex justify-content-between py-10 px-10">
						<div>
							<h2>Add Data</h2>
						</div>
						<div>
							<Button onClick={this.handleSave} variant="contained" className="btn-primary mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-check"></i> Save</Button>
							<Button onClick={this.handleBack} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Cancel</Button>
						</div>
					</div>
					<form noValidate autoComplete="off">
						<div className="row">
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								<div className="form-group">
									<TextField
										required
										id="tableName"
										name="tableName"
										fullWidth
										label="Table Name"
										defaultValue={formData.tableName}
										error={formError.tableName}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="fieldPos"
										name="fieldPos"
										fullWidth
										label="Field Pos"
										defaultValue={formData.fieldPos}
										error={formError.fieldPos}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="fieldName"
										name="fieldName"
										fullWidth
										label="Field Name"
										defaultValue={formData.fieldName}
										error={formError.fieldName}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="fieldType"
										name="fieldType"
										type="fieldType"
										fullWidth
										label="Field Type"
										defaultValue={formData.fieldType}
										error={formError.fieldType}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="fieldDesc"
										name="fieldDesc"
										type="fieldDesc"
										fullWidth
										label="Field Description"
										defaultValue={formData.fieldDesc}
										error={formError.fieldDesc}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="fieldReff"
										name="fieldReff"
										type="fieldReff"
										fullWidth
										label="Field Reff"
										defaultValue={formData.fieldReff}
										error={formError.fieldReff}
										onChange={this.handleChange}
									/>
								</div>
							</div>
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								<div className="form-group">
									<TextField
										required
										id="fieldKey"
										name="fieldKey"
										type="fieldKey"
										fullWidth
										label="Field Key"
										defaultValue={formData.fieldKey}
										error={formError.fieldKey}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="fieldMan"
										name="fieldMan"
										type="fieldMan"
										fullWidth
										label="Field Man"
										defaultValue={formData.fieldMan}
										error={formError.fieldMan}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="fieldAuto"
										name="fieldAuto"
										type="fieldAuto"
										fullWidth
										label="Field Auto"
										defaultValue={formData.fieldAuto}
										error={formError.fieldAuto}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="groupBy"
										name="groupBy"
										type="groupBy"
										fullWidth
										label="Group By"
										defaultValue={formData.groupBy}
										error={formError.groupBy}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="status"
										name="status"
										type="status"
										fullWidth
										label="Status"
										defaultValue={formData.status}
										error={formError.status}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="createdBy"
										name="createdBy"
										type="createdBy"
										fullWidth
										label="Created By"
										defaultValue={formData.createdBy}
										error={formError.createdBy}
										onChange={this.handleChange}
									/>
								</div>
							</div>
						</div>
					</form>
				</RctCollapsibleCard>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps, {
	logoutUser
})(AddNewParamForm);