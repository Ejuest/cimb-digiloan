/**
 * User Management Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { startCase } from 'lodash';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP } from 'Helpers/helpers';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// mui-data-table custom toolbar
import CustomToolbar from "Components/MUIDataTable/CustomToolbar";

class ListExistingParameter extends Component {

	state = {
		data: [],
		tableName: "",
		selectedData: null, // selected user to perform operations
		loading: true, // loading activity,
		rowHeader: []
	}

	componentDidMount = () => {
		const token = this.props.token;
		const { tableName } = this.props.match.params;
		const reqData = {
			tableName: tableName
		}
		getApi(token).post('CIMB-Appraisal-Parameter/api/json/reply/GetParamTable', reqData)
			.then(res => {
				const data = [];
				const rowHeader = [];
				const contents = res.data.content;
				// console.log("content:", contents);
				if(contents && contents.length !== 0){
					Object.keys(contents[0]).forEach(function (key) {
						if (key != "__type" && (typeof contents[0][key] !== 'object' || contents[0][key] == null)) {
							rowHeader.push(startCase(key));
						}
					});
					//regex for checking ASP DATE
					const regexASPDate = /^\/Date\((\d+)(?:-(\d+))?\)/;
					
					contents.forEach((content, idx) => {
						const newData = [];
						Object.keys(content).forEach(function (key) {
							// console.log(key, content[key]);
							// console.log(content[key], typeof content[key]);
							if (key !== "__type") {
								if (typeof content[key] === 'boolean') {
									newData.push(content[key] ? "Yes" : "No");
								} else if (typeof content[key] === 'number') {
									newData.push(content[key]);
								} else if (content[key] == null) {
									newData.push("-");
								} else if (typeof content[key] === 'string') {
									regexASPDate.test(content[key]) ?
										newData.push(convertDateASP(content[key], "DD-MM-YYYY")) :
										newData.push(content[key]) ;
								}
							}
						});
						data.push(newData);
					});
				}

				this.setState({
					tableName: tableName,
					data: data,
					loading: false,
					rowHeader
				});
			})
			.catch(err => console.error("error GetParamTable", err));
	}

	customToolbarOnClick = () => {
		const { history, match } = this.props;
		history.push(`${match.url}/add-data`);
	}

	render() {
		const { data, tableName, loading, selectedData, rowHeader } = this.state;
		const columns = rowHeader;
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none',
			customToolbar: () => {
				return (
					<CustomToolbar tooltipTitle="Add Data" onClick={this.customToolbarOnClick} />
				);
			}
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="Parameter System - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					<MUIDataTable
						title={`Existing Parameter - ${tableName}`}
						data={data}
						columns={columns}
						options={options}
					/>
					{loading &&
						<RctSectionLoader />
					}
				</RctCollapsibleCard>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const token = JSON.parse(authUser.token);
	return { token };
}

export default connect(mapStateToProps)(ListExistingParameter);