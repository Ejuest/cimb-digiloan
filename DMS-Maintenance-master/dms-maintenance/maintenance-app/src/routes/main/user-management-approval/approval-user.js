/**
 * User Management Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class ApprovalUser extends Component {

	state = {
		users: null,
		data: [],
		selectedUser: null, // selected user to perform operations
		loading: true, // loading activity
	}

	componentDidMount() {
		//get list user
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetUsersChecker')
			.then(res => {
				let users = res.data.users;
				// console.log("Users:", users);
				const data = users.map((user, index) => [
					user.id,
					user.name,
					user.email,
					user.group,
					user.branch,
					user.parent,
					user.dormant ? "Yes" : "No",
					user.passwordFailCount,
					user.active ? "Yes" : "No",
					user.createBy,
					convertDateASP(user.createDate, "DD-MM-YYYY"),
					user.isApproved ? "Yes" : "No",
					user.isMaker ? "Yes" : "No",
					user.status,
					index
				]);
				this.setState({
					users: users,
					data: data,
					loading: false
				});
			}).catch(err => {
				console.error("error GetUsersChecker", err);
				this.setState({ loading: false });
				NotificationManager.error(err.message, "Error", getNotifTimeout());
			});
	}

	onClickApprove = (idx) => {
		const {users} = this.state;
		const selectedUser = users[idx];
		this.setState({ loading: true });
		const { name, tempId } = selectedUser;
		const reqData = {
			tempId: tempId,
			userBy: this.props.user.userId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/ApproveUsersChecker', reqData)
			.then(res => {
				console.log("res:", res.data);
				if (res.data.responseCode == "00") {
					NotificationManager.success(`User ${name} Approved.`, "Success", getNotifTimeout());
					//RELOAD
					this.componentDidMount();
				} else {
					throw new Error(res.data.responseCode+" "+res.data.responseDesc);
				}
			}).catch(err => {
				console.error("error ApproveUsersChecker", err);
				NotificationManager.error(`Approve User ${name} Failed. ${err.message}`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	onClickReject = (idx) => {
		const {users} = this.state;
		this.refs.rejectConfirmationDialog.open();
		this.setState({ selectedUser: users[idx] });
	}

	rejectUser = () => {
		this.refs.rejectConfirmationDialog.close();
		const { selectedUser } = this.state;
		const { tempId, name } = selectedUser;
		this.setState({ loading: true });
		const reqData = {
			tempId: tempId,
			userId: this.props.user.userId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/RejectUsersChecker', reqData)
			.then(res => {
				// console.log("res:", res.data);
				NotificationManager.success(`User ${name} Rejected.`, "Success", getNotifTimeout());
				//RELOAD
				this.componentDidMount();
			}).catch(err => {
				console.error("error RejectUsersChecker", err);
				NotificationManager.error(`Reject User ${name} Failed.`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	render() {
		const { data, loading, selectedUser } = this.state;
		const columns = [
			"User ID",
			"Nama Lengkap",
			"Email",
			"Group",
			"Branch",
			"Upliner",
			"Dormant",
			"False Pwd Count",
			"Active",
			"Create By",
			"Create Date",
			"Approved",
			"Maker",
			"Status",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
								<Button onClick={() => this.onClickApprove(value)} variant="contained" className="btn-success mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-check"></i> Approve</Button>
								<Button onClick={() => this.onClickReject(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Reject</Button>
							</div>
						);
					}
				}
			}
		];
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="User Management - Approval"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={"Approval User"}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="rejectConfirmationDialog"
					title={`Are you sure want to reject user ${selectedUser? selectedUser.name: ""}?`}
					onConfirm={() => this.rejectUser()}
				/>
			</div>
		);
	}
}


// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps)(ApprovalUser);
