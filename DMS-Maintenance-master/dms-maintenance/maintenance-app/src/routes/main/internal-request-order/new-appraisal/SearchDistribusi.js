import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import MenuItem from '@material-ui/core/MenuItem';
 

class SearchDistribusi extends React.Component {

    constructor(props) {
        super(props);
    
        this. state = {
            data:[],
            opendialog: false,
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
  
        AparsialApi().get('ORDER/SUSER')
        .then(res => {
            if (res.status == 200) {
                
               const datanew = res.data.map((group, index) => [
                   group.USERID,
                   group.USERNAME,
                   group.GROUPNAME,
                   group.DISTRIBUTIONID,
                   index,
               ]);
            this.setState({
               data:  datanew,
            });
        } }).catch(err => {
            console.error("error SUSER", err);
        });
    }
    handleClickopendialog = () => {
		this.setState({
			opendialog: true,
			fields: {},
			errors: {}
		});
	};
    
    handleCloseData = () => {
		this.setState({ opendialog: false });
		this.setState({
			fields: {},
			errors: {}
		});
    };
    
    onSelectData =(idx)=>{
        const { data } = this.state;
        const selectedGroup = data[idx];
     //   let fields = this.state.fields;
        
        // fields['kodePos'] =  selectedGroup[0];
        // fields['Kecamatan'] =  selectedGroup[1];
        // fields['Kabupaten'] =  selectedGroup[2];
        // fields['Propinsi'] =  selectedGroup[3];
        // fields['Kelurahan'] =  selectedGroup[4];
         this.setState({ opendialog:false});
        // localStorage.setItem("SelectData",JSON.stringify(fields));
       this.props.updateDistribusi(selectedGroup);
        
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        const columnData = ["USERID","USERNAME","VALUER/KJPP" ,
       {
           name: "Action",
           options: {
               filter: false,
               sort: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                   return (
                       <div>
                           <MatButton onClick={() => this.onSelectData(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Select</MatButton>
                       </div>
                   );
               }
           }
       }
   ];
   //  END DT SERTIFIKAT
        return (
            <React.Fragment>
          
              <Dialog maxWidth="md" className="client-dialog" open={this.state.opendialog} onClose={this.handleCloseData} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">DISTRIBUSI</DialogTitle>
                    <DialogContent>
                        <div>
                        <RctCollapsibleCard fullBlock>
                                        {this.state.loading &&
                                            <RctSectionLoader />
                                        }
                                        <MUIDataTable
                                            title="PILIH DISTRIBUSI"
                                            data={this.state.data}
                                            columns={columnData}
                                            options={options}
                                        />
                            </RctCollapsibleCard>
                        </div>
                    </DialogContent>
                </Dialog>
                
                <MatButton variant="contained" color="primary"  onClick={this.handleClickopendialog} className="mt-3 ml-2 text-white">...</MatButton>
        </React.Fragment>
        );
    }
}

export default SearchDistribusi
 