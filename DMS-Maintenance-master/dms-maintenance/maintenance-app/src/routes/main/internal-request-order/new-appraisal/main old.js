/**
 * Miscellaneous Widgets Page
 */

import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
 
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';

 //page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

 //rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

 //rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

function TabContainer({ children }) {
    return (
        <Typography component="div">
            {children}
        </Typography>
    );
}

 
 
export default class InternalRequestOrderNewAppraisalMain extends Component {
    state = {
        activeIndex: 0,
        selectedDate: moment(),
        openzipcode: false,
        opensertifikat :false,
        openpilihdistribusi: false,
        dokType:[],
        jenisJaminan:[],
        jenisFasilitas:[],
        cabang:[],
        jenisPermintaanTransaksi:[],
        jenisPenilaian:[],
        reason:[],
        zipdata:[],
        dataCert:[],
        fields: {},
        fieldsertifikat:{},
        errors: {},
        loading: false
    }
    
    componentDidMount() {
        
        AparsialApi().get('RF_CERTIFICATE_TYPE/Get')
        .then(res => {
            if (res.status == 200) {
               
            this.setState({
                dokType: res.data,
            });
        }
        }).catch(err => {
            console.error("error RF_CERTIFICATE_TYPE", err);
        });

         AparsialApi().get('RF_FACILITY/Get')
         .then(res => {
             if (res.status == 200) {
         
             this.setState({
                 jenisFasilitas:  res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_FACILITY", err);
         });


         AparsialApi().get('RF_TYPE_OF_GUARANTEE/Get')
         .then(res => {
             if (res.status == 200) {
                
             this.setState({
                 jenisJaminan: res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_TYPE_OF_GUARANTEE", err);
         });

         AparsialApi().get('RFBRANCH/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
          
             this.setState({
                 cabang: resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RFBRANCH", err);
         });

         AparsialApi().get('RF_FLAG_APPRAISAL/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
                 
             this.setState({
                 jenisPermintaanTransaksi: resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RF_FLAG_APPRAISAL", err);
         });

         AparsialApi().get('RF_ASSESSMENT_TYPE/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
                
             this.setState({
                 jenisPenilaian: resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RF_ASSESSMENT_TYPE", err);
         });

        
         AparsialApi().get('RF_REASON/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
                 
             this.setState({
                 reason:  resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RF_REASON", err);
         });

         AparsialApi().get('RFZIPCODE/Get')
         .then(res => {
             if (res.status == 200) {
                 
                const datanew = res.data.map((group, index) => [
					group.ziP_CODE,
                    group.province,
                    group.city,
                    group.district,
                    group.subdistrict,
                    group.ziP_DESC,
                    index,
                ]);
             this.setState({
                zipdata:  datanew,
             });
         }
         }).catch(err => {
             console.error("error RF_REASON", err);
         });
         
        
    }

    handleClickopenzipcode = () => {
		this.setState({
			openzipcode: true,
			fields: {},
			errors: {}
		});
	};
    
    handleCloseZipCode = () => {
		this.setState({ openzipcode: false });
		this.setState({
			fields: {},
			errors: {}
		});
    };

    handleClickopensertifikat = () => {
		this.setState({
			opensertifikat: true,
			fields: {},
			errors: {}
		});
	};
 
    handleClosesetifikat = () => {
		this.setState({ opensertifikat: false });
		this.setState({
			fields: {},
			errors: {}
		});
    };


    handleClickopenPilihDistribusi = () => {
        console.log("clicked");
		this.setState({
			openpilihdistribusi: true,
			fields: {},
			errors: {}
		});
	};
 
    handleClosePilihDistribusi = () => {
		this.setState({ openpilihdistribusi: false });
		this.setState({
			fields: {},
			errors: {}
		});
    };

     
   /// submit form data
	onFormSubmit(e) {
		e.preventDefault();
        alert("Form has test.")
    }

    onClickEdit = (idx) => {
		 const { users } = this.state;
		 this.props.history.push(`/app/main/user-management-maker/list-user/${users[idx].id}`);
	}

    onSelectZipcode =(idx)=>{
        const { zipdata } = this.state;
        const selectedGroup = zipdata[idx];
        let fields = this.state.fields;
        
        fields['kodePos'] =  selectedGroup[0];
        fields['Kecamatan'] =  selectedGroup[1];
        fields['Kabupaten'] =  selectedGroup[2];
        fields['Propinsi'] =  selectedGroup[3];
        fields['Kelurahan'] =  selectedGroup[4];
        this.setState({ fields ,openzipcode:false});
        
    }
	/**
	 * On Delete
	 */
	onClickDelete = (idx) => {
		 const { users } = this.state;
		 this.refs.deleteConfirmationDialog.open();
		 this.setState({ selectedUser: users[idx] });
    }
 
    handleChange(event, value) {
        const menu = ["main","document-checking","payment"];
        this.props.history.push(`/app/main/internal-request-order/new-appraisal/${menu[value]}/1`);
        this.setState({ activeIndex: value });
    }
	handleDateChange = (date) => {
		this.setState({ selectedDate: date });
	};

 
      handletextChange(field, e) {
		let fields = this.state.fields;
		fields[field] = e.target.value;
		this.setState({ fields });
    }

    handletextChangeSertifikat(field, e) {
		let fieldsertifikat = this.state.fields;
		fieldsertifikat[field] = e.target.value;
		this.setState({ fieldsertifikat });
    }

    
    removeSertifikat(idx) {
        let  j  = this.state.dataCert.filter(function(l) {
            return l[1] == (idx);
          });
        
          const items = this.state.dataCert.splice([j], 1);
        this.setState({
            dataCert: items
         });
      
         
       }

	//submit form data
	onFormSubmitSertifikat(e) {
		e.preventDefault();
	//	if (this.handleValidation()) {
        const { dataCert } = this.state;
        
        const datanew =  [
            this.state.fieldsertifikat['DokType'],
            this.state.fieldsertifikat['NoDokumen'],
            this.state.fieldsertifikat['NIB'],
            this.state.fieldsertifikat['AtasNama'],
            this.state.fieldsertifikat['NoDokumen'],
        ];
			 dataCert.push(datanew);
			this.setState({ dataCert,opensertifikat: false });
	//	} else {
	//		alert("Form has errors.")
	//	}
	}
     
      
   
    render() {
        const { activeIndex,
            selectedDate,
            openzipcode,
            opensertifikat,
            openpilihdistribusi, 
            fieldsertifikat,
            errors,
            loading ,
            dokType,
            jenisJaminan,
            jenisFasilitas,
            cabang,
            jenisPermintaanTransaksi,
            jenisPenilaian,
            reason,
            zipdata,
            dataCert,
        } = this.state;
        const extendUrl = "/app/main/internal-request-order";
       //  START DT SERTIFIKAT
        const data = [
            ["HGB", "121323", "1212121", "Napoleon"],
            ["SHM", "333434", "qweqe", "Bernat"],
			 
        ]
        const columns = ["Type Sertifikat", "No Sertifikat", "NIB", "Nama Pemegang Hak",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Edit</MatButton>
                                <MatButton onClick={() => this.removeSertifikat(value)} variant="contained" color="primary" className="btn-danger mr-10 mb-10 text-white btn-icon">Delete</MatButton>
							</div>
						);
					}
				}
			}
        ];
       //  END DT SERTIFIKAT
        // START DT DEDUP
        const dataDedup = [
            ["Masih in progress", "Exact Match", "Lala", "012345", "Create LPJ", "CIMB Pusat", "False"],
            ["Match Cancel", "Exact Match", "Lili", "012345","Appraisal Support","CIMB Pusat","False"],
			 
        ];

        const dataassygn =[["01/01/2020", "Internal KPPI","System"],["01/12/2020", "Exsternal KPPI","System"]];
        const coldataassygn=["Tgl Assignment", "Valuer","Assign Oleh"];
        const columnsDedup = ["Kriteria", "Match Level", "Nama", "No Referensi", "Status", "Cabang", "Linked",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">View</MatButton>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="btn-danger mr-10 mb-10 text-white btn-icon">Confirm</MatButton>
							</div>
						);
					}
				}
			}
        ];
       //  END DT DEDUP
        const datazipcode =[["10110","DKI Jakarta","Jakarta Pusat","Gambir","Gambir","Jakarta Pusat"]
                            ,["10120","DKI Jakarta","Jakarta Pusat","Gambir","Kebon Kelapa","Jakarta Pusat"]];
       const columnzipcode = ["ZipCode","Propinsi","Kota","Kecamatan","Kelurahan","Dati II",
       {
           name: "Action",
           options: {
               filter: false,
               sort: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                   return (
                       <div>
                           <MatButton onClick={() => this.onSelectZipcode(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Select</MatButton>
                       </div>
                   );
               }
           }
       }
   ];

        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
        return (
            
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Main" match={this.props.match} />
                {/* START DIALOG KODE POS */}
                <Dialog maxWidth="md" className="client-dialog" open={this.state.openzipcode} onClose={this.handleCloseZipCode} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">KODE POS</DialogTitle>
                    <DialogContent>
                        <div>
                        <RctCollapsibleCard fullBlock>
                                        {loading &&
                                            <RctSectionLoader />
                                        }
                                        <MUIDataTable
                                            title="PILIH LOKASI"
                                            data={zipdata}
                                            columns={columnzipcode}
                                            options={options}
                                        />
                            </RctCollapsibleCard>
                        </div>
                    </DialogContent>
                </Dialog>
                {/* END DIALOG KODE POS */}
                {/* START DIALOG PILIH DISTRIBUSI */}
                <Dialog className="client-dialog" open={this.state.openpilihdistribusi} onClose={this.handleClosePilihDistribusi} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">PILIH DISTRIBUSI</DialogTitle>
                    <DialogContent>
                        <div>
                            <form onSubmit={this.onFormSubmit.bind(this)}>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                    awd
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                        awd
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                    adw
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-lg-12">
                                        awd
                                    </div>
                                </div>
                                <div className="pt-25 text-right">
                                    <MatButton variant="contained" onClick={this.handleClosePilihDistribusi} className="btn-danger mr-15 text-white">Cancel</MatButton>
                                    <MatButton className="btn-success text-white text-capitalize" type="submit">Submit</MatButton>
                                </div>
                            </form>
                        </div>
                    </DialogContent>
                </Dialog>
                {/* END DIALOG PILIH DISTRIBUSI */}
                {/* START DIALOG SERTIFIKAT */}
                <Dialog className="client-dialog" open={this.state.opensertifikat} onClose={this.handleClosesetifikat} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">SERTIFIKAT</DialogTitle>
					<DialogContent>
						<div>
							<form onSubmit={this.onFormSubmitSertifikat.bind(this)}>
								<div className="row">
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                        <TextField id="TdokType" select label="Tipe Dokumen"
                                            value={fieldsertifikat["DokType"]}
                                            onChange={this.handletextChangeSertifikat.bind( this,'DokType')}
                                            SelectProps={{
                                                MenuProps: {
                                                },
                                            }}
                                            helperText="Tipe Dokumen"
                                            fullWidth>
                                            {dokType.map(option => (
                                            <MenuItem key={option.id} value={option.name}>
                                                {option.name}
                                            </MenuItem>
                                            ))}
                                        </TextField>
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                        <TextField id="noDokumen" fullWidth label="No Dokumen"   
                                        onChange={this.handletextChangeSertifikat.bind( this,'NoDokumen')}  
                                        value={ fieldsertifikat["NoDokumen"] ?  fieldsertifikat["NoDokumen"] : ''}/>
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                        <TextField id="nib" fullWidth label="NIB" value=""  
                                         onChange={this.handletextChangeSertifikat.bind( this,'NIB')} 
                                        value={ fieldsertifikat["NIB"] ?  fieldsertifikat["NIB"] : ''} />
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12">
                                        <TextField id="atasNama" fullWidth label="Atas Nama" 
                                        onChange={this.handletextChangeSertifikat.bind( this,'AtasNama')} 
                                        value={ fieldsertifikat["AtasNama"] ?  fieldsertifikat["AtasNama"] : ''} />
									</div>
								</div>
								<div className="pt-25 text-right">
									<MatButton variant="contained" onClick={this.handleClosesetifikat} className="btn-danger mr-15 text-white">
										Cancel
            		                </MatButton>
									<MatButton className="btn-success text-white text-capitalize" type="submit">Submit</MatButton>
								</div>
							</form>
						</div>
					</DialogContent>
				</Dialog>
                {/* END DIALOG SERTIFIKAT */}
                <Tabs value={activeIndex} onChange={(e, value) => this.handleChange(e, value)}>
                    <Tab label="Main" />
                    <Tab label="Document Checking" />
                    <Tab label="Payment" />
                </Tabs>
                <TabContainer>
                    <RctCollapsibleCard className="text-danger" heading="APPLICATION DATA">
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField id="noPermohonan" fullWidth label="No Permohonan" value="" />
                                </div>
                                <div className="form-group">
                                    <DatePicker
                                        label="Tanggal Permohonan"
                                        id="tanggalPermohonan"
                                        value={selectedDate}
                                        onChange={this.handleDateChange}
                                        animateYearScrolling={false}
                                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                        fullWidth
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisPermintaanTransaksi" select label="Jenis Permintaan Transaksi"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Permintaan Transaksi"
                                        fullWidth>
                                        {jenisPermintaanTransaksi.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisFasilitas" select label="Jenis Fasilitas"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Fasilitas"
                                        fullWidth>
                                        {jenisFasilitas.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="namaDebitur" fullWidth label="Nama Debitur" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="nilaiExposure" fullWidth label="Nilai Exposure" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisJaminan" select label="Jenis Jaminan"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Jaminan"
                                        fullWidth>
                                        {jenisJaminan.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="deskripsiJaminan" fullWidth label="Deskripsi Jaminan" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="alamatJaminan1" fullWidth label="Alamat Jaminan" value="" />
                                    <TextField id="alamatJaminan2" fullWidth label="" value="" />
                                    <TextField id="alamatJaminan3" fullWidth label="" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField disabled id="kodePos" label="Kode Pos" onChange={this.handletextChange.bind(this, "kodePos")} value={this.state.fields["kodePos"] ?  this.state.fields["kodePos"] : ''} readOnly />
                                    <MatButton variant="contained" color="primary"  onClick={this.handleClickopenzipcode} className="mt-3 ml-2 text-white">...</MatButton>
                                </div>
                                <div className="form-group">
                                    <TextField id="kelurahan" fullWidth label="Kelurahan"  onChange={this.handletextChange.bind(this, "Kelurahan")} value={ this.state.fields["Kelurahan"] ?  this.state.fields["Kelurahan"] : ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="kecamatan" fullWidth label="Kecamatan" onChange={this.handletextChange.bind(this, "Kecamatan")} value={ this.state.fields["Kecamatan"] ?  this.state.fields["Kecamatan"] : ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="kabupaten" fullWidth label="Kabupaten" onChange={this.handletextChange.bind(this, "Kabupaten")} value={ this.state.fields["Kabupaten"] ?  this.state.fields["Kabupaten"] : ''} readOnly  />
                                </div>
                                <div className="form-group">
                                    <TextField id="propinsi" fullWidth label="Propinsi" onChange={this.handleDateChange.bind(this, "Propinsi")} value={ this.state.fields["Propinsi"] ?  this.state.fields["Propinsi"] : ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <FormLabel component="legend">VIP</FormLabel>
                                    <FormControlLabel control={
                                        <Checkbox color="primary" value="1" />
                                    } label="Ya"
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisPenilaian" select label="Jenis Penilaian"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {jenisPenilaian.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <RctCollapsibleCard fullBlock>
                                        {loading &&
                                            <RctSectionLoader />
                                        }
                                        <MUIDataTable
                                            title=""
                                            data={dataassygn}
                                            columns={coldataassygn}
                                            options={options}
                                        />
                                    </RctCollapsibleCard>
                                </div>
                                <div className="form-group">
                                    <FormLabel component="legend">Distribusi Manual</FormLabel>
                                    <FormControlLabel control={
                                        <Checkbox color="primary" value="1" />
                                    } label=""
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField disabled id="pilihDistribusi" label="Pilih Distribusi" value="" />
                                    <MatButton variant="contained" color="primary" onClick={()=> this.handleClickopenPilihDistribusi()} className="mt-3 ml-2 text-white">...</MatButton>
                                </div>
                                
                            </div>
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField id="namaPIC1" fullWidth label="Nama PIC 1" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="noWaPIC1" fullWidth label="No WA PIC 1" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="noTelpPIC1" fullWidth label="No Telp PIC 1" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="namaPIC2" fullWidth label="Nama PIC 2" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="noWaPIC2" fullWidth label="No WA PIC 2" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="noTelpPIC2" fullWidth label="No Telp PIC 2" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="Tcabang" select label="Cabang"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="cabang"
                                        fullWidth>
                                        {cabang.map(option => (
                                        <MenuItem key={option.branchid} value={option.branchname}>
                                            {option.branchname}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="marketingOfficer" fullWidth label="Marketing Officer" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="NoTelpMarketing" fullWidth label="No Telp Marketing" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve1" fullWidth label="Reserve 1" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve2" fullWidth label="Reserve 2" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve3" fullWidth label="Reserve 3" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve4" fullWidth label="Reserve 4" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve5" fullWidth label="Reserve 5" value="" />
                                </div>
                            </div>
                        </div>
                    </RctCollapsibleCard>
                    <RctCollapsibleCard className="text-danger" heading="DAFTAR SERTIFIKAT">
                    <   RctCollapsibleCard fullBlock>
                            {loading &&
                                <RctSectionLoader />
                            }
                            <MUIDataTable
                                title={<div><MatButton onClick={() => this.handleClickopensertifikat()} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon"> ADD NEW</MatButton></div>}
                                data={dataCert}
                                columns={columns}
                                options={options}
                            />
                        </RctCollapsibleCard>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SIMPAN</MatButton>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">DEDUP</MatButton>
                    </RctCollapsibleCard>
                    <RctCollapsibleCard className="text-danger" heading="DEDUP RESULT">
                    <   RctCollapsibleCard fullBlock>
                            {loading &&
                                <RctSectionLoader />
                            }
                            <MUIDataTable
                                title=""
                                data={dataDedup}
                                columns={columnsDedup}
                                options={options}
                            />
                        </RctCollapsibleCard>
                        <div className="form-group">
                            <TextField id="Reason" select label="Reason"
                                value=""
                                SelectProps={{
                                    MenuProps: {
                                    },
                                }}
                                helperText="Reason"
                                fullWidth>
                                {reason.map(option => (
                                <MenuItem key={option.id} value={option.name}>
                                    {option.name}
                                </MenuItem>
                                ))}
                            </TextField>
                        </div>
                        <div className="form-group">
                            <TextField id="remarks" fullWidth label="Remarks" multiline rows="4" defaultValue="" />
                        </div>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SEND BACK</MatButton>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">PROCEED</MatButton>
                    </RctCollapsibleCard>
                </TabContainer>
                
            </div>
            
      
         
        );
    }
}