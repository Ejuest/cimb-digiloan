import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import MenuItem from '@material-ui/core/MenuItem';

export default class AddCertificate extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            dataCert:[],
            dokType:[],
            opensertifikat :false,
            Edit :false,
            fieldsertifikat:{},
            mandatory:[],
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
        AparsialApi().get('RF_CERTIFICATE_TYPE/Get')
        .then(res => {
            if (res.status == 200) {
                fieldsertifikat = this.state.fieldsertifikat;
             
            this.setState({
                dokType: res.data,
            });
          //  localStorage.setItem("dataCert",JSON.stringify(dataCert));
        }
        }).catch(err => {
            console.error("error RF_CERTIFICATE_TYPE", err);
        });

        AparsialApi().get('RF_SCREEN_MANDATORY/Get',{
            params: {
                screen: "AddCertificate"
            }
          })
         .then(res => {
             if (res.status == 200) {
                
             this.setState({
                mandatory: res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_SCREEN_MANDATORY", err);
         });
        
        if( localStorage.getItem('CERTIFICATE') != "")
        {
            try
            {
                let cert = JSON.parse(localStorage.getItem('CERTIFICATE'));
           
                const datanew = cert.map((group, index) => [
                    group.CERTIFICATE_TYPE_ID,
                    group.CERTIFICATE_NO,
                    group.NIB,
                    group.NAME_OF_HOLDERS,
                    group.CERTIFICATE_NO,
                ]);
    
                this.setState({ dataCert:datanew });
            }catch{}
           
        }
    }
 

    //  componentDidUpdate(prevProps) {
    //     if (prevProps.dataCert.length !== this.props.dataCert.length) {
    //        this.getFavClient();
    //        this.getRecentClient();
    //     }
    //  }
  
     // get favourite client data
     getFavClient() {
        let newArray = [];
        let data = this.props.dataCert;
        if (data !== null) {
           for (let Item of data) {
              if (Item.type === 'favourite') {
                 newArray.push(Item)
              }
           }
           this.setState({
            dataCert: newArray,
              isUpdated:false
           })
        }
     }

        // get recent client data
   getRecentClient() {
    let newArray = [];
    let data = this.props.clientsData;
    if (data !== null) {
       for (let Item of data) {
          if (Item.type === 'recently_added') {
             newArray.push(Item)
          }
       }
       this.setState({
        dataCert: newArray,
          isUpdated:false
       })
    }
 }


    handleClosesetifikat = () => {
		this.setState({ opensertifikat: false });
		this.setState({
			fields: {},
            errors: {},
            Edit:false
		});
    };



    handleClickopensertifikat = () => {
		this.setState({
			opensertifikat: true,
            fieldsertifikat:{},
			errors: {}
		});
    };
    
    handletextChangeSertifikat(field, e) {
		let fieldsertifikat = this.state.fieldsertifikat;
        fieldsertifikat[field] = e.target.value;
		this.setState({ fieldsertifikat });
    }

    removeSertifikat(idx) {
        let  j  = this.state.dataCert.filter(function(l) {
            return l[1] == (idx);
          });

         
         if(this.state.dataCert.length >1)
          {
            const items = this.state.dataCert.splice([j], 1);
            this.setState({
                dataCert: items
             });
             let app = JSON.parse(localStorage.getItem('APPLICATION'));
             const datanew =[];
             dataCert.map((group, index) => 
             datanew.push({
               ID: group.ID || app.ID,
               APPLICATION_ID :app.ID ,
               CERTIFICATE_TYPE_ID:group[0],
               CERTIFICATE_NO:group[1],
               NIB:group[2],
               NAME_OF_HOLDERS:group[3],
               LAND_AREA_M2:'',
               EXP_DATE:'',
               GS_SU_NO:'',
               GS_SU_DATE:'',
               REL_WITH_CUST:'',
                
             }));
              localStorage.setItem("CERTIFICATE",JSON.stringify(datanew));
          }
          else
          {
            this.setState({
                dataCert: [],
             });
             localStorage.setItem("CERTIFICATE",JSON.stringify(dataCert));
          } 

       
     // this.props.onDelete(this.state,idx);
         
       }

       onClickEdit(idx){
        let  j  = this.state.dataCert.filter(function(l) {
            return l[1] == (idx);
          });
          let Nfieldsertifikat={};
          Nfieldsertifikat['DokType']=j[0][0];
          Nfieldsertifikat['NoDokumen']=j[0][1];
          Nfieldsertifikat['NIB']=j[0][2];
          Nfieldsertifikat['AtasNama'] =j[0][3];
      
        this.setState({
            fieldsertifikat:Nfieldsertifikat,
            Edit: true,
            opensertifikat :true
         });
         
       }

	//submit form data
	onFormSubmitSertifikat(e) {
        e.preventDefault();
      let valid = true;
      const { dataCert, Edit,errors,mandatory,fieldsertifikat } = this.state;
        Object.keys(mandatory).forEach(function(key) {
            //arr.push(key);
            if( fieldsertifikat[key] !== undefined)
            {
                if(fieldsertifikat[key] =="")
                  {
                    valid =false;
                    errors[key] ="Cannot be empty";
                  }
            }
            else
            {
                valid =false;
                errors[key] ="Cannot be empty";
            }
        });

    this.setState({ errors});

    if(valid)
    {
    if(!Edit)
    {
        const datanew =  [
             fieldsertifikat['DokType'],
             fieldsertifikat['NoDokumen'],
             fieldsertifikat['NIB'],
             fieldsertifikat['AtasNama'],
             fieldsertifikat['NoDokumen'],
        ];
        //this.props.onAdd(this.state.fieldsertifikat);
		   dataCert.push(datanew);
             this.setState({ dataCert ,opensertifikat: false });
    }
    else
    {  let newclientsData = [];
        for (const item of dataCert) {
            if (item[1] ===  fieldsertifikat['NoDokumen']) {
               item[0] = fieldsertifikat['DokType'];
               item[1] =  fieldsertifikat['NoDokumen'];
               item[2] =  fieldsertifikat['NIB'];
               item[3] =  fieldsertifikat['AtasNama'];
               item[4] =  fieldsertifikat['NoDokumen'];
            }
            newclientsData.push(item)
         }
         this.setState({ dataCert:newclientsData ,opensertifikat: false ,Edit:false});
    }
    
   let app = JSON.parse(localStorage.getItem('APPLICATION'));
   const datanew =[];
     dataCert.map((group, index) => 
     datanew.push({
       ID:group.ID || app.ID,
       APPLICATION_ID :app.ID ,
       CERTIFICATE_TYPE_ID:group[0],
       CERTIFICATE_NO:group[1],
       NIB:group[2],
       NAME_OF_HOLDERS:group[3],
       LAND_AREA_M2:'',
       EXP_DATE:'',
       GS_SU_NO:'',
       GS_SU_DATE:'',
       REL_WITH_CUST:'',
        
     }));
  
     localStorage.setItem("CERTIFICATE",JSON.stringify(datanew));
    }
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        const columns = ["Type Sertifikat", "No Sertifikat", "NIB", "Nama Pemegang Hak",
        {
            name: "Action",
            options: {
                filter: false,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <div>
                            <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Edit</MatButton>
                            <MatButton onClick={() => this.removeSertifikat(value)} variant="contained" color="primary" className="btn-danger mr-10 mb-10 text-white btn-icon">Delete</MatButton>
                        </div>
                    );
                }
            }
        }
    ];
   //  END DT SERTIFIKAT
        return (
            <React.Fragment>
             {/* START DIALOG SERTIFIKAT */}
             <Dialog className="client-dialog" open={this.state.opensertifikat} onClose={this.handleClosesetifikat} aria-labelledby="form-dialog-title">
             <DialogTitle id="form-dialog-title">SERTIFIKAT</DialogTitle>
             <DialogContent>
                 <div>
                     <form onSubmit={this.onFormSubmitSertifikat.bind(this)}>
                         <div className="row">
                             <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                 <TextField id="TdokType" select label="Tipe Dokumen"
                                     value={this.state.fieldsertifikat["DokType"] ||''}
                                     onChange={this.handletextChangeSertifikat.bind( this,'DokType')}
                                     style={{backgroundColor: this.state.mandatory.DokType || ''}}
                                     error={this.state.errors.DokType ? true : false}
                                     SelectProps={{
                                         MenuProps: {
                                         },
                                     }}
                                     helperText="Tipe Dokumen"
                                     fullWidth>
                                     {this.state.dokType.map(option => (
                                     <MenuItem key={option.ID} value={option.NAME}>
                                         {option.NAME}
                                     </MenuItem>
                                     ))}
                                 </TextField>
                             </div>
                             <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                 <TextField type="number" id="noDokumen" fullWidth label="No Dokumen" 
                                     style={{backgroundColor: this.state.mandatory.NoDokumen || ''}}
                                     error={this.state.errors.NoDokumen ? true : false}  
                                 onChange={this.handletextChangeSertifikat.bind( this,'NoDokumen')}  
                                 value={ this.state.fieldsertifikat["NoDokumen"] ?  this.state.fieldsertifikat["NoDokumen"] : ''}/>
                             </div>
                             <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                 <TextField type="number" id="nib" fullWidth label="NIB" value=""  
                                  style={{backgroundColor: this.state.mandatory.NIB || ''}}
                                  error={this.state.errors.NIB ? true : false}  
                                  onChange={this.handletextChangeSertifikat.bind( this,'NIB')} 
                                 value={ this.state.fieldsertifikat["NIB"] ?  this.state.fieldsertifikat["NIB"] : ''} />
                             </div>
                             <div className="col-sm-12 col-md-12 col-lg-12">
                                 <TextField id="atasNama" fullWidth label="Atas Nama" 
                                  style={{backgroundColor: this.state.mandatory.AtasNama || ''}}
                                  error={this.state.errors.AtasNama ? true : false}  
                                 onChange={this.handletextChangeSertifikat.bind( this,'AtasNama')} 
                                 value={ this.state.fieldsertifikat["AtasNama"] ?  this.state.fieldsertifikat["AtasNama"] : ''} />
                             </div>
                         </div>
                         <div className="pt-25 text-right">
                             <MatButton variant="contained" onClick={this.handleClosesetifikat} className="btn-danger mr-15 text-white">
                                 Cancel
                             </MatButton>
                             <MatButton className="btn-success text-white text-capitalize" type="submit">Submit</MatButton>
                         </div>
                     </form>
                 </div>
             </DialogContent>
         </Dialog>
         {/* END DIALOG SERTIFIKAT */}
            < RctCollapsibleCard fullBlock>
                    { this.state.loading &&
                        <RctSectionLoader />
                    }
                    <MUIDataTable
                        title={<div><MatButton onClick={() => this.handleClickopensertifikat()} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon"> ADD NEW</MatButton></div>}
                        data={this.state.dataCert}
                        columns={columns}
                        options={options}
                    />
                </RctCollapsibleCard>
            </React.Fragment>
        );
    }
}
