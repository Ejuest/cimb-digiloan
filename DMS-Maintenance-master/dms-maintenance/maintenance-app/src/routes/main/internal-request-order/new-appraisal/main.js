/**
 * Miscellaneous Widgets Page
 */

import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import axios from 'axios';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
 
 //page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

 //rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

 //rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';
import { NotificationManager } from 'react-notifications';

import AddCertificate from 'Routes/main/internal-request-order/new-appraisal/AddCertificate';
import AddDedup from 'Routes/main/internal-request-order/new-appraisal/AddDedup';

import SearchZipcode from 'Routes/main/internal-request-order/new-appraisal/SearchZipcode';
import AssesmentHistory from 'Routes/main/internal-request-order/new-appraisal/AssesmentHistory';
import SearchDistribusi from 'Routes/main/internal-request-order/new-appraisal/SearchDistribusi';
function TabContainer({ children }) {
    return (
        <Typography component="div">
            {children}
        </Typography>
    );
}

 
 
export default class InternalRequestOrderNewAppraisalMain extends Component {
    state = {
        activeIndex: 0,
        activeMainIndex: 0,
        selectedDate: moment(),
        openpilihdistribusi: false,
        APPLICATION :[],
        APPFLAG :[],
        DEBITUR:[],
        ASSESSMENT_HISTORY:[],
        CERTIFICATE:[],
        DEDUPRESULT:[],
        jenisJaminan:[],
        jenisFasilitas:[],
        cabang:[],
        jenisPermintaanTransaksi:[],
        jenisPenilaian:[],
        dataassygn:[],
        reason:[],
        fields: {},
        fieldsertifikat:{},
        mandatory:[],
        errors: {},
        showdedup:false,
        loading: false
    }
    constructor(props, context) {
        super(props, context);
    }
    
    
    componentDidMount() {

         AparsialApi().get('RF_FACILITY/Get')
         .then(res => {
             if (res.status == 200) {
         
             this.setState({
                 jenisFasilitas:  res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_FACILITY", err);
         });


         AparsialApi().get('RF_TYPE_OF_GUARANTEE/Get')
         .then(res => {
             if (res.status == 200) {
                
             this.setState({
                 jenisJaminan: res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_TYPE_OF_GUARANTEE", err);
         });

         AparsialApi().get('RFBRANCH/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
          
             this.setState({
                 cabang: resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RFBRANCH", err);
         });

         AparsialApi().get('RF_FLAG_APPRAISAL/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
                 
             this.setState({
                 jenisPermintaanTransaksi: resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RF_FLAG_APPRAISAL", err);
         });

         AparsialApi().get('RF_ASSESSMENT_TYPE/Get')
         .then(res => {
             if (res.status == 200) {
                
             this.setState({
                jenisPenilaian: res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_ASSESSMENT_TYPE", err);
         });

         AparsialApi().get('RF_SCREEN_MANDATORY/Get',{
            params: {
                screen: "main"
            }
          })
         .then(res => {
             if (res.status == 200) {
                
             this.setState({
                mandatory: res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_SCREEN_MANDATORY", err);
         });


         if(localStorage.getItem('SelectApp') != null)
         {
             if(localStorage.getItem('APPLICATION') == null)
             {
             let dataapp = JSON.parse(localStorage.getItem('SelectApp'));
            
             AparsialApi().get('ORDER/Get', {
                params: {
                    APPLICATION_NO: dataapp[0].noapplikasi
                }
              })
             .then(res => {
                 if (res.status == 200) {
                    
                    let app = res.data.APPLICATION;
                    let flag = res.data.APPFLAG;
                    let deb = res.data.DEBITUR;
                    let asses = res.data.ASSESSMENT_HISTORY;
                    let cert = res.data.CERTIFICATE;
                    let ded = res.data.DEDUPRESULT;
                    let gethis = asses.filter(l => {
                        return l.ASSESSMENT_TYPE_ID.match(app.ASSESSMENT_TYPE_ID);
                      });

                      let ashistory = gethis.map((group, index) => [
                        convertDateASP(group.ASSIG_DATE, "DD-MM-YYYY"),
                        group.VALUER,
                        group.ASSIG_BY,
                    ]);
                    
                    this.setState({
                        APPLICATION :app,
                        APPFLAG:flag,
                        DEBITUR:deb,
                        ASSESSMENT_HISTORY:asses,
                        CERTIFICATE:cert,
                        DEDUPRESULT:ded,
                        dataassygn:ashistory,
                    });
                  localStorage.setItem('APPLICATION',JSON.stringify( app));
                  localStorage.setItem('APPFLAG',JSON.stringify(flag));
                  localStorage.setItem('DEBITUR',JSON.stringify(deb));
                  localStorage.setItem('ASSESSMENT_HISTORY',JSON.stringify(asses));
                  localStorage.setItem('CERTIFICATE',JSON.stringify(cert));
                  localStorage.setItem('DEDUPRESULT',JSON.stringify(ded));
            
                }
                }).catch(err => {
                    console.error("error RF_ASSESSMENT_TYPE", err);
                });
            }
            else
            {
              let  app = JSON.parse(localStorage.getItem('APPLICATION'));
              let  flag = JSON.parse(localStorage.getItem('APPFLAG'));
              let  deb = JSON.parse(localStorage.getItem('DEBITUR'));
              let  asses = JSON.parse(localStorage.getItem('ASSESSMENT_HISTORY'));
              let  cert = JSON.parse(localStorage.getItem('CERTIFICATE'));
              let  ded = JSON.parse(localStorage.getItem('DEDUPRESULT'));

              let gethis = asses.filter(l => {
                return l.ASSESSMENT_TYPE_ID.match(app.ASSESSMENT_TYPE_ID);
              });

              let ashistory = gethis.map((group, index) => [
                convertDateASP(group.ASSIG_DATE, "DD-MM-YYYY"),
                group.VALUER,
                group.ASSIG_BY,
            ]);

                this.setState({
                    APPLICATION :app,
                    APPFLAG:flag,
                    DEBITUR:deb,
                    ASSESSMENT_HISTORY:asses,
                    CERTIFICATE:cert,
                    DEDUPRESULT:ded,
                    selectedDate:app.DATE_OF_APPLICATION,
                    dataassygn:ashistory,
                });
            
            }
           
         }
        
    }

    handleshowdedup = () =>{
      
        this.setState({
			showdedup: true
		});
    }
  
    handleClickopenPilihDistribusi = () => {
		this.setState({
			openpilihdistribusi: true
		});
	};
 
    handleClosePilihDistribusi = () => {
		this.setState({
            openpilihdistribusi: false,
		
		});
    };

     
  
    onClickEdit = (idx) => {
		 const { users } = this.state;
		 this.props.history.push(`/app/main/user-management-maker/list-user/${users[idx].id}`);
	}

	/**
	 * On Delete
	 */
	onClickDelete = (idx) => {
		 const { users } = this.state;
		 this.refs.deleteConfirmationDialog.open();
		 this.setState({ selectedUser: users[idx] });
    }
 
    handleChange(event, value) {
        const menu = ["main","document-checking","payment"];
        this.props.history.push(`/app/main/internal-request-order/new-appraisal/${menu[value]}/1`);
        this.setState({ activeIndex: value });
    }
    handleMainChange(event,value){
        const menu = ["new-appraisal/main","penilaian/input-pembanding-tanah"];
        this.props.history.push(`/app/main/internal-request-order/${menu[value]}/1`);
        this.setState({ activeMainIndex: value });
    }
	handleDateChange = (date) => {
        let APPLICATION = this.state.APPLICATION; 
        APPLICATION.DATE_OF_APPLICATION =date;

        this.setState({ 
            selectedDate: date,
            APPLICATION
         });
         localStorage.setItem('APPLICATION',APPLICATION);
	};

 
 
    APPLICATIONChange(field, e) {
		let APPLICATION = this.state.APPLICATION;
        APPLICATION[field] = e.target.value;
        this.setState({ APPLICATION });
        localStorage.setItem('APPLICATION',JSON.stringify( APPLICATION));
        if(field =='ASSESSMENT_TYPE_ID')
        {
            let  asses = JSON.parse(localStorage.getItem('ASSESSMENT_HISTORY'));
            const list = asses.filter(l => {
                return l.ASSESSMENT_TYPE_ID.toLowerCase().match(APPLICATION[field].toLowerCase());
              });
            
              if(list.length >0)
              {
                const datanew = list.map((group, index) => [
                    convertDateASP(group.ASSIG_DATE, "DD-MM-YYYY"),
                    group.VALUER,
                    group.ASSIG_BY,
                ]);

                this.setState({
                    dataassygn: datanew,
                });
          
                }
        }
    }

    DEBITURChange(field, e) {
		let DEBITUR = this.state.DEBITUR;
        DEBITUR[field] = e.target.value;
        this.setState({ DEBITUR });
        localStorage.setItem('DEBITUR',JSON.stringify( DEBITUR));
    }

    APPFLAGChange(field, e) {
		let APPFLAG = this.state.APPFLAG;
        APPFLAG[field] = e.target.value;
        this.setState({ APPFLAG });
        localStorage.setItem('APPFLAG',JSON.stringify( APPFLAG));
    }

    setzipcode=(select)=>{
        let DEBITUR = this.state.DEBITUR;
  
        DEBITUR.ZIP_CODE_ID=  select[0];
        DEBITUR.DISTRICT_ID =  select[1];
        DEBITUR.SUBDISTRICT_ID =  select[2];
        DEBITUR.PROVINCE_ID =  select[3];
        DEBITUR.CITY_ID =  select[4];
        this.setState({ DEBITUR});
        localStorage.setItem('DEBITUR',JSON.stringify( DEBITUR));
    }

    setDistribusi=(select)=>{
        let APPLICATION = this.state.APPLICATION;
        APPLICATION['DISTRIBUTION_ID'] =select[3];
        this.setState({ APPLICATION});
        localStorage.setItem('APPLICATION',JSON.stringify( APPLICATION));
    }

  

    UpdateOrder(e){

        let  app = JSON.parse(localStorage.getItem('APPLICATION'));
        let  flag = JSON.parse(localStorage.getItem('APPFLAG'));
        let  deb = JSON.parse(localStorage.getItem('DEBITUR'));
        let  asses = JSON.parse(localStorage.getItem('ASSESSMENT_HISTORY'));
        let  cert = JSON.parse(localStorage.getItem('CERTIFICATE'));
        let  ded = JSON.parse(localStorage.getItem('DEDUPRESULT'));
        let mandatory = this.state.mandatory;
    if(app.MANUAL_DISTRIBUTION =="1")
         app.MANUAL_DISTRIBUTION =true;
    else
        app.MANUAL_DISTRIBUTION =false;

   if(deb.VIP =="1")
        deb.VIP =true;
   else
      deb.VIP =false;

      let errors =this.state.errors;
      let appvalid =true;
      let debvalid =true;

       
      Object.keys(mandatory).forEach(function(key) {
        //arr.push(key);
        if(deb[key] !== undefined)
        {
            if(deb[key] =="")
              {
                debvalid =false;
                errors[key] ="Cannot be empty";
              }
        }
        if(app[key] !== undefined)
        {
            if(app[key] =="")
            {
                appvalid =false;
              errors[key] ="Cannot be empty";
              
            }
        }
      

      });

      this.setState({ errors});
 
      if(appvalid && debvalid)
      {
        const reqData = {
            APPLICATION: app,
            APPFLAG:flag,
            DEBITUR:deb,
            ASSESSMENT_HISTORY:asses,
            CERTIFICATE:cert,
            DEDUPRESULT:ded
        };
        
    AparsialApi().post('ORDER/UPDATE',reqData) 
     .then(res => {
         if (res.data.errorcode > 0)
         {
            NotificationManager.error(res.data.msg,getNotifTimeout());
         }
         else
         {
            NotificationManager.success(res.data.msg,getNotifTimeout());
         }
        
    }).catch(err => {
        console.error("error Update", err);
        NotificationManager.error(err.message, getNotifTimeout());
    });

      }
         
    }

    render() {
        const { activeIndex,
            activeMainIndex,
            selectedDate,
            APPLICATION,
            APPFLAG,
            DEBITUR,
            ASSESSMENT_HISTORY,
            CERTIFICATE,
            DEDUPRESULT,
            openpilihdistribusi, 
            loading ,
            jenisJaminan,
            jenisFasilitas,
            cabang,
            jenisPermintaanTransaksi,
            jenisPenilaian,
            reason,
        } = this.state;
        const extendUrl = "/app/main/internal-request-order";

     
      

        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        return (
            
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Main" match={this.props.match} />
                <Tabs value={activeMainIndex} onChange={(e, value) => this.handleMainChange(e, value)}>
                    <Tab label="Main" />
                    <Tab label="Penilaian" />
                </Tabs>
                <Tabs value={activeIndex} onChange={(e, value) => this.handleChange(e, value)}>
                    <Tab label="Main" />
                    <Tab label="Document Checking" />
                    <Tab label="Payment" />
                </Tabs>
                <TabContainer>
                    <RctCollapsibleCard className="text-danger" heading="APPLICATION DATA">
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField type="number" id="noPermohonan" fullWidth label="No Permohonan" 
                                     style={{backgroundColor: this.state.mandatory.APPLICATION_NO || ''}}
                                     error={this.state.errors.APPLICATION_NO ? true : false}
                                    onChange={this.APPLICATIONChange.bind(this, "APPLICATION_NO")} value={this.state.APPLICATION.APPLICATION_NO || ''} />
                                </div>
                                <div className="form-group">
                                    <DatePicker
                                        label="Tanggal Permohonan"
                                        id="tanggalPermohonan"
                                        value={selectedDate}
                                        format="DD/MM/YYYY"
                                        onChange={this.handleDateChange}
                                        style={{backgroundColor: this.state.mandatory.DATE_OF_APPLICATION || ''}}
                                        error={this.state.errors.DATE_OF_APPLICATION ? true : false}
                                        animateYearScrolling={false}
                                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                        fullWidth
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisPermintaanTransaksi" select label="Jenis Permintaan Transaksi"
                                        onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        style={{backgroundColor: this.state.mandatory.FLAG_APPRAISAL_ID || ''}}
                                        error={this.state.errors.FLAG_APPRAISAL_ID ? true : false}
                                        value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Permintaan Transaksi"
                                        fullWidth>
                                        {jenisPermintaanTransaksi.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisFasilitas" select label="Jenis Fasilitas"
                                        onChange={this.DEBITURChange.bind(this, "FACILITY_ID")} 
                                        style={{backgroundColor: this.state.mandatory.FACILITY_ID || ''}}
                                        error={this.state.errors.FACILITY_ID ? true : false}
                                        value={this.state.DEBITUR.FACILITY_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Fasilitas"
                                        fullWidth>
                                        {jenisFasilitas.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="namaDebitur" 
                                    fullWidth label="Nama Debitur" 
                                    style={{backgroundColor: this.state.mandatory.CUSTOMER_NAME || ''}}
                                    error={this.state.errors.CUSTOMER_NAME ? true : false}
                                    onChange={this.DEBITURChange.bind(this, "CUSTOMER_NAME")} 
                                        value={this.state.DEBITUR.CUSTOMER_NAME || ''}  />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="nilaiExposure" 
                                    fullWidth label="Nilai Exposure" 
                                    style={{backgroundColor: this.state.mandatory.DISTRIBUTION_ID || ''}}
                                    onChange={this.DEBITURChange.bind(this, "EXPOSURE_VALUE")} 
                                        value={this.state.DEBITUR.EXPOSURE_VALUE || ''}  />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisJaminan" select label="Jenis Jaminan"
                                         onChange={this.DEBITURChange.bind(this, "TYPE_OF_GUARANTEE_ID")} 
                                         style={{backgroundColor: this.state.mandatory.CUSTOMER_NAME || ''}}
                                         error={this.state.errors.CUSTOMER_NAME ? true : false}
                                         value={this.state.DEBITUR.TYPE_OF_GUARANTEE_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Jaminan"
                                        fullWidth>
                                        {jenisJaminan.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="deskripsiJaminan" fullWidth label="Deskripsi Jaminan" onChange={this.DEBITURChange.bind(this, "DESCRIPTION_OF_GUARANTEE")} 
                                       style={{backgroundColor: this.state.mandatory.DESCRIPTION_OF_GUARANTEE || ''}}
                                       error={this.state.errors.DESCRIPTION_OF_GUARANTEE ? true : false}
                                       value={this.state.DEBITUR.DESCRIPTION_OF_GUARANTEE || ''}  />
                                </div>
                                <div className="form-group">
                                    <TextField id="alamatJaminan1" fullWidth label="Alamat Jaminan" value="" onChange={this.DEBITURChange.bind(this, "ADDRESS_OF_GUARANTEE_1")} 
                                         style={{backgroundColor: this.state.mandatory.ADDRESS_OF_GUARANTEE_1 || ''}}
                                         error={this.state.errors.ADDRESS_OF_GUARANTEE_1 ? true : false}
                                        value={this.state.DEBITUR.ADDRESS_OF_GUARANTEE_1 || ''} />
                                    <TextField id="alamatJaminan2" fullWidth label="" onChange={this.DEBITURChange.bind(this, "ADDRESS_OF_GUARANTEE_2")} 
                                         style={{backgroundColor: this.state.mandatory.ADDRESS_OF_GUARANTEE_2 || ''}}
                                         error={this.state.errors.ADDRESS_OF_GUARANTEE_2 ? true : false}
                                         value={this.state.DEBITUR.ADDRESS_OF_GUARANTEE_2 || ''} />
                                    <TextField id="alamatJaminan3" fullWidth label="" onChange={this.DEBITURChange.bind(this, "ADDRESS_OF_GUARANTEE_3")} 
                                         style={{backgroundColor: this.state.mandatory.ADDRESS_OF_GUARANTEE_3 || ''}}
                                         error={this.state.errors.ADDRESS_OF_GUARANTEE_3 ? true : false}
                                         value={this.state.DEBITUR.ADDRESS_OF_GUARANTEE_3 || ''} />
                                </div>
                                <div className="form-group">
                                    <TextField disabled id="kodePos" label="Kode Pos" 
                                     style={{backgroundColor: this.state.mandatory.ZIP_CODE_ID || ''}}
                                     error={this.state.errors.ZIP_CODE_ID ? true : false}
                                    onChange={this.DEBITURChange.bind(this, "ZIP_CODE_ID")} value={this.state.DEBITUR.ZIP_CODE_ID  || ''} readOnly />
                                    <SearchZipcode updatezip={this.setzipcode.bind(this)}  />
                                </div>
                                <div className="form-group">
                                    <TextField id="kelurahan" fullWidth label="Kelurahan"  
                                    style={{backgroundColor: this.state.mandatory.CITY_ID || ''}}
                                    error={this.state.errors.CITY_ID ? true : false}
                                    onChange={this.DEBITURChange.bind(this, "CITY_ID")} value={ this.state.DEBITUR.CITY_ID || ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="kecamatan" fullWidth label="Kecamatan" 
                                     style={{backgroundColor: this.state.mandatory.DISTRICT_ID || ''}}
                                     error={this.state.errors.DISTRICT_ID ? true : false}
                                    onChange={this.DEBITURChange.bind(this, "DISTRICT_ID")} value={ this.state.DEBITUR.DISTRICT_ID  || ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="kabupaten" fullWidth label="Kabupaten" 
                                     style={{backgroundColor: this.state.mandatory.SUBDISTRICT_ID || ''}}
                                     error={this.state.errors.SUBDISTRICT_ID ? true : false}
                                    onChange={this.DEBITURChange.bind(this, "SUBDISTRICT_ID")} value={ this.state.DEBITUR.SUBDISTRICT_ID  || ''} readOnly  />
                                </div>
                                <div className="form-group">
                                    <TextField id="propinsi" fullWidth label="Propinsi" 
                                     style={{backgroundColor: this.state.mandatory.PROVINCE_ID || ''}}
                                     error={this.state.errors.PROVINCE_ID ? true : false}
                                    onChange={this.DEBITURChange.bind(this, "PROVINCE_ID")} value={ this.state.DEBITUR.PROVINCE_ID || ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <FormLabel component="legend">VIP</FormLabel>
                                    <FormControlLabel control={
                                        <Checkbox color="primary"
                                        onChange={this.DEBITURChange.bind(this, "VIP")} 
                                        value={this.state.DEBITUR.VIP || false} 
                                         />
                                    } label="Ya"
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisPenilaian" select label="Jenis Penilaian"
                                        onChange={this.APPLICATIONChange.bind(this, "ASSESSMENT_TYPE_ID")} 
                                        value={this.state.APPLICATION.ASSESSMENT_TYPE_ID || false} 
                                        style={{backgroundColor: this.state.mandatory.ASSESSMENT_TYPE_ID || ''}}
                                        error={this.state.errors.ASSESSMENT_TYPE_ID ? true : false}
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {jenisPenilaian.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                <AssesmentHistory dataassygn={this.state.dataassygn} />
                                </div>
                                <div className="form-group">
                                    <FormLabel component="legend">Distribusi Manual</FormLabel>
                                    <FormControlLabel control={
                                        <Checkbox color="primary" 
                                        onChange={this.APPLICATIONChange.bind(this, "MANUAL_DISTRIBUTION")} 
                                        value={this.state.APPLICATION.MANUAL_DISTRIBUTION || '1'} 
                                         />
                                    } label=""
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField disabled id="pilihDistribusi" label="Pilih Distribusi" 
                                    style={{backgroundColor: this.state.mandatory.ASSESSMENT_TYPE_ID || ''}}
                                    error={this.state.errors.ASSESSMENT_TYPE_ID ? true : false}
                                      onChange={this.APPLICATIONChange.bind(this, "ASSESSMENT_TYPE_ID")} 
                                      value={this.state.APPLICATION.DISTRIBUTION_ID || ''} 
                                    />
                                     <SearchDistribusi updateDistribusi={this.setDistribusi.bind(this)} />
                                </div>
                                
                            </div>
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField id="namaPIC1" fullWidth label="Nama PIC 1"
                                      style={{backgroundColor: this.state.mandatory.PIC_1_NAME || ''}}
                                      error={this.state.errors.PIC_1_NAME ? true : false}
                                      onChange={this.DEBITURChange.bind(this, "PIC_1_NAME")} 
                                      value={this.state.DEBITUR.PIC_1_NAME || ''} 
                                       />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noWaPIC1" fullWidth label="No WA PIC 1"
                                     style={{backgroundColor: this.state.mandatory.NO_WA_PIC_1 || ''}}
                                     error={this.state.errors.NO_WA_PIC_1 ? true : false}
                                     onChange={this.DEBITURChange.bind(this, "NO_WA_PIC_1")} 
                                     value={this.state.DEBITUR.NO_WA_PIC_1 || ''} 
                                      />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noTelpPIC1" fullWidth label="No Telp PIC 1" 
                                     style={{backgroundColor: this.state.mandatory.PHONE_NUMBER_PIC_1 || ''}}
                                     error={this.state.errors.PHONE_NUMBER_PIC_1 ? true : false}
                                     onChange={this.DEBITURChange.bind(this, "PHONE_NUMBER_PIC_1")} 
                                     value={this.state.DEBITUR.PHONE_NUMBER_PIC_1 || ''}
                                     />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="namaPIC2" fullWidth label="Nama PIC 2" 
                                    style={{backgroundColor: this.state.mandatory.PIC_2_NAME || ''}}
                                    error={this.state.errors.PIC_2_NAME ? true : false}
                                     onChange={this.DEBITURChange.bind(this, "PIC_2_NAME")} 
                                     value={this.state.DEBITUR.PIC_2_NAME || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noWaPIC2" fullWidth label="No WA PIC 2"
                                      style={{backgroundColor: this.state.mandatory.NO_WA_PIC_2 || ''}}
                                      error={this.state.errors.NO_WA_PIC_2 ? true : false}
                                    onChange={this.DEBITURChange.bind(this, "NO_WA_PIC_2")} 
                                    value={this.state.DEBITUR.NO_WA_PIC_2 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noTelpPIC2" fullWidth label="No Telp PIC 2" 
                                     style={{backgroundColor: this.state.mandatory.PHONE_NUMBER_PIC_2 || ''}}
                                     error={this.state.errors.PHONE_NUMBER_PIC_2 ? true : false}
                                      onChange={this.DEBITURChange.bind(this, "PHONE_NUMBER_PIC_2")} 
                                      value={this.state.DEBITUR.PHONE_NUMBER_PIC_2 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="Tcabang" select label="Cabang"
                                           onChange={this.DEBITURChange.bind(this, "BRANCH")} 
                                           style={{backgroundColor: this.state.mandatory.BRANCH || ''}}
                                           error={this.state.errors.BRANCH ? true : false}
                                           value={this.state.DEBITUR.BRANCH || ''}
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="cabang"
                                        fullWidth>
                                        {cabang.map(option => (
                                        <MenuItem key={option.BRANCHNAME} value={option.BRANCHID}>
                                            {option.BRANCHNAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="marketingOfficer" fullWidth label="Marketing Officer" 
                                    style={{backgroundColor: this.state.mandatory.MARKETING_OFFICER || ''}}
                                    error={this.state.errors.MARKETING_OFFICER ? true : false}
                                     onChange={this.DEBITURChange.bind(this, "MARKETING_OFFICER")} 
                                     value={this.state.DEBITUR.MARKETING_OFFICER || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="NoTelpMarketing" fullWidth label="No Telp Marketing" 
                                    style={{backgroundColor: this.state.mandatory.NO_TEL_MARKETING || ''}}
                                    error={this.state.errors.NO_TEL_MARKETING ? true : false}
                                     onChange={this.DEBITURChange.bind(this, "NO_TEL_MARKETING")} 
                                     value={this.state.DEBITUR.NO_TEL_MARKETING || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve1" fullWidth label="Reserve 1" 
                                     style={{backgroundColor: this.state.mandatory.RESERVE_1 || ''}}
                                     error={this.state.errors.RESERVE_1 ? true : false}
                                     onChange={this.DEBITURChange.bind(this, "RESERVE_1")} 
                                     value={this.state.DEBITUR.reserve1 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve2" fullWidth label="Reserve 2" 
                                      style={{backgroundColor: this.state.mandatory.RESERVE_2 || ''}}
                                      error={this.state.errors.RESERVE_2 ? true : false}
                                      onChange={this.DEBITURChange.bind(this, "RESERVE_2")} 
                                      value={this.state.DEBITUR.RESERVE_2 || ''}
                                      />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve3" fullWidth label="Reserve 3" 
                                    style={{backgroundColor: this.state.mandatory.RESERVE_3 || ''}}
                                    error={this.state.errors.RESERVE_3 ? true : false}
                                     onChange={this.DEBITURChange.bind(this, "RESERVE_3")} 
                                     value={this.state.DEBITUR.RESERVE_3 || ''}
                                     />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve4" fullWidth label="Reserve 4" 
                                     style={{backgroundColor: this.state.mandatory.RESERVE_4 || ''}}
                                     error={this.state.errors.RESERVE_4 ? true : false}
                                      onChange={this.DEBITURChange.bind(this, "RESERVE_4")} 
                                      value={this.state.DEBITUR.RESERVE_4 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve5" fullWidth label="Reserve 5"
                                    style={{backgroundColor: this.state.mandatory.RESERVE_5 || ''}}
                                    error={this.state.errors.RESERVE_5 ? true : false}
                                    onChange={this.DEBITURChange.bind(this, "RESERVE_5")} 
                                    value={this.state.DEBITUR.RESERVE_5 || ''}
                                    />
                                </div>
                            </div>
                        </div>
                    </RctCollapsibleCard>
                    <RctCollapsibleCard className="text-danger" heading="DAFTAR SERTIFIKAT">
                    <AddCertificate/>
                <MatButton variant="contained"  onClick={this.UpdateOrder.bind(this)} className="btn-danger mr-10 mb-10 text-white">SIMPAN</MatButton>
                <MatButton variant="contained"  onClick={this.handleshowdedup} className="btn-danger mr-10 mb-10 text-white">DEDUP</MatButton>
               </RctCollapsibleCard>
                    {this.state.showdedup ? (
                        <AddDedup/>
                    ) :''  }
                </TabContainer>
                
            </div>
            
      
         
        );
    }
}