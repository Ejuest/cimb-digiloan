import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import MenuItem from '@material-ui/core/MenuItem';
 

class SearchIdColl extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
            colldata:[],
            opencoll: false,
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
        const datanew = [
            ["1","A","Tangerang","Consumer",0],
            ["2","B","Jakarta","Consumer 2",1]
        ];
        this.setState({
            colldata: datanew
        });
        // AparsialApi().get('RFZIPCODE/Get')
        // .then(res => {
        //     if (res.status == 200) {
                
        //        const datanew = res.data.map((group, index) => [
        //            group.ziP_CODE,
        //            group.province,
        //            group.city,
        //            group.district,
        //            group.subdistrict,
        //            group.ziP_DESC,
        //            index,
        //        ]);
        //     this.setState({
        //        zipdata:  datanew,
        //     });
        // } }).catch(err => {
        //     console.error("error RFZIPCODE", err);
        // });
    }
    handleClickopencoll = () => {
		this.setState({
			opencoll: true,
			fields: {},
			errors: {}
		});
	};
    
    handleCloseColl = () => {
		this.setState({ opencoll: false });
		this.setState({
			fields: {},
			errors: {}
		});
    };
    
    onSelectColl =(idx)=>{
     const { colldata } = this.state;
        const selectedGroup = colldata[idx];
     //   let fields = this.state.fields;
        
        // fields['kodePos'] =  selectedGroup[0];
        // fields['Kecamatan'] =  selectedGroup[1];
        // fields['Kabupaten'] =  selectedGroup[2];
        // fields['Propinsi'] =  selectedGroup[3];
        // fields['Kelurahan'] =  selectedGroup[4];
         this.setState({ opencoll:false});
        // localStorage.setItem("SelectZipcode",JSON.stringify(fields));
       this.props.updateidcoll(selectedGroup);
        
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        const columncoll = ["COL ID","TIPE JAMINAN","LOKASI JAMINAN","JENIS FASILITAS",
       {
           name: "Action",
           options: {
               filter: false,
               sort: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                   return (
                       <div>
                           <MatButton onClick={() => this.onSelectColl(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Select</MatButton>
                       </div>
                   );
               }
           }
       }
   ];
   //  END DT SERTIFIKAT
        return (
            <React.Fragment>
              {/* START DIALOG KODE POS */}
              <Dialog maxWidth="md" className="client-dialog" open={this.state.opencoll} onClose={this.handleCloseColl} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">KODE POS</DialogTitle>
                    <DialogContent>
                        <div>
                        <RctCollapsibleCard fullBlock>
                                        {this.state.loading &&
                                            <RctSectionLoader />
                                        }
                                        <MUIDataTable
                                            title="DATA DETAIL"
                                            data={this.state.colldata}
                                            columns={columncoll}
                                            options={options}
                                        />
                            </RctCollapsibleCard>
                        </div>
                    </DialogContent>
                </Dialog>
                {/* END DIALOG KODE POS */}

                <MatButton variant="contained" color="primary"  onClick={this.handleClickopencoll} className="mt-3 ml-2 text-white">SEARCH</MatButton>
        </React.Fragment>
        );
    }
}

export default SearchIdColl
 