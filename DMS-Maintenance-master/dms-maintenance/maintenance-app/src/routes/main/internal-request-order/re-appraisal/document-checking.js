/**
 * Miscellaneous Widgets Page
 */

import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";

import { AparsialApi  } from 'Api';

import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';

 //page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

 //rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

 //rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

function TabContainer({ children }) {
    return (
        <Typography component="div">
            {children}
        </Typography>
    );
}


export default class InternalRequestOrderReAppraisalDocumentChecking extends Component {
    state = {
        activeIndex: 2,
        data :[],
        dataTemp :{},
        OpenDokumen :false,
        dokType:[],
        reason:[],
        fields: {},
        errors: {},
        loading: false
    }
    
    componentDidMount() {
        AparsialApi().get('RF_CERTIFICATE_TYPE/Get')
        .then(res => {
            if (res.status == 200) {
               
            this.setState({
                dokType: res.data,
            });
        }
        }).catch(err => {
            console.error("error RF_CERTIFICATE_TYPE", err);
        });
        
         AparsialApi().get('RF_REASON/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
                 
             this.setState({
                 reason:  resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RF_REASON", err);
         });
        
    }

    handleClickOpenDokumen = () => {
		this.setState({OpenDokumen: true,});
	};
 
    handleCloseDokumen = () => {
		this.setState({ OpenDokumen: false });
    };


   // submit form data sertifikat
	onFormSubmitDokumen(e) {
        e.preventDefault();
        let data = this.state.data;
        var newclientsData = [ 
            this.state.dataTemp.namaDokumen,
            this.state.dataTemp.valid,
            this.state.dataTemp.mandatory
        ];
        data.push(newclientsData);
        this.setState({ data:data ,OpenDokumen: false});
    }
    
    handletextChange(field, e) {
		let fields = this.state.fields;
        fields[field] = e.target.value;
		this.setState({ fields });
    }
    handletextChangeDataTemp(field, e) {
		let dataTemp = this.state.dataTemp;
        dataTemp[field] = e.target.value;
		this.setState({ dataTemp });
    }
    handleChange(event, value) {
        const menu = ["main","re-appraisal","document-checking","payment"];
        this.props.history.push(`/app/main/internal-request-order/re-appraisal/${menu[value]}/1`);
        this.setState({ activeIndex: value });
    }
    render() {
        const { activeIndex,
            data,
            dataTemp,
            OpenDokumen,
            fields
            ,errors,
            loading ,
            dokType,
            reason,
        } = this.state;
        const extendUrl = "/app/main/internal-request-order";
       //  START DT SERTIFIKAT
        const columns = ["Nama Dokumen", 
            {
				name: "Valid",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <FormControlLabel control={
                                        <Checkbox color="primary" />
                                    } label=""
                                    />
							</div>
						);
					}
				}
            },
            {
				name: "Mandatory",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <FormControlLabel control={
                                        <Checkbox color="primary"
                                        />
                                    } label=""
                                    />
							</div>
						);
					}
				}
			},
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Download</MatButton>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="btn-danger mr-10 mb-10 text-white btn-icon">Upload</MatButton>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="btn-danger mr-10 mb-10 text-white btn-icon">View</MatButton>
							</div>
						);
					}
				}
			}
        ];
       //  END DT SERTIFIKAT
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
        return (
            
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Document Checking" match={this.props.match} />
                {/* START DIALOG SERTIFIKAT */}
                <Dialog className="client-dialog" open={this.state.OpenDokumen} onClose={this.handleCloseDokumen} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">DOKUMEN</DialogTitle>
					<DialogContent>
						<div>
							<form onSubmit={this.onFormSubmitDokumen.bind(this)}>
								<div className="row">
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                        <TextField id="TdokType" select label="Tipe Dokumen"
                                            value={this.state.dataTemp.namaDokumen || ""}
                                            onChange={this.handletextChangeDataTemp.bind( this,'namaDokumen')}
                                            SelectProps={{
                                                MenuProps: {
                                                },
                                            }}
                                            helperText="Tipe Dokumen"
                                            fullWidth>
                                            {dokType.map(option => (
                                            <MenuItem key={option.id} value={option.name}>
                                                {option.name}
                                            </MenuItem>
                                            ))}
                                        </TextField>
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                        <FormControlLabel control={
                                            <Checkbox color="primary"
                                            onChange={this.handletextChangeDataTemp.bind(this, "valid")} 
                                            value={this.state.fields.valid || '1'} 
                                            />
                                        } label="Valid"
                                        />
									</div>
                                    <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                        <FormControlLabel control={
                                            <Checkbox color="primary"
                                            onChange={this.handletextChangeDataTemp.bind(this, "mandatory")} 
                                            value={this.state.fields.mandatory || '1'} 
                                            />
                                        } label="Mandatory"
                                        />
									</div>
								</div>
								<div className="pt-25 text-right">
									<MatButton variant="contained" onClick={this.handleCloseDokumen} className="btn-danger mr-15 text-white">
										Cancel
            		                </MatButton>
									<MatButton className="btn-success text-white text-capitalize" type="submit">Submit</MatButton>
								</div>
							</form>
						</div>
					</DialogContent>
				</Dialog>
                {/* END DIALOG SERTIFIKAT */}
                <Tabs value={activeIndex} onChange={(e, value) => this.handleChange(e, value)}>
                    <Tab label="Main" />
                    <Tab label="Re Appraisal" />
                    <Tab label="Document Checking" />
                    <Tab label="Payment" />
                </Tabs>
                <TabContainer>
                    <RctCollapsibleCard className="text-danger" heading="DOKUMEN">
                    <   RctCollapsibleCard fullBlock>
                            {loading &&
                                <RctSectionLoader />
                            }
                            <MUIDataTable
                                title={<div><MatButton onClick={() => this.handleClickOpenDokumen()} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon"> ADD NEW</MatButton></div>}
                                data={this.state.data}
                                columns={columns}
                                options={options}
                            />
                        </RctCollapsibleCard>
                        <div className="form-group">
                            <TextField id="Reason" select label="Reason"
                                value={this.state.fields["reason"]  || ''}
                                onChange={this.handletextChange.bind( this,'reason')}
                                SelectProps={{
                                    MenuProps: {
                                    },
                                }}
                                helperText="Reason"
                                fullWidth>
                                {reason.map(option => (
                                <MenuItem key={option.id} value={option.name}>
                                    {option.name}
                                </MenuItem>
                                ))}
                            </TextField>
                        </div>
                        <div className="form-group">
                            <TextField id="remarks" fullWidth label="Remarks" multiline rows="4" value={this.state.fields["remarks"] || ""} onChange={this.handletextChange.bind( this,'remarks')} />
                        </div>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">KEMBALI KE RCS</MatButton>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SIMPAN</MatButton>
                    </RctCollapsibleCard>
                    
                </TabContainer>
                
            </div>
            
      
         
        );
    }
}