import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import MenuItem from '@material-ui/core/MenuItem';

export default class AddCertificate extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            dataCert:[],
            dokType:[],
            opensertifikat :false,
            Edit :false,
            fieldsertifikat:{},
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
        AparsialApi().get('RF_CERTIFICATE_TYPE/Get')
        .then(res => {
            if (res.status == 200) {
               
            this.setState({
                dokType: res.data,
            });
          //  localStorage.setItem("dataCert",JSON.stringify(dataCert));
        }
        }).catch(err => {
            console.error("error RF_CERTIFICATE_TYPE", err);
        });
    }
 

    //  componentDidUpdate(prevProps) {
    //     if (prevProps.dataCert.length !== this.props.dataCert.length) {
    //        this.getFavClient();
    //        this.getRecentClient();
    //     }
    //  }
  
     // get favourite client data
     getFavClient() {
        let newArray = [];
        let data = this.props.dataCert;
        if (data !== null) {
           for (let Item of data) {
              if (Item.type === 'favourite') {
                 newArray.push(Item)
              }
           }
           this.setState({
            dataCert: newArray,
              isUpdated:false
           })
        }
     }

        // get recent client data
   getRecentClient() {
    let newArray = [];
    let data = this.props.clientsData;
    if (data !== null) {
       for (let Item of data) {
          if (Item.type === 'recently_added') {
             newArray.push(Item)
          }
       }
       this.setState({
        dataCert: newArray,
          isUpdated:false
       })
    }
 }


    handleClosesetifikat = () => {
		this.setState({ opensertifikat: false });
		this.setState({
			fields: {},
            errors: {},
            Edit:false
		});
    };



    handleClickopensertifikat = () => {
		this.setState({
			opensertifikat: true,
            fieldsertifikat:{},
			errors: {}
		});
    };
    
    handletextChangeSertifikat(field, e) {
		let fieldsertifikat = this.state.fieldsertifikat;
        fieldsertifikat[field] = e.target.value;
		this.setState({ fieldsertifikat });
    }

    removeSertifikat(idx) {
        let  j  = this.state.dataCert.filter(function(l) {
            return l[1] == (idx);
          });

         
         if(this.state.dataCert.length >1)
          {
            const items = this.state.dataCert.splice([j], 1);
            this.setState({
                dataCert: items
             });
             localStorage.setItem("dataCert",JSON.stringify(dataCert));
          }
          else
          {
            this.setState({
                dataCert: [],
             });
             localStorage.setItem("dataCert",JSON.stringify(dataCert));
          } 

       
     // this.props.onDelete(this.state,idx);
         
       }

       onClickEdit(idx){
        let  j  = this.state.dataCert.filter(function(l) {
            return l[1] == (idx);
          });
          let Nfieldsertifikat={};
          Nfieldsertifikat['DokType']=j[0][0];
          Nfieldsertifikat['NoDokumen']=j[0][1];
          Nfieldsertifikat['NIB']=j[0][2];
          Nfieldsertifikat['AtasNama'] =j[0][3];
      
        this.setState({
            fieldsertifikat:Nfieldsertifikat,
            Edit: true,
            opensertifikat :true
         });
         
       }

	//submit form data
	onFormSubmitSertifikat(e) {
        e.preventDefault();
      
         const { dataCert, Edit } = this.state;
    if(!Edit)
    {
        const datanew =  [
            this.state.fieldsertifikat['DokType'],
            this.state.fieldsertifikat['NoDokumen'],
            this.state.fieldsertifikat['NIB'],
            this.state.fieldsertifikat['AtasNama'],
            this.state.fieldsertifikat['NoDokumen'],
        ];
        //this.props.onAdd(this.state.fieldsertifikat);
		   dataCert.push(datanew);
             this.setState({ dataCert ,opensertifikat: false });
    }
    else
    {  let newclientsData = [];
        for (const item of dataCert) {
            if (item[1] === this.state.fieldsertifikat['NoDokumen']) {
               item[0] =this.state.fieldsertifikat['DokType'];
               item[1] = this.state.fieldsertifikat['NoDokumen'];
               item[2] = this.state.fieldsertifikat['NIB'];
               item[3] = this.state.fieldsertifikat['AtasNama'];
               item[4] = this.state.fieldsertifikat['NoDokumen'];
            }
            newclientsData.push(item)
         }
         this.setState({ dataCert:newclientsData ,opensertifikat: false ,Edit:false});
    }
    
     localStorage.setItem("dataCert",JSON.stringify(dataCert));
 
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        const columns = ["Type Sertifikat", "No Sertifikat", "NIB", "Nama Pemegang Hak",
        {
            name: "Action",
            options: {
                filter: false,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <div>
                            <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Edit</MatButton>
                            <MatButton onClick={() => this.removeSertifikat(value)} variant="contained" color="primary" className="btn-danger mr-10 mb-10 text-white btn-icon">Delete</MatButton>
                        </div>
                    );
                }
            }
        }
    ];
   //  END DT SERTIFIKAT
        return (
            <React.Fragment>
             {/* START DIALOG SERTIFIKAT */}
             <Dialog className="client-dialog" open={this.state.opensertifikat} onClose={this.handleClosesetifikat} aria-labelledby="form-dialog-title">
             <DialogTitle id="form-dialog-title">SERTIFIKAT</DialogTitle>
             <DialogContent>
                 <div>
                     <form onSubmit={this.onFormSubmitSertifikat.bind(this)}>
                         <div className="row">
                             <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                 <TextField id="TdokType" select label="Tipe Dokumen"
                                     value={this.state.fieldsertifikat["DokType"] ||''}
                                     onChange={this.handletextChangeSertifikat.bind( this,'DokType')}
                                     SelectProps={{
                                         MenuProps: {
                                         },
                                     }}
                                     helperText="Tipe Dokumen"
                                     fullWidth>
                                     {this.state.dokType.map(option => (
                                     <MenuItem key={option.id} value={option.name}>
                                         {option.name}
                                     </MenuItem>
                                     ))}
                                 </TextField>
                             </div>
                             <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                 <TextField type="number" id="noDokumen" fullWidth label="No Dokumen"   
                                 onChange={this.handletextChangeSertifikat.bind( this,'NoDokumen')}  
                                 value={ this.state.fieldsertifikat["NoDokumen"] ?  this.state.fieldsertifikat["NoDokumen"] : ''}/>
                             </div>
                             <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                 <TextField type="number" id="nib" fullWidth label="NIB" value=""  
                                  onChange={this.handletextChangeSertifikat.bind( this,'NIB')} 
                                 value={ this.state.fieldsertifikat["NIB"] ?  this.state.fieldsertifikat["NIB"] : ''} />
                             </div>
                             <div className="col-sm-12 col-md-12 col-lg-12">
                                 <TextField id="atasNama" fullWidth label="Atas Nama" 
                                 onChange={this.handletextChangeSertifikat.bind( this,'AtasNama')} 
                                 value={ this.state.fieldsertifikat["AtasNama"] ?  this.state.fieldsertifikat["AtasNama"] : ''} />
                             </div>
                         </div>
                         <div className="pt-25 text-right">
                             <MatButton variant="contained" onClick={this.handleClosesetifikat} className="btn-danger mr-15 text-white">
                                 Cancel
                             </MatButton>
                             <MatButton className="btn-success text-white text-capitalize" type="submit">Submit</MatButton>
                         </div>
                     </form>
                 </div>
             </DialogContent>
         </Dialog>
         {/* END DIALOG SERTIFIKAT */}
            < RctCollapsibleCard fullBlock>
                    { this.state.loading &&
                        <RctSectionLoader />
                    }
                    <MUIDataTable
                        title={<div><MatButton onClick={() => this.handleClickopensertifikat()} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon"> ADD NEW</MatButton></div>}
                        data={this.state.dataCert}
                        columns={columns}
                        options={options}
                    />
                </RctCollapsibleCard>
            </React.Fragment>
        );
    }
}
