/**
 * Miscellaneous Widgets Page
 */

import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';
import { getApiCustom } from 'Api';
 
import {
    Card,
    CardImg,
    CardText,
    CardBody,
    CardTitle,
    CardSubtitle,
    Button,
    TabContent, TabPane, Nav, NavItem, NavLink,
    Form, FormGroup, Label, Input, FormText,
    Row, Col,

 } from 'reactstrap';
 
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { NavLink as RRNavLink } from 'react-router-dom';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

function TabContainer({ children }) {
    return (
        <Typography component="div">
            {children}
        </Typography>
    );
}
const jenisPermintaanTransaksi = [
    {
        value: 'newAppraisal',
        label: 'New Appraisal',
    },
    {
        value: 'reAppraisal',
        label: 'Re Appraisal',
    },
    {
        value: 'banding',
        label: 'Banding',
    },
    {
        value: 'pending',
        label: 'Pending',
    },
];
const jenisFasilitas = [
    {
        value: 'consumer',
        label: 'Consumer'
    }
];
const jenisJaminan = [
    {
        value: 'rumahTinggal',
        label: 'Rumah Tinggal'
    }
];
const jenisPenilaian = [
    {
        value: 'internal',
        label: 'Internal'
    },
    {
        value: 'eksternal',
        label: 'Eksternal'
    },
    
];
const reason = [
    {
        value: 'alasanOverride',
        label: 'Alasan Override'
    },
    {
        value: 'alasanLain',
        label: 'Alasan Lain'
    },
    
];
const cabang = [
    {
        value: 'cimbNiagaPusat',
        label: 'CIMB Niaga Pusat'
    },
    {
        value: 'cimbNiagaBintaro',
        label: 'CIMB Niaga Bintaro'
    },
    
];
export default class InternalRequestOrderInbox extends Component {
    state = {
        openzipcode: false,
        opensetifikat :false,
        fields: {},
		errors: {}
    }
    
    handleClickopenzipcode = () => {
		this.setState({
			openzipcode: true,
			fields: {},
			errors: {}
		});
	};

    handleClickopensetifikat = () => {
		this.setState({
			opensetifikat: true,
			fields: {},
			errors: {}
		});
	};

 
    
	handleClose = () => {
		this.setState({ openzipcode: false });
		this.setState({
			fields: {},
			errors: {}
		});
    };

    handleClosesetifikat = () => {
		this.setState({ opensetifikat: false });
		this.setState({
			fields: {},
			errors: {}
		});
    };

       	//submit form data sertifikat
	onFormSubmitSertifikat(e) {
		e.preventDefault();
        alert("Form has test.")
    }
     
    	//submit form data
	onFormSubmit(e) {
		e.preventDefault();
        alert("Form has test.")
    }

    onClickEdit = (idx) => {
		// const { users } = this.state;
		// this.props.history.push(`/app/main/user-management-maker/list-user/${users[idx].id}`);
	}

	/**
	 * On Delete
	 */
	onClickDelete = (idx) => {
		// const { users } = this.state;
		// this.refs.deleteConfirmationDialog.open();
		// this.setState({ selectedUser: users[idx] });
    }
    
	componentDidMount() {
		
	}
    handleChange(event, value) {
        this.setState({ activeIndex: value });
    }
	handleDateChange = (date) => {
		this.setState({ selectedDate: date });
	};

    render() {
        const { loading, selectedUser } = this.state;
        const data = [
            ["HGB", "121323", "1212121", "Napoleon"],
            ["SHM", "333434", "qweqe", "Bernat"],
			 
        ]
        const columns = ["Type Sertifikat", "No Sertifikat", "NB", "Nama Pemegang Hak",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
								<Button onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Edit</Button>
								<Button onClick={() => this.onClickDelete(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon">Delete</Button>
							</div>
						);
					}
				}
			}
        ];
        
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
        const extendUrl = "/app/main/internal-request-order";
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Main" match={this.props.match} />

                {/* dialog zipcode */}
                <Dialog className="client-dialog" open={this.state.openzipcode} onClose={this.handleClose} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">Test</DialogTitle>
					<DialogContent>
						<div>
							<form onSubmit={this.onFormSubmit.bind(this)}>
								<div className="row">
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
									 awd
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
										 awd
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
									 adw
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12">
										 awd
									</div>
								</div>
								<div className="pt-25 text-right">
									<Button variant="contained" onClick={this.handleClose} className="btn-danger mr-15 text-white">
										Cancel
            		                </Button>
									<Button className="btn-success text-white text-capitalize" type="submit">Submit</Button>
								</div>
							</form>
						</div>
					</DialogContent>
				</Dialog>
            {/* dialog zipcode */}


                <Nav tabs>
                    <NavItem>
                        <NavLink to={`${extendUrl}/new-appraisal/main`}  activeClassName="active" tag={RRNavLink}>Main</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink to={`${extendUrl}/new-appraisal/document-checking`} activeClassName="" tag={RRNavLink}>
                            Document Checking
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink to={`${extendUrl}/new-appraisal/payment`} activeClassName="" tag={RRNavLink}>
                            Payment
                        </NavLink>
                    </NavItem>
                </Nav>
                <Row>
                    <Col md="12">
                        <Col md="12" style={{backgroundColor: '#ff4d4d'}} className="p-2">
                            <h4 className="text-white text-center">APPLICATION DATA</h4>
                        </Col>
                        <Row className="m-3 mt-0 pt-3 bg-white">
                            <Col md="6">
                                <FormGroup>
                                    <Label className="w-100" for="noPermohonan">No Permohonan</Label>
                                    <Input type="text" name="noPermohonan" id="noPermohonan" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="tanggalPermohonan">Tanggal Permohonan</Label>
                                    <Input type="date" name="tanggalPermohonan" id="tanggalPermohonan" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="jenisPermintaanTransaksi">Jenis Permintaan Transaksi</Label>
                                    <select className="form-control" id="jenisPermintaanTransaksi" readOnly>
                                        <option>New Appraisal</option>
                                    </select>
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="jenisFasilitas">Jenis Fasilitas</Label>
                                    <select className="form-control" id="jenisFasilitas">
                                        <option>Consumer</option>
                                        <option>Consumer</option>
                                        <option>Consumer</option>
                                    </select>
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="namaDebitur">Nama Debitur</Label>
                                    <Input type="text" name="namaDebitur" id="namaDebitur" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="nilaiExposure">Nilai Exposure</Label>
                                    <Input type="text" name="nilaiExposure" id="nilaiExposure" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="jenisJaminan">Jenis Jaminan</Label>
                                    <select className="form-control" id="jenisJaminan">
                                        <option>Rumah Tinggal</option>
                                    </select>
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="deskripsiJaminan">Deskripsi Jaminan</Label>
                                    <Input type="text" name="deskripsiJaminan" id="deskripsiJaminan" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="alamatJaminan">Alamat Jaminan</Label>
                                    <Input type="text" name="alamatJaminan1" id="alamatJaminan1" />
                                    <Input type="text" name="alamatJaminan2" id="alamatJaminan2" />
                                    <Input type="text" name="alamatJaminan3" id="alamatJaminan3" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="kodePos">Kode Pos</Label>
                                    <div className="container">
                                 <div className="row">
                                  <div className="col-8">
                                    <Input className="w-150" type="text" name="kodePos" id="kodePos" />
                                    </div>
                                    <div className="col"> 
                                    <Button size="small" color="primary" onClick={this.handleClickopenzipcode} >...</Button>
                                    </div>
                                    </div>
                                    </div>
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="kelurahan">Kelurahan</Label>
                                    <Input type="text" name="kelurahan" id="kelurahan" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="kecamatan">Kecamatan</Label>
                                    <Input type="text" name="kecamatan" id="kecamatan" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="kabupaten">Kabupaten</Label>
                                    <Input type="text" name="kabupaten" id="kabupaten" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="propinsi">Propinsi</Label>
                                    <Input type="text" name="propinsi" id="propinsi" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="vip">VIP</Label>
                                    <Input type="checkbox" id="vip" /> 
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="jenisPenilaian">Jenis Penilaian</Label>
                                    <select className="form-control" id="jenisPenilaian">
                                        <option>Internal</option>
                                        <option>External</option>
                                    </select>
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="distribusiManual">Distribusi Manual</Label>
                                    <Input type="checkbox" id="distribusiManual" />{' '}
                                </FormGroup>
                                <FormGroup>
                                <Label className="w-100" for="pilihDistribusi">Pilih Distribusi</Label>
                                <div className="container">
                                 <div className="row">
                                  <div className="col-8">
                                    <Input className="w-150" type="text" name="pilihDistribusi" id="pilihDistribusi" />
                                  </div>
                                    <div className="col">
                                        <Button size="small" color="primary">...</Button>
                                    </div>
                                 </div>
                                </div>
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="reason">Reason</Label>
                                    <select className="form-control" id="reason">
                                        <option>Alasan Override</option>
                                    </select>
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="remarks">Remarks</Label>
                                    <Input type="textarea" name="text" id="remarks" />
                                </FormGroup>
                            </Col>
                            <Col md="4">
                                <FormGroup>
                                    <Label className="w-100" for="namaPIC1">Nama PIC 1</Label>
                                    <Input type="text" name="namaPIC1" id="namaPIC1" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="noWAPIC1">No WA PIC 1</Label>
                                    <Input type="text" name="noWAPIC1" id="noWAPIC1" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="noTelpPIC1">No Telp PIC 1</Label>
                                    <Input type="text" name="noTelpPIC1" id="noTelpPIC1" />
                                </FormGroup>

                                <FormGroup>
                                    <Label className="w-100" for="namaPIC2">Nama PIC 2</Label>
                                    <Input type="text" name="namaPIC2" id="namaPIC2" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="noWAPIC2">No WA PIC 2</Label>
                                    <Input type="text" name="noWAPIC2" id="noWAPIC2" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="noTelpPIC2">No Telp PIC 2</Label>
                                    <Input type="text" name="noTelpPIC2" id="noTelpPIC2" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="cabang">Cabang</Label>
                                    <select className="form-control" id="cabang">
                                        <option>CIMB NIAGA PUSAT</option>
                                    </select>
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="marketingOfficer">Marketing Officer</Label>
                                    <Input type="text" name="marketingOfficer" id="marketingOfficer" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="noTelpMarketing">No Telp Marketing</Label>
                                    <Input type="text" name="noTelpMarketing" id="noTelpMarketing" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="reserve1">Reserve 1</Label>
                                    <Input type="text" name="reserve1" id="reserve1" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="reserve2">Reserve 2</Label>
                                    <Input type="text" name="reserve2" id="reserve2" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="reserve3">Reserve 3</Label>
                                    <Input type="text" name="reserve3" id="reserve3" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="reserve4">Reserve 4</Label>
                                    <Input type="text" name="reserve4" id="reserve4" />
                                </FormGroup>
                                <FormGroup>
                                    <Label className="w-100" for="reserve5">Reserver 5</Label>
                                    <Input type="text" name="reserve5" id="reserve5" />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Col md="12" style={{backgroundColor: '#ff4d4d'}} className="p-2">
                            <h4 className="text-white text-center">DAFTAR SERTIFIKAT</h4>
                        </Col>
             
                    {/* dialog Sertifikat */}
                <Dialog className="client-dialog" open={this.state.opensetifikat} onClose={this.handleClosesetifikat} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">Dokumen</DialogTitle>
					<DialogContent>
						<div>
							<form onSubmit={this.onFormSubmitSertifikat.bind(this)}>
								<div className="row">
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                    <Label className="w-100" for="reserve2">Type Dokumen</Label>    
                                    <select className="form-control" id="DokType">
                                        <option>SHM</option>
                                    </select>
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                    <Label className="w-100" for="reserve2">No Dokumen</Label>   
                                    <Input type="text" name="NoDokumen" id="NoDokumen" />
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                    <Label className="w-100" for="reserve2">NB</Label> 
                                    <Input type="text" name="NB" id="NB" />
									</div>
									<div className="col-sm-12 col-md-12 col-lg-12">
                                    <Label className="w-100" for="reserve2">Atas Nama</Label> 
                                    <Input type="text" name="AtasNama" id="AtasNama" />
									</div>
								</div>
								<div className="pt-25 text-right">
									<Button variant="contained" onClick={this.handleClosesetifikat} className="btn-danger mr-15 text-white">
										Cancel
            		                </Button>
									<Button className="btn-success text-white text-capitalize" type="submit">Submit</Button>
								</div>
							</form>
						</div>
					</DialogContent>
				</Dialog>
            {/* dialog zipcode */}
            <Col md="12">
            <RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={<div><Button onClick={() => this.handleClickopensetifikat()} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon"> ADD New</Button></div>}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
            </Col>
                        <Col md="12">
                        <div class="d-flex justify-content-center">

+                        <div class="p-2"><Button color="primary">SIMPAN</Button></div>

+                        <div class="p-2"><Button  color="primary">DEDUP</Button></div>

+                        <div class="p-2"><Button  color="primary">PROCEED</Button></div>

+                        </div>

                         </Col>

+                     

                     </Col>

                 </Row>

             </div>
            
      
         
        );
    }
}