/**
 * Miscellaneous Widgets Page
 */

import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
 
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
 
 //page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

 //rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

 //rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';

import AddCertificate from 'Routes/main/internal-request-order/re-appraisal/AddCertificate';
import AddDedup from 'Routes/main/internal-request-order/re-appraisal/AddDedup';

import SearchZipcode from 'Routes/main/internal-request-order/re-appraisal/SearchZipcode';
import AssesmentHistory from 'Routes/main/internal-request-order/re-appraisal/AssesmentHistory';
import SearchIdColl from 'Routes/main/internal-request-order/re-appraisal/SearchIdColl';

function TabContainer({ children }) {
    return (
        <Typography component="div">
            {children}
        </Typography>
    );
}

 
 
export default class InternalRequestOrderReAppraisalReAppraisal extends Component {
    state = {
        activeIndex: 1,
        selectedDate: moment(),
        openpilihdistribusi: false,
        jenisJaminan:[],
        jenisFasilitas:[],
        cabang:[],
        jenisPermintaanTransaksi:[],
        jenisPenilaian:[],
        dataassygn:[],
        reason:[],
        fields: {},
        fieldsertifikat:{},
        errors: {},
        showdedup:false,
        loading: false
    }
    constructor(props, context) {
        super(props, context);
    }
    
    
    componentDidMount() {
         AparsialApi().get('RF_FACILITY/Get')
         .then(res => {
             if (res.status == 200) {
         
             this.setState({
                 jenisFasilitas:  res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_FACILITY", err);
         });


         AparsialApi().get('RF_TYPE_OF_GUARANTEE/Get')
         .then(res => {
             if (res.status == 200) {
                
             this.setState({
                 jenisJaminan: res.data,
             });
         }
         }).catch(err => {
             console.error("error RF_TYPE_OF_GUARANTEE", err);
         });

         AparsialApi().get('RFBRANCH/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
          
             this.setState({
                 cabang: resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RFBRANCH", err);
         });

         AparsialApi().get('RF_FLAG_APPRAISAL/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
                 
             this.setState({
                 jenisPermintaanTransaksi: resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RF_FLAG_APPRAISAL", err);
         });

         AparsialApi().get('RF_ASSESSMENT_TYPE/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
                
             this.setState({
                 jenisPenilaian: resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RF_ASSESSMENT_TYPE", err);
         });

        
    }

    handleshowdedup = () =>{
      
        this.setState({
			showdedup: true
		});
    }
  
    handleClickopenPilihDistribusi = () => {
		this.setState({
			openpilihdistribusi: true
		});
	};
 
    handleClosePilihDistribusi = () => {
		this.setState({
            openpilihdistribusi: false,
		
		});
    };

     
   /// submit form data
	onFormSubmit(e) {
		e.preventDefault();
        alert("Form has test.")
    }

    onClickEdit = (idx) => {
		 const { users } = this.state;
		 this.props.history.push(`/app/main/user-management-maker/list-user/${users[idx].id}`);
	}

	/**
	 * On Delete
	 */
	onClickDelete = (idx) => {
		 const { users } = this.state;
		 this.refs.deleteConfirmationDialog.open();
		 this.setState({ selectedUser: users[idx] });
    }
 
    handleChange(event, value) {
        const menu = ["main","re-appraisal","document-checking","payment"];
        this.props.history.push(`/app/main/internal-request-order/re-appraisal/${menu[value]}/1`);
        this.setState({ activeIndex: value });
    }
	handleDateChange = (date) => {
		this.setState({ selectedDate: date });
	};

 
      handletextChange(field, e) {
		let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
        if(field =='jenisPenilaian')
        {
            AparsialApi().get('ASSESSMENT_HISTORY/Get')
            .then(res => {
                if (res.status == 200) {
                    const list = res.data.filter(l => {
                        return l.assessmenT_TYPE_ID.toLowerCase().match(fields[field].toLowerCase());
                      });
                    
                      if(list.length >0)
                      {
                        const datanew = list.map((group, index) => [
                            convertDateASP(group.assiG_DATE, "DD-MM-YYYY"),
                            group.valuer,
                            group.assiG_BY,
                        ]);
    
                        this.setState({
                            dataassygn: datanew,
                        });
                      }
                      else
                      {
                        this.setState({
                            dataassygn: [],
                        });
                      }
                   
            }
            }).catch(err => {
                console.error("error RF_ASSESSMENT_TYPE", err);
            });
        }
		
    }
   
    setzipcode=(select)=>{
        let fields = this.state.fields;
 
        fields['kodePos'] =  select[0];
        fields['Kecamatan'] =  select[1];
        fields['Kabupaten'] =  select[2];
        fields['Propinsi'] =  select[3];
        fields['Kelurahan'] =  select[4];
        this.setState({ fields});
       
    }
    setidcoll=(select)=>{
        console.log(select);
        let fields = this.state.fields;
        fields['idColl'] =  select[0];
        this.setState({ fields });
    }
    render() {
        const { activeIndex,
            selectedDate,
            openpilihdistribusi, 
            errors,
            loading ,
            jenisJaminan,
            jenisFasilitas,
            cabang,
            jenisPermintaanTransaksi,
            jenisPenilaian,
            reason,
        } = this.state;
        const extendUrl = "/app/main/internal-request-order";

     
      

        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        return (
            
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Re Appraisal" match={this.props.match} />
              
                {/* START DIALOG PILIH DISTRIBUSI */}
                <Dialog className="client-dialog" open={this.state.openpilihdistribusi} onClose={this.handleClosePilihDistribusi} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">PILIH DISTRIBUSI</DialogTitle>
                    <DialogContent>
                        <div>
                            <form onSubmit={this.onFormSubmit.bind(this)}>
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                    awd
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                        awd
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-lg-12 mb-20">
                                    adw
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-lg-12">
                                        awd
                                    </div>
                                </div>
                                <div className="pt-25 text-right">
                                    <MatButton variant="contained" onClick={this.handleClosePilihDistribusi} className="btn-danger mr-15 text-white">Cancel</MatButton>
                                    <MatButton className="btn-success text-white text-capitalize" type="submit">Submit</MatButton>
                                </div>
                            </form>
                        </div>
                    </DialogContent>
                </Dialog>
                {/* END DIALOG PILIH DISTRIBUSI */}
               
                <Tabs value={activeIndex} onChange={(e, value) => this.handleChange(e, value)}>
                    <Tab label="Main" />
                    <Tab label="Re Appraisal" />
                    <Tab label="Document Checking" />
                    <Tab label="Payment" />
                </Tabs>
                <TabContainer>
                    <RctCollapsibleCard className="text-danger" heading="APPLICATION DATA">
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField id="idColl" label="ID Coll" onChange={this.handletextChange.bind(this, "idColl")} value={this.state.fields.idColl || ''} />
                                    <SearchIdColl updateidcoll={this.setidcoll.bind(this)}  />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noPermohonan" fullWidth label="No Permohonan" onChange={this.handletextChange.bind(this, "noPermohonan")} value={this.state.fields.noPermohonan || ''} />
                                </div>
                                <div className="form-group">
                                    <DatePicker
                                        label="Tanggal Permohonan"
                                        id="tanggalPermohonan"
                                        format="DD/MM/YYYY"
                                        value={selectedDate}
                                        onChange={this.handleDateChange}
                                        animateYearScrolling={false}
                                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                        fullWidth
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisPermintaanTransaksi" select label="Jenis Permintaan Transaksi"
                                        onChange={this.handletextChange.bind(this, "jenisPermintaanTransaksi")} 
                                        value={this.state.fields.jenisPermintaanTransaksi || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Permintaan Transaksi"
                                        fullWidth>
                                        {jenisPermintaanTransaksi.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisFasilitas" select label="Jenis Fasilitas"
                                        onChange={this.handletextChange.bind(this, "jenisFasilitas")} 
                                        value={this.state.fields.jenisFasilitas || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Fasilitas"
                                        fullWidth>
                                        {jenisFasilitas.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="namaDebitur" fullWidth label="Nama Debitur" onChange={this.handletextChange.bind(this, "namaDebitur")} 
                                        value={this.state.fields.namaDebitur || ''}  />
                                </div>
                                <div className="form-group">
                                    <TextField id="nilaiExposure" fullWidth label="Nilai Exposure" onChange={this.handletextChange.bind(this, "nilaiExposure")} 
                                        value={this.state.fields.nilaiExposure || ''}  />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisJaminan" select label="Jenis Jaminan"
                                         onChange={this.handletextChange.bind(this, "jenisJaminan")} 
                                         value={this.state.fields.jenisJaminan || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Jenis Jaminan"
                                        fullWidth>
                                        {jenisJaminan.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="deskripsiJaminan" fullWidth label="Deskripsi Jaminan" onChange={this.handletextChange.bind(this, "deskripsiJaminan")} 
                                         value={this.state.fields.deskripsiJaminan || ''}  />
                                </div>
                                <div className="form-group">
                                    <TextField id="alamatJaminan1" fullWidth label="Alamat Jaminan" value="" onChange={this.handletextChange.bind(this, "alamatJaminan1")} 
                                         value={this.state.fields.alamatJaminan1 || ''} />
                                    <TextField id="alamatJaminan2" fullWidth label="" onChange={this.handletextChange.bind(this, "alamatJaminan2")} 
                                         value={this.state.fields.alamatJaminan2 || ''} />
                                    <TextField id="alamatJaminan3" fullWidth label="" onChange={this.handletextChange.bind(this, "alamatJaminan3")} 
                                         value={this.state.fields.alamatJaminan3 || ''} />
                                </div>
                                <div className="form-group">
                                    <TextField disabled id="kodePos" label="Kode Pos" onChange={this.handletextChange.bind(this, "kodePos")} value={this.state.fields["kodePos"] ?  this.state.fields["kodePos"] : ''} readOnly />
                                    <SearchZipcode updatezip={this.setzipcode.bind(this)}  />
                                </div>
                                <div className="form-group">
                                    <TextField id="kelurahan" fullWidth label="Kelurahan"  onChange={this.handletextChange.bind(this, "Kelurahan")} value={ this.state.fields["Kelurahan"] ?  this.state.fields["Kelurahan"] : ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="kecamatan" fullWidth label="Kecamatan" onChange={this.handletextChange.bind(this, "Kecamatan")} value={ this.state.fields["Kecamatan"] ?  this.state.fields["Kecamatan"] : ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="kabupaten" fullWidth label="Kabupaten" onChange={this.handletextChange.bind(this, "Kabupaten")} value={ this.state.fields["Kabupaten"] ?  this.state.fields["Kabupaten"] : ''} readOnly  />
                                </div>
                                <div className="form-group">
                                    <TextField id="propinsi" fullWidth label="Propinsi" onChange={this.handleDateChange.bind(this, "Propinsi")} value={ this.state.fields["Propinsi"] ?  this.state.fields["Propinsi"] : ''} readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="TjenisPenilaian" select label="Jenis Penilaian"
                                        onChange={this.handletextChange.bind(this, "jenisPenilaian")} 
                                        value={this.state.fields.jenisPenilaian || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {jenisPenilaian.map(option => (
                                        <MenuItem key={option.name} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                
                                
                            </div>
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField id="namaPIC1" fullWidth label="Nama PIC 1"
                                      onChange={this.handletextChange.bind(this, "namaPIC1")} 
                                      value={this.state.fields.namaPIC1 || ''} 
                                       />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noWaPIC1" fullWidth label="No WA PIC 1"
                                     onChange={this.handletextChange.bind(this, "noWaPIC1")} 
                                     value={this.state.fields.noWaPIC1 || ''} 
                                      />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noTelpPIC1" fullWidth label="No Telp PIC 1" 
                                     onChange={this.handletextChange.bind(this, "noTelpPIC1")} 
                                     value={this.state.fields.noTelpPIC1 || ''}
                                     />
                                </div>
                                <div className="form-group">
                                    <TextField id="namaPIC2" fullWidth label="Nama PIC 2" 
                                     onChange={this.handletextChange.bind(this, "namaPIC2")} 
                                     value={this.state.fields.namaPIC2 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noWaPIC2" fullWidth label="No WA PIC 2"
                                    onChange={this.handletextChange.bind(this, "noWaPIC2")} 
                                    value={this.state.fields.noWaPIC2 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="noTelpPIC2" fullWidth label="No Telp PIC 2" 
                                      onChange={this.handletextChange.bind(this, "noTelpPIC2")} 
                                      value={this.state.fields.noTelpPIC2 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="Tcabang" select label="Cabang"
                                           onChange={this.handletextChange.bind(this, "Cabang")} 
                                           value={this.state.fields.Cabang || ''}
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="cabang"
                                        fullWidth>
                                        {cabang.map(option => (
                                        <MenuItem key={option.branchid} value={option.branchname}>
                                            {option.branchname}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="marketingOfficer" fullWidth label="Marketing Officer" 
                                     onChange={this.handletextChange.bind(this, "marketingOfficer")} 
                                     value={this.state.fields.marketingOfficer || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField type="number" id="NoTelpMarketing" fullWidth label="No Telp Marketing" 
                                     onChange={this.handletextChange.bind(this, "NoTelpMarketing")} 
                                     value={this.state.fields.NoTelpMarketing || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve1" fullWidth label="Reserve 1" 
                                     onChange={this.handletextChange.bind(this, "reserve1")} 
                                     value={this.state.fields.reserve1 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve2" fullWidth label="Reserve 2" 
                                      onChange={this.handletextChange.bind(this, "reserve2")} 
                                      value={this.state.fields.reserve2 || ''}
                                      />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve3" fullWidth label="Reserve 3" 
                                     onChange={this.handletextChange.bind(this, "reserve3")} 
                                     value={this.state.fields.reserve3 || ''}
                                     />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve4" fullWidth label="Reserve 4" 
                                      onChange={this.handletextChange.bind(this, "reserve4")} 
                                      value={this.state.fields.reserve4 || ''}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="reserve5" fullWidth label="Reserve 5"
                                    onChange={this.handletextChange.bind(this, "reserve5")} 
                                    value={this.state.fields.reserve5 || ''}
                                    />
                                </div>
                            </div>
                        </div>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SIMPAN</MatButton>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">PROCEED</MatButton>

                    </RctCollapsibleCard>
                    
                </TabContainer>
                
            </div>
            
      
         
        );
    }
}