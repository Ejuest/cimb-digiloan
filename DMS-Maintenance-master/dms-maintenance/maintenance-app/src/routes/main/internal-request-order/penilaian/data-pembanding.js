/**
 * Miscellaneous Widgets Page
 */

import React, { Component, useState } from 'react';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

 //page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

 //rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

function TabContainer({ children }) {
    return (
        <Typography component="div">
            {children}
        </Typography>
    );
}
 
export default class InternalRequestOrderPenilaianDataPembanding extends Component {
    state = {
        activeIndex: 2,
        activeMainIndex: 1,
        activeSubMainIndex: 0,
        openSearch: false,
        openForm: false,
        fields: {},
        errors: {},
        loading: false
    }
    constructor(props, context) {
        super(props, context);
    }
    
    componentDidMount() {
        
    }
    handleChange(event, value) {
        const menu = ["input-pembanding-tanah","input-pembanding-bangunan","data-pembanding"];
        this.props.history.push(`/app/main/internal-request-order/penilaian/${menu[value]}/1`);
        this.setState({ activeIndex: value });
    }
    handleMainChange(event,value){
        const menu = ["new-appraisal/main","penilaian/input-pembanding-tanah"];
        this.props.history.push(`/app/main/internal-request-order/${menu[value]}/1`);
        this.setState({ activeMainIndex: value });
    }
    handleSubMainChange(event,value){
        const menu = ["input-pembanding-tanah","template-tanah","ringkasan-hasil-penilaian","upload-foto"];
        this.props.history.push(`/app/main/internal-request-order/penilaian/${menu[value]}/1`);
        this.setState({ activeSubMainIndex: value });
    }
    openSearch = ()=>{
        this.setState({ openSearch: true });
    }
    setModalSearch = (select) => {
        this.setState({ openSearch: select });
    }
    setSearch=(select)=>{
        console.log(select);
        // let DEBITUR = this.state.DEBITUR;
  
        // DEBITUR.ZIP_CODE_ID=  select[0];
        // DEBITUR.DISTRICT_ID =  select[1];
        // DEBITUR.SUBDISTRICT_ID =  select[2];
        // DEBITUR.PROVINCE_ID =  select[3];
        // DEBITUR.CITY_ID =  select[4];
        // this.setState({ DEBITUR});
        // localStorage.setItem('DEBITUR',JSON.stringify( DEBITUR));
        this.setState({openSearch: false});
    }
    openForm = ()=>{
        this.setState({ openForm: true });
    }
    setModalForm = (select) => {
        this.setState({ openForm: select });
    }
    setForm=(select)=>{
        console.log(select);
        // let DEBITUR = this.state.DEBITUR;
  
        // DEBITUR.ZIP_CODE_ID=  select[0];
        // DEBITUR.DISTRICT_ID =  select[1];
        // DEBITUR.SUBDISTRICT_ID =  select[2];
        // DEBITUR.PROVINCE_ID =  select[3];
        // DEBITUR.CITY_ID =  select[4];
        // this.setState({ DEBITUR});
        // localStorage.setItem('DEBITUR',JSON.stringify( DEBITUR));
        this.setState({openForm: false});
    }
    render() {
        const { activeIndex,
            activeMainIndex,
            activeSubMainIndex,
            openSearch,
            openForm,
            fields,
            errors,
            loading
        } = this.state;
        const extendUrl = "/app/main/internal-request-order";

        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };

        const dataPembandingTanah1 = [
            {
                NAME: "Data Pembanding Tanah 1",
                ID: "Data Pembanding Tanah 1",
            },
        ];
        const dataPembandingTanah2 = dataPembandingTanah1;
        const dataPembandingTanah3 = dataPembandingTanah1;

        const dataPembandingBangunan1 = [
            {
                NAME: "Data Pembanding Bangunan 1",
                ID: "Data Pembanding Bangunan 1",
            },
        ];
        const dataPembandingBangunan2 = dataPembandingBangunan1;
        const dataPembandingBangunan3 = dataPembandingBangunan1;

        const tipeBangunan = [
            {
                NAME: "Keatas",
                ID: "Keatas",
            },
            {
                NAME: "Menengah",
                ID: "Menengah",
            },
        ];
        const legalitasBangunan = [
            {
                NAME: "SHM",
                ID: "SHM",
            },
            {
                NAME: "AJB",
                ID: "AJB",
            },
        ];
        const ddlObjek = [
            {
                NAME: "Sama dengan objek",
                ID: "Sama dengan objek",
            },
            {
                NAME: "Sama dengan objek 2",
                ID: "Sama dengan objek 2",
            },
        ];
        const posisiUnit = [
            {
                NAME: "Depan",
                ID: "Depan",
            },
            {
                NAME: "Belakang",
                ID: "Belakang",
            },
        ];
        const lebarJalan = [
            {
                NAME: "1",
                ID: "1",
            },
            {
                NAME: "2",
                ID: "2",
            },
        ];
        const materialJalan = [
            {
                NAME: "Aspal",
                ID: "Aspal",
            },
            {
                NAME: "Paving Blok",
                ID: "Paving Blok",
            },
        ];
        const kondisiJalan = [
            {
                NAME: "Bagus",
                ID: "Bagus",
            },
            {
                NAME: "Jelek",
                ID: "Jelek",
            },
        ];
        const aksesibilitas = [
            {
                NAME: "Baik",
                ID: "Baik",
            },
            {
                NAME: "Kurang Baik",
                ID: "Kurang Baik",
            },
        ];
        const view = [
            {
                NAME: "Jalan Raya",
                ID: "Jalan Raya",
            },
            {
                NAME: "Sungai",
                ID: "Sungai",
            },
        ];
        const peruntukanZoning = [
            {
                NAME: "Pemukiman / Rumah Tinggal",
                ID: "Pemukiman / Rumah Tinggal",
            },
            {
                NAME: "Sewa",
                ID: "Sewa",
            },
        ];
        const karakteristikEkonomi = [
            {
                NAME: "Strata Golongan Berpendapatan",
                ID: "Strata Golongan Berpendapatan",
            },
            {
                NAME: "Pedagang",
                ID: "Pedagang",
            },
        ];
        const fasilitasLingkungan = [
            {
                NAME: "Bagus",
                ID: "Bagus",
            },
            {
                NAME: "Kurang Bagus",
                ID: "Kurang Bagus",
            },
        ];
        const bentukTanah = [
            {
                NAME: "Menyerupai Persegi Panjang",
                ID: "Menyerupai Persegi Panjang",
            },
            {
                NAME: "Persegi Panjang",
                ID: "Persegi Panjang",
            },
        ];
        const elevasiTanah = [
            {
                NAME: "Diatas Permukaan Tanah",
                ID: "Diatas Permukaan Tanah",
            },
            {
                NAME: "Diatas Permukaan Laut",
                ID: "Diatas Permukaan Laut",
            },
        ];
        const konturTanah = [
            {
                NAME: "Tanah Matang",
                ID: "Tanah Matang",
            },
            {
                NAME: "Tanah Matang",
                ID: "Tanah Matang",
            },
        ];
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Main" match={this.props.match} />
                <Tabs value={activeMainIndex} onChange={(e, value) => this.handleMainChange(e, value)}>
                    <Tab label="Main" />
                    <Tab label="Penilaian" />
                </Tabs>
                <Tabs value={activeSubMainIndex} onChange={(e, value) => this.handleSubMainChange(e, value)}>
                    <Tab label="Data Pembanding" />
                    <Tab label="Template Tanah" />
                    <Tab label="Ringkasan Hasil Penilaian" />
                    <Tab label="Upload Foto" />
                </Tabs>
                <Tabs value={activeIndex} onChange={(e, value) => this.handleChange(e, value)}>
                    <Tab label="Input Pembanding Tanah" />
                    <Tab label="Input Pembanding Bangunan" />
                    <Tab label="Data Pembanding" />
                </Tabs>
                <TabContainer>
                    <RctCollapsibleCard className="text-danger" heading="DATA PEMBANDING">
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <div className="form-group">
                                    <label>DATA PEMBANDING TANAH</label>
                                </div>
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <div className="form-group">
                                    <TextField id="dataPembandingTanah1" select label="Data Pembanding 1"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {dataPembandingTanah1.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <div className="form-group">
                                    <TextField id="dataPembandingTanah2" select label="Data Pembanding 2"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {dataPembandingTanah2.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <div className="form-group">
                                    <TextField id="dataPembandingTanah3" select label="Data Pembanding 3"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {dataPembandingTanah3.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <div className="form-group">
                                    <label>DATA PEMBANDING BANGUNAN</label>
                                </div>
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <div className="form-group">
                                    <TextField id="dataPembandingBangunan1" select label="Data Pembanding 1"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {dataPembandingBangunan1.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <div className="form-group">
                                    <TextField id="dataPembandingBangunan2" select label="Data Pembanding 2"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {dataPembandingBangunan2.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <div className="form-group">
                                    <TextField id="dataPembandingBangunan3" select label="Data Pembanding 3"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        fullWidth>
                                        {dataPembandingBangunan3.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th className="w-20">Field Name</th>
                                            <th className="w-20">Objek</th>
                                            <th className="w-20">Data Pembanding 1</th>
                                            <th className="w-20">Data Pembanding 2</th>
                                            <th className="w-20">Data Pembanding 3</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>DATA COMPARE</td>
                                            <td>
                                                <TextField id="dataCompare" fullWidth label="" value="Tanah Kosong" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Jarak Dengan Aktiva</td>
                                            <td>&nbsp;</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Titik Koordinat Objek</td>
                                            <td>
                                                <TextField id="titikKoordinatObjek" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Lokasi Objek</td>
                                            <td>
                                                <TextField id="lokasiObjek" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Kelurahan</td>
                                            <td>
                                                <TextField id="kelurahan" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Kecamatan</td>
                                            <td>
                                                <TextField id="kecamatan" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Kota/Kabupaten</td>
                                            <td>
                                                <TextField id="kotaKab" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Kode Pos</td>
                                            <td>
                                                <TextField id="kodePos" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Propinsi</td>
                                            <td>
                                                <TextField id="propinsi" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Luas Tanah (m2)</td>
                                            <td>
                                                <TextField id="luasTanah" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Total Luas Bangunan (m2)</td>
                                            <td>
                                                <TextField id="totalLuasBangunan" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Dibangun/Renovasi</td>
                                            <td>
                                                <TextField id="tahunDibangunRenovasi" fullWidth label="" value="" />
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Tipe Bangunan</td>
                                            <td>
                                                <TextField id="tipeBangunan" select label="Tipe Bangunan"
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {tipeBangunan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>    
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Legalitas Bangunan</td>
                                            <td>
                                                <TextField id="legalitasBangunan" select label="Legalitas Bangunan"
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {legalitasBangunan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>    
                                            </td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Sumber Data</td>
                                            <td>&nbsp;</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Telepon Pembanding</td>
                                            <td>&nbsp;</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Sebagai</td>
                                            <td>&nbsp;</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Waktu Penawaran/Transaksi</td>
                                            <td>&nbsp;</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Daftar Harga (Rp.)</td>
                                            <td>&nbsp;</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Transaksi/Penawaran</td>
                                            <td>&nbsp;</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                            <td>retrieved</td>
                                        </tr>
                                        <tr>
                                            <td>Perkiraan Diskon</td>
                                            <td>&nbsp;</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                        </tr>
                                        <tr>
                                            <td>Harga Setelah Diskon</td>
                                            <td>&nbsp;</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                        </tr>
                                        <tr>
                                            <td>Perkiraan CRN Bangunan (Rp. per m2)</td>
                                            <td>&nbsp;</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                        </tr>
                                        <tr>
                                            <td>Perkiraan Indikasi Nilai Pasar Bangunan (Rp. per m2)</td>
                                            <td>&nbsp;</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                        </tr>
                                        <tr>
                                            <td>Indikasi Nilai Pasar Bangunan (Rp.)</td>
                                            <td>&nbsp;</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                        </tr>
                                        <tr>
                                            <td>Indikasi Nilai Tanah (Rp.)</td>
                                            <td>&nbsp;</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                        </tr>
                                        <tr>
                                            <td>Indikasi Nilai Tanah (Rp. per m2)</td>
                                            <td>&nbsp;</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                            <td>formula</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th className="w-20">Range Informasi Harga /m2</th>
                                            <th className="text-center w-20">Formula</th>
                                            <th className="text-center w-60" colSpan="6">Formula</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Lokasi</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Secara Umum (diluar objek)</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="secaraUmum1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="secaraUmum2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="secaraUmum3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Secara Khusus (dalam objek yg sama)</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Posisi Unit</td>
                                            <td>
                                                <TextField id="posisiUnit1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {posisiUnit.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="posisiUnit2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {posisiUnit.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="posisiUnit3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {posisiUnit.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="posisiUnit4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {posisiUnit.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lebar Jalan (m2)</td>
                                            <td>
                                                <TextField id="lebarJalan1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {lebarJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="lebarJalan2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {lebarJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="lebarJalan3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {lebarJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="lebarJalan4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {lebarJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Material Jalan</td>
                                            <td>
                                                <TextField id="materialJalan1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {materialJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="materialJalan2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {materialJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="materialJalan3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {materialJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="materialJalan4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {materialJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kondisi Jalan</td>
                                            <td>
                                                <TextField id="kondisiJalan1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {kondisiJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="kondisiJalan2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {kondisiJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="kondisiJalan3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {kondisiJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="kondisiJalan4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {kondisiJalan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Aksesibilitas</td>
                                            <td>
                                                <TextField id="aksesibilitas1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {aksesibilitas.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="aksesibilitas2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {aksesibilitas.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="aksesibilitas3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {aksesibilitas.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="aksesibilitas4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {aksesibilitas.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>View</td>
                                            <td>
                                                <TextField id="view1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {view.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="view2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {view.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="view3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {view.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="view4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {view.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Peruntukan/Zoning</td>
                                            <td>
                                                <TextField id="peruntukanZoning1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {peruntukanZoning.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="peruntukanZoning2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {peruntukanZoning.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="peruntukanZoning3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {peruntukanZoning.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="peruntukanZoning4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {peruntukanZoning.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Karakteristik Ekonomi</td>
                                            <td>
                                                <TextField id="karakteristikEkonomi1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {karakteristikEkonomi.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="karakteristikEkonomi2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {karakteristikEkonomi.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="karakteristikEkonomi3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {karakteristikEkonomi.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="karakteristikEkonomi4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {karakteristikEkonomi.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Fasilitas Lingkungan (fasum/Utilitas)</td>
                                            <td>
                                                <TextField id="fasilitasLingkungan1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {fasilitasLingkungan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="fasilitasLingkungan2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {fasilitasLingkungan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="fasilitasLingkungan3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {fasilitasLingkungan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="fasilitasLingkungan4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {fasilitasLingkungan.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Karakteristik Fisik</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Legalitas Kepemilikan</td>
                                            <td>
                                                <TextField id="legalitasKempilikan1" label="" value="SHM"></TextField>
                                            </td>
                                            <td>
                                                <TextField id="legalitasKempilikan2" label="" value="SHM"></TextField>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="legalitasKempilikan3" label="" value="SHM"></TextField>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="legalitasKempilikan2" label="" value="SHM"></TextField>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Perbedaan Luas (m2)</td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <TextField id="perbedaanLuas1" type="number" label=""></TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="perbedaanLuas2" type="number" label=""></TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="perbedaanLuas3" type="number" label=""></TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bentuk Tanah</td>
                                            <td>
                                                <TextField id="bentukTanah1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {bentukTanah.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="bentukTanah2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {bentukTanah.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="bentukTanah3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {bentukTanah.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="bentukTanah4" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {bentukTanah.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lebar Bagian Depan Aset (m)</td>
                                            <td>
                                                <TextField id="lebarBagianDepanAset1" type="number" label=""></TextField>
                                            </td>
                                            <td>
                                                <TextField id="lebarBagianDepanAset2" type="number" label=""></TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus1" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="lebarBagianDepanAset3" type="number" label=""></TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus2" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                            <td>
                                                <TextField id="lebarBagianDepanAset4" type="number" label=""></TextField>
                                            </td>
                                            <td>
                                                <TextField id="secaraKhusus3" select label=""
                                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                                    SelectProps={{
                                                        MenuProps: {
                                                        },
                                                    }}
                                                    fullWidth>
                                                    {ddlObjek.map(option => (
                                                    <MenuItem key={option.NAME} value={option.ID}>
                                                        {option.NAME}
                                                    </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </RctCollapsibleCard>
                </TabContainer>
                
            </div>
        );
    }
}