import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';


class SearchDataPembandingBangunan extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
        const newdata = [
            ['Jl. Raya Bogor', 'Mustika Indah', 'Block A20', 'Cisarua','Cisarua','Bogor','Jawa Barat', '90876','Mewah','1.500.000','-', '01/03/2014', 0],
        ];
        this.setState({
            data: newdata
        });
    }
    
    handleClose = () => {
       this.props.setModalSearch(false);
    }
    
    onSelect =(idx)=>{
     //   let fields = this.state.fields;
        
        // fields['kodePos'] =  selectedGroup[0];
        // fields['Kecamatan'] =  selectedGroup[1];
        // fields['Kabupaten'] =  selectedGroup[2];
        // fields['Propinsi'] =  selectedGroup[3];
        // fields['Kelurahan'] =  selectedGroup[4];
        // localStorage.setItem("SelectZipcode",JSON.stringify(fields));
       this.props.updateSearch(idx);
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        const columns = ["Nama Jalan","Perumahan","Cluster","Kelurahan","Kecamatan","Kabupaten/Kota","Propinsi","Kode Pos","Tipe Bangunan","Biaya Bangunan Baru/m2","Source","Tanggal Data",
        {
            name: "Action",
            options: {
                filter: false,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <div>
                            <MatButton onClick={() => this.onSelect(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Select</MatButton>
                        </div>
                    );
                }
            }
        }
        ];
        return (
            <React.Fragment>
                {/* START DIALOG KODE POS */}
                <Dialog maxWidth="md" className="client-dialog" open={this.props.stateModalSearch} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">SEARCH</DialogTitle>
                    <DialogContent>
                        <RctCollapsibleCard fullBlock>
                            
                            {this.state.loading &&
                                <RctSectionLoader />
                            }
                            <MUIDataTable
                                title=""
                                data={this.state.data}
                                columns={columns}
                                options={options}
                            />
                        </RctCollapsibleCard>
                        
                    </DialogContent>
                </Dialog>
                {/* END DIALOG KODE POS */}
                
                
        </React.Fragment>
        );
    }
}

export default SearchDataPembandingBangunan
 