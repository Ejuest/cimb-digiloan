import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import MatButton from '@material-ui/core/Button';

import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class GridPembandingBangunan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            errors: {},
            loading: false
        };
    }
      
    componentDidMount() {
        const newdata = [
            ['1', 'Menengah', '2017', 'DATA PEMBANDING 1', 0],
            ['2', 'Menengah', '2010', 'DATA PEMBANDING 2', 1],
            ['3', 'Menengah', '2013', 'DATA PEMBANDING 3', 2]
        ];
        this.setState({
            data: newdata
        });
    }
    
    onSelect =(idx)=>{
                
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        const columns = ["No","Spesifikasi Umum Bangunan","Tahun Bangun / Renovasi","Data Pembanding",
       {
           name: "Action",
           options: {
               filter: false,
               sort: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                   return (
                       <div>
                           <MatButton onClick={() => this.onSelect(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Edit</MatButton>
                           <MatButton onClick={() => this.onSelect(value)} variant="contained" className="mr-10 mb-10 text-white btn-icon btn-danger">Hapus</MatButton>
                       </div>
                   );
               }
           }
       }
   ];
   //  END DT SERTIFIKAT
        return (
            <React.Fragment>                
                <RctCollapsibleCard fullBlock>
                    {this.state.loading &&
                        <RctSectionLoader />
                    }
                    <MUIDataTable
                        title=""
                        data={this.state.data}
                        columns={columns}
                        options={options}
                    />
                </RctCollapsibleCard>
        </React.Fragment>
        );
    }
}

export default GridPembandingBangunan
 