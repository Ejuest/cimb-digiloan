import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import MatButton from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';


class SaranaPelengkap extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open:false,
            data:[],
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
        const newdata = [
            ['', '', '', 0],
        ];
        this.setState({
            data: newdata
        });
    }
    
    handleClose = () => {
       this.setState({
           open: false,
       })
    }
    handleOpen = () => {
        this.setState({
            open: true,
        })
    }
    onSelect =(idx)=>{
     //   let fields = this.state.fields;
        
        // fields['kodePos'] =  selectedGroup[0];
        // fields['Kecamatan'] =  selectedGroup[1];
        // fields['Kabupaten'] =  selectedGroup[2];
        // fields['Propinsi'] =  selectedGroup[3];
        // fields['Kelurahan'] =  selectedGroup[4];
        // localStorage.setItem("SelectZipcode",JSON.stringify(fields));
       this.props.updateSearch(idx);
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        const columns = ["Sarana Pelengkap","IM","Harga /m2",
        {
            name: "Action",
            options: {
                filter: false,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <div>
                            <MatButton variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">EDIT</MatButton>
                            <MatButton variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">HAPUS</MatButton>
                        </div>
                    );
                }
            }
        }
        ];
        
        return (
            <React.Fragment>
                {/* START DIALOG KODE POS */}
                <Dialog maxWidth="md" className="client-dialog" open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">SEARCH</DialogTitle>
                    <DialogContent>
                        <div className="form-group">
                            <TextField id="saranaPelengkap" fullWidth label="Sarana Pelengkap" />
                        </div>
                        <div className="form-group">
                            <TextField id="im" fullWidth label="IM" />
                        </div>
                        <div className="form-group">
                            <TextField id="Hara /m2" type="number" fullWidth label="Harga /m2" />
                        </div>
                        <div className="form-group">
                            <MatButton variant="contained" className="btn-danger mr-10 text-white">SIMPAN</MatButton>
                        </div>
                    </DialogContent>
                </Dialog>
                <div className="form-group">
                    <MatButton variant="contained" onClick={this.handleOpen} className="btn-danger mr-10 text-white">TAMBAH</MatButton>
                </div>
                <div className="form-group">
                    <MUIDataTable
                        title=""
                        data={this.state.data}
                        columns={columns}
                        options={options}
                    />
                </div>
                {/* END DIALOG KODE POS */}
                
                
        </React.Fragment>
        );
    }
}

export default SaranaPelengkap
 