import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import MatButton from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import SaranaPelengkap from 'Routes/main/internal-request-order/penilaian/sarana-pelengkap';


class FormDataPembandingBangunan extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],
            sumberDataPembanding:[],
            spesifikasiUmumBangunan:[],
            pondasi:[],
            struktur:[],
            rangkaAtap:[],
            penutupAtap:[],
            penutupAtap:[],
            plafon:[],
            dinding:[],
            pintu:[],
            jendela:[],
            lantai:[],
            utilitas:[],
            profesionalFee:[],
            biayaPerajin: [],
            keuntunganKontraktor: [],
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
        
        const sumberDataPembanding = [
            {
                NAME: "Data Pembanding 1",
                ID: "Data Pembanding 1",
            },
            {
                NAME: "Data Pembanding 2",
                ID: "Data Pembanding 2",
            },
        ];
        const spesifikasiUmumBangunan = [
            {
                NAME: "Mewah",
                ID: "Mewah",
            },
            {
                NAME: "Menengah",
                ID: "Menengah",
            },
        ];
        const pondasi = [
            {
                NAME: "Pondasi Dangkal Dengan Pondasi ....",
                ID: "Pondasi Dangkal Dengan Pondasi ....",
            },
            {
                NAME: "Pondasi Dangkal Dengan Pondasi ....",
                ID: "Pondasi Dangkal Dengan Pondasi ....",
            },
        ];
        const struktur = [
            {
                NAME: "Struktur Beton Bertulang",
                ID: "Struktur Beton Bertulang",
            },
            {
                NAME: "Baja Ringan",
                ID: "Baja Ringan",
            },
        ];
        const rangkaAtap = [
            {
                NAME: "Rangka Atap Baja Ringan Kelas I",
                ID: "Rangka Atap Baja Ringan Kelas I",
            },
            {
                NAME: "Rangka Atap Baja Ringan Kelas II",
                ID: "Rangka Atap Baja Ringan Kelas II",
            },
        ];
        const penutupAtap = [
            {
                NAME: "Genteng Kodok",
                ID: "Genteng Kodok",
            },
            {
                NAME: "Genteng Katak",
                ID: "Genteng Katak",
            },
        ];
        const plafon = [
            {
                NAME: "Triplek Dengan Rangka Kayu",
                ID: "Triplek Dengan Rangka Kayu",
            },
            {
                NAME: "Triplek Dengan Rangka Baja Ringan",
                ID: "Triplek Dengan Rangka Baja Ringan",
            },
        ];
        const dinding = [
            {
                NAME: "Batu Bata Dilapisi Cat",
                ID: "Batu Bata Dilapisi Cat",
            },
            {
                NAME: "Batu Keramik Dilapisi Cat",
                ID: "Batu Keramik Dilapisi Cat",
            },
        ];
        const pintu = [
            {
                NAME: "Kusen Kayu Kelas I",
                ID: "Kusen Kayu Kelas I",
            },
            {
                NAME: "Kusen Kayu Kelas II",
                ID: "Kusen Kayu Kelas II",
            },
        ];
        const jendela = [
            {
                NAME: "Kusen Kayu Kelas I",
                ID: "Kusen Kayu Kelas I",
            },
            {
                NAME: "Kusen Kayu Kelas II",
                ID: "Kusen Kayu Kelas II",
            },
        ];
        const lantai = [
            {
                NAME: "Keramik Lokal",
                ID: "Keramik Lokal",
            },
            {
                NAME: "Kayu Jati",
                ID: "Kayu Jati",
            },
        ];
        const utilitas = [
            {
                NAME: "Peralatan sanitair, catu daya, ME...",
                ID: "Peralatan sanitair, catu daya, ME...",
            },
            {
                NAME: "TV",
                ID: "TV",
            },
        ];
        const profesionalFee = [
            {
                NAME: "Perantara",
                ID: "Perantara",
            },
            {
                NAME: "Pemasaran",
                ID: "Pemasaran",
            },
        ];
        const biayaPerajin = [
            {
                NAME: "Kayu",
                ID: "Kayu",
            },
            {
                NAME: "Genteng",
                ID: "Genteng",
            },
        ];
        const keuntunganKontraktor = [
            {
                NAME: "Keuntungan Kontraktor 1",
                ID: "Keuntungan Kontraktor 1",
            },
            {
                NAME: "Keuntungan Kontraktor 2",
                ID: "Keuntungan Kontraktor 2",
            },
        ];
        this.setState({
            sumberDataPembanding:sumberDataPembanding,
            spesifikasiUmumBangunan:spesifikasiUmumBangunan,
            pondasi:pondasi,
            struktur:struktur,
            rangkaAtap:rangkaAtap,
            penutupAtap:penutupAtap,
            plafon:plafon,
            dinding:dinding,
            pintu:pintu,
            jendela:jendela,
            lantai:lantai,
            utilitas:utilitas,
            profesionalFee:profesionalFee,
            biayaPerajin:biayaPerajin,
            keuntunganKontraktor:keuntunganKontraktor,
        });
    }
    
    handleClose = () => {
       this.props.setModalForm(false);
    }
    
    onSelect =(idx)=>{
     //   let fields = this.state.fields;
        
        // fields['kodePos'] =  selectedGroup[0];
        // fields['Kecamatan'] =  selectedGroup[1];
        // fields['Kabupaten'] =  selectedGroup[2];
        // fields['Propinsi'] =  selectedGroup[3];
        // fields['Kelurahan'] =  selectedGroup[4];
        // localStorage.setItem("SelectZipcode",JSON.stringify(fields));
       this.props.updateForm(idx);
    }
    setSaranaPelengkap=(select)=>{
        let data = this.state.data;
        data.push(select);
        this.setState({ data });
    }
    render() {
        return (
            <React.Fragment>
                {/* START DIALOG KODE POS */}
                <Dialog fullScreen maxWidth="md" className="client-dialog" open={this.props.stateModalForm} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">FORM</DialogTitle>
                    <DialogContent>
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <h5>OBJEK</h5>
                            </div>
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <div className="form-group">
                                    <TextField id="sumberDataPembanding" select label="Sumber Data Pembanding"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Sumber Data Pembanding"
                                        fullWidth>
                                        {this.state.sumberDataPembanding.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                                <div className="form-group">
                                    <TextField id="spesifikasiUmumBangunan" select label="Spesifikasi Umum Bangunan"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Spesifikasi Umum Bangunan"
                                        fullWidth>
                                        {this.state.spesifikasiUmumBangunan.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                                <div className="form-group">
                                    <TextField id="tinggiLantaiFisik" fullWidth label="Tinggi Lantai Fisik" />
                                </div>
                                <div className="form-group">
                                    <TextField id="tahunBangunan" fullWidth label="Tahun Bangunan/Renovasi" />
                                </div>
                                <div className="form-group">
                                    <TextField id="luasBangunan" fullWidth label="Luas Bangunan" />
                                </div>
                                <div className="form-group">
                                    <TextField id="indexLantai" fullWidth label="IndexLantai" readOnly />
                                </div>
                            </div>
                            
                            
                        </div>
                        <div className="row mt-5">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <h5>KOMPONEN</h5>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <h5>ASUMSI MATERIAL OBJEK</h5>
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <h5>IM</h5>
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <h5>HARGA /m2</h5>
                            </div>
                        </div>
                        <div className="row mt-10">
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <h5>BIAYA LANGSUNG</h5>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Pondasi</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="pondasi" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.pondasi.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imPondasi" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaPondasi" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Struktur</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="struktur" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.struktur.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imStruktur" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaStruktur" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Rangka Atap</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="pondasi" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.rangkaAtap.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imRangkaAtap" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaRangkaAtap" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Penutup Atap</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="penutupAtap" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.penutupAtap.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imPenutupAtap" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaPenutupAtap" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Plafon</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="plafon" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.plafon.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imPlafon" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaPlafon" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Dinding</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="dinding" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.dinding.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imDinding" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaDinding" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Pintu</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="pintu" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.pintu.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imPintu" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaPintu" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Jendela</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="jendela" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.jendela.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imJendela" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaJendela" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Lantai</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="lantai" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.lantai.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imLantai" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaLantai" fullWidth label="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <p className="mb-0">Utilitas</p>
                            </div>
                            <div className="col-sm-4 col-md-4 col-xl-4">
                                <TextField id="utilitas" select label=""
                                    // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                    // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {this.state.utilitas.map(option => (
                                    <MenuItem key={option.NAME} value={option.ID}>
                                        {option.NAME}
                                    </MenuItem>
                                    ))}
                                </TextField>   
                            </div>
                            <div className="col-sm-2 col-md-2 col-xl-2">
                                <TextField id="imUtilitas" fullWidth label="" />
                            </div>
                            <div className="col-sm-3 col-md-3 col-xl-3">
                                <TextField type="number" id="hargaUtilitas" fullWidth label="" />
                            </div>
                        </div>
                        

                        <div className="row mt-10">
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <h5>SARANA PELENGKAP</h5>
                            </div>
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <SaranaPelengkap updateSaranaPelengkap={this.setSaranaPelengkap.bind(this)}  />
                                <div className="form-group">
                                    <TextField id="totalBiayaLangsung" type="number" fullWidth label="Total Biaya Langsung" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Professional Fee</label> 
                                    <TextField className="mr-5" id="profesionalFee" select label=""
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="">
                                        {this.state.profesionalFee.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>  
                                    <TextField className="mr-5" id="persentaseFee" label="" readOnly />
                                    <TextField id="totalFee" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Biaya Perajin</label> 
                                    <TextField className="mr-5" id="biayaPerajin" select label=""
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="">
                                        {this.state.biayaPerajin.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>  
                                    <TextField className="mr-5" id="persentaseBiayaPerajin" label="" readOnly />
                                    <TextField id="totalBiayaPerajin" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Keuntungan Kontraktor</label> 
                                    <TextField className="mr-5" id="keuntunganKontraktor" select label=""
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="">
                                        {this.state.keuntunganKontraktor.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>  
                                    <TextField className="mr-5" id="persentaseKeuntunganKontraktor" label="" readOnly />
                                    <TextField id="totalKeuntunganKontraktor" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="totalBiayaLangsung2" type="number" fullWidth label="Total Biaya Langsung" readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="totalBiayaPembangunanBaru" type="number" fullWidth label="Total Biaya Pembangunan Baru" readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="ppn" type="number" fullWidth label="PPN 10%" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="totalBiayaPembangunanBaruPpn" type="number" fullWidth label="Total Biaya Pembangunan Baru + PPN 10%" readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="umurEkonomisBangunan" type="number" fullWidth label="Umur Ekonomis Bangunan" readOnly />
                                </div>
                                <div className="form-group">
                                    <TextField id="umurEfektifBangunan" type="number" fullWidth label="Umur Efektif Bangunan" readOnly />
                                </div>
                            </div>
                        </div>
                        <div className="row mt-10">
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <h5>PENYUSUTAN</h5>
                            </div>
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                
                                <div className="form-group">
                                    <label className="w-30">Kemunduran Fisik</label> 
                                    <TextField className="mr-5" id="persentaseKemunduranFisik1" label="" readOnly />
                                    <TextField className="mr-5" id="persentaseKemunduranFisik2" label="" readOnly />
                                    <TextField id="totalKemunduranFisik" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Kondisi Terlihat (+/-)</label> 
                                    <TextField className="mr-5" id="persentaseKondisiTerlihat1" label="" readOnly />
                                    <TextField className="mr-5" id="persentaseKondisiTerlihat2" label="" readOnly />
                                    <TextField id="totalKondisiTerlihat" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Total Kemunduran Fisik</label> 
                                    <TextField className="mr-5" id="persentaseTotalKemunduranFisik1" label="" readOnly />
                                    <TextField className="mr-5" id="persentaseTotalKemunduranFisik2" label="" readOnly />
                                    <TextField id="totalKemunduranFisik2" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Keusangan Fungsional</label> 
                                    <TextField className="mr-5" id="persentaseKeusanganFungsional1" label="" readOnly />
                                    <TextField className="mr-5" id="persentaseKeusanganFungsional2" label="" readOnly />
                                    <TextField id="totalKeusanganFungsional" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Keusangan Ekonomis</label> 
                                    <TextField className="mr-5" id="persentaseKeusanganEkonomis1" label="" readOnly />
                                    <TextField className="mr-5" id="persentaseKeusanganEkonomis2" label="" readOnly />
                                    <TextField id="totalKeusanganEkonomis" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Total Penyusutan</label> 
                                    <TextField className="mr-5" id="persentaseTotalPenyusutan1" label="" readOnly />
                                    <TextField className="mr-5" id="persentaseTotalPenyusutan2" label="" readOnly />
                                    <TextField id="totalTotalPenyusutan" label="" type="number" readOnly />
                                </div>
                                <div className="form-group">
                                    <label className="w-30">Nilai PasarBangunan /m2</label> 
                                    <TextField className="mr-5" id="persentaseNilaiPasarBangunan1" label="" readOnly />
                                    <TextField className="mr-5" id="persentaseNilaiPasarBangunan2" label="" readOnly />
                                    <TextField id="totalNilaiPasarBangunan" label="" type="number" readOnly />
                                </div>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <RctCollapsibleCard>
                                    <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SIMPAN DRAFT</MatButton>
                                    <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SIMPAN</MatButton>
                                </RctCollapsibleCard>
                            </div>
                        </div>
                    </DialogContent>
                </Dialog>
                {/* END DIALOG KODE POS */}
                
                
        </React.Fragment>
        );
    }
}

export default FormDataPembandingBangunan
 