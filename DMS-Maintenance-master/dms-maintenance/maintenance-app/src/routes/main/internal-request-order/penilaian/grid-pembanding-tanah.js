import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class GridPembandingTanah extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            errors: {},
            loading: false
        };
    }
      
    componentDidMount() {
        const newdata = [
            ['1', '2020/01/CCR-IA/0001', 'FIAH', '01/03/2014', 'DATA PEMBANDING 1', 0],
            ['2', '2020/02/CCR-IA/0001', 'DIAN', '01/05/2014', 'DATA PEMBANDING 2', 1],
            ['3', '2020/03/CCR-IA/0001', 'EKA', '01/04/2014', 'DATA PEMBANDING 3', 2]
        ];
        this.setState({
            data: newdata
        });
    }
    
    onSelect =(idx)=>{
                
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        const columns = ["No","No Laporan","Nama Debitur","Tanggal Pengajuan","Data Pembanding",
       {
           name: "Action",
           options: {
               filter: false,
               sort: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                   return (
                       <div>
                           <MatButton onClick={() => this.onSelect(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Edit</MatButton>
                           <MatButton onClick={() => this.onSelect(value)} variant="contained" className="mr-10 mb-10 text-white btn-icon btn-danger">Hapus</MatButton>
                       </div>
                   );
               }
           }
       }
   ];
   //  END DT SERTIFIKAT
        return (
            <React.Fragment>                
                <RctCollapsibleCard fullBlock>
                    {this.state.loading &&
                        <RctSectionLoader />
                    }
                    <MUIDataTable
                        title=""
                        data={this.state.data}
                        columns={columns}
                        options={options}
                    />
                </RctCollapsibleCard>
        </React.Fragment>
        );
    }
}

export default GridPembandingTanah
 