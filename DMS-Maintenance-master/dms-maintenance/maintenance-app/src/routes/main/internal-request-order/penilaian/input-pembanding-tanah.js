/**
 * Miscellaneous Widgets Page
 */

import React, { Component, useState } from 'react';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

 //page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

 //rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

 //rct section loader

import GridPembandingTanah from 'Routes/main/internal-request-order/penilaian/grid-pembanding-tanah';
import SearchDataPembandingTanah from 'Routes/main/internal-request-order/penilaian/search-data-pembanding-tanah';
import FormDataPembandingTanah from 'Routes/main/internal-request-order/penilaian/form-data-pembanding-tanah';

function TabContainer({ children }) {
    return (
        <Typography component="div">
            {children}
        </Typography>
    );
}
 
export default class InternalRequestOrderPenilaianInputPembandingTanah extends Component {
    state = {
        activeIndex: 0,
        activeMainIndex: 1,
        activeSubMainIndex: 0,
        openSearch: false,
        openForm: false,
        fields: {},
        errors: {},
        loading: false
    }
    constructor(props, context) {
        super(props, context);
    }
    
    componentDidMount() {
        
    }
    handleChange(event, value) {
        const menu = ["input-pembanding-tanah","input-pembanding-bangunan","data-pembanding"];
        this.props.history.push(`/app/main/internal-request-order/penilaian/${menu[value]}/1`);
        this.setState({ activeIndex: value });
    }
    handleMainChange(event,value){
        const menu = ["new-appraisal/main","penilaian/input-pembanding-tanah"];
        this.props.history.push(`/app/main/internal-request-order/${menu[value]}/1`);
        this.setState({ activeMainIndex: value });
    }
    handleSubMainChange(event,value){
        const menu = ["input-pembanding-tanah","template-tanah","ringkasan-hasil-penilaian","upload-foto"];
        this.props.history.push(`/app/main/internal-request-order/penilaian/${menu[value]}/1`);
        this.setState({ activeSubMainIndex: value });
    }
    openSearch = ()=>{
        this.setState({ openSearch: true });
    }
    setModalSearch = (select) => {
        this.setState({ openSearch: select });
    }
    setSearch=(select)=>{
        console.log(select);
        // let DEBITUR = this.state.DEBITUR;
  
        // DEBITUR.ZIP_CODE_ID=  select[0];
        // DEBITUR.DISTRICT_ID =  select[1];
        // DEBITUR.SUBDISTRICT_ID =  select[2];
        // DEBITUR.PROVINCE_ID =  select[3];
        // DEBITUR.CITY_ID =  select[4];
        // this.setState({ DEBITUR});
        // localStorage.setItem('DEBITUR',JSON.stringify( DEBITUR));
        this.setState({openSearch: false});
    }
    openForm = ()=>{
        this.setState({ openForm: true });
    }
    setModalForm = (select) => {
        this.setState({ openForm: select });
    }
    setForm=(select)=>{
        console.log(select);
        // let DEBITUR = this.state.DEBITUR;
  
        // DEBITUR.ZIP_CODE_ID=  select[0];
        // DEBITUR.DISTRICT_ID =  select[1];
        // DEBITUR.SUBDISTRICT_ID =  select[2];
        // DEBITUR.PROVINCE_ID =  select[3];
        // DEBITUR.CITY_ID =  select[4];
        // this.setState({ DEBITUR});
        // localStorage.setItem('DEBITUR',JSON.stringify( DEBITUR));
        this.setState({openForm: false});
    }
    render() {
        const { activeIndex,
            activeMainIndex,
            activeSubMainIndex,
            openSearch,
            openForm,
            fields,
            errors,
            loading
        } = this.state;
        const extendUrl = "/app/main/internal-request-order";

        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Main" match={this.props.match} />
                <Tabs value={activeMainIndex} onChange={(e, value) => this.handleMainChange(e, value)}>
                    <Tab label="Main" />
                    <Tab label="Penilaian" />
                </Tabs>
                <Tabs value={activeSubMainIndex} onChange={(e, value) => this.handleSubMainChange(e, value)}>
                    <Tab label="Data Pembanding" />
                    <Tab label="Template Tanah" />
                    <Tab label="Ringkasan Hasil Penilaian" />
                    <Tab label="Upload Foto" />
                </Tabs>
                <Tabs value={activeIndex} onChange={(e, value) => this.handleChange(e, value)}>
                    <Tab label="Input Pembanding Tanah" />
                    <Tab label="Input Pembanding Bangunan" />
                    <Tab label="Data Pembanding" />
                </Tabs>
                <TabContainer>
                    <RctCollapsibleCard className="text-danger" heading="PEMBANDING TANAH">
                        <SearchDataPembandingTanah updateSearch={this.setSearch.bind(this)} stateModalSearch={openSearch} setModalSearch={this.setModalSearch.bind(this)}  />
                        <FormDataPembandingTanah updateForm={this.setForm.bind(this)} stateModalForm={openForm} setModalForm={this.setModalForm.bind(this)}  />
                        <MatButton variant="contained" color="primary" onClick={this.openForm} className="mt-3 ml-2 mb-2 text-white">ADD NEW</MatButton>
                        <MatButton variant="contained" color="primary" onClick={this.openSearch} className="mt-3 ml-2 mb-2 text-white">SEARCH</MatButton>
                        <GridPembandingTanah/>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">KEMBALI</MatButton>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SIMPAN</MatButton>
                    </RctCollapsibleCard>
                </TabContainer>
                
            </div>
        );
    }
}