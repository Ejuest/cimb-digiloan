import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import MatButton from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';


class FormDataPembandingTanah extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
        const newdata = [
            ['fototampakdepan.jpg', 'Valuer 1', '5mb', '01/03/2014', 0],
        ];
        this.setState({
            data: newdata
        });
    }
    
    handleClose = () => {
       this.props.setModalForm(false);
    }
    
    onSelect =(idx)=>{
     //   let fields = this.state.fields;
        
        // fields['kodePos'] =  selectedGroup[0];
        // fields['Kecamatan'] =  selectedGroup[1];
        // fields['Kabupaten'] =  selectedGroup[2];
        // fields['Propinsi'] =  selectedGroup[3];
        // fields['Kelurahan'] =  selectedGroup[4];
        // localStorage.setItem("SelectZipcode",JSON.stringify(fields));
       this.props.updateForm(idx);
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        const columns = ["Nama File","Upload By","File Size","Tanggal Upload",
        {
            name: "Action",
            options: {
                filter: false,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <div>
                            <MatButton variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">HAPUS</MatButton>
                        </div>
                    );
                }
            }
        }
        ];

        const sumberDataPembanding = [
            {
                NAME: "Data Pembanding 1",
                ID: "Data Pembanding 1",
            },
            {
                NAME: "Data Pembanding 2",
                ID: "Data Pembanding 2",
            },
        ];
        const kelurahan = [
            {
                NAME: "Kebon Jeruk",
                ID: "Kebon Jeruk",
            },
            {
                NAME: "Meruya Utara",
                ID: "Meruya Utara",
            },
        ];
        const kecmatan = [
            {
                NAME: "Kebon Jeruk",
                ID: "Kebon Jeruk",
            },
            {
                NAME: "Meruya Utara",
                ID: "Meruya Utara",
            },
        ];
        const kota = [
            {
                NAME: "Jakarta Barat",
                ID: "Jakarta Barat",
            },
            {
                NAME: "Jakarta Selatan",
                ID: "Jakarta Selatan",
            },
        ];
        const provinsi = [
            {
                NAME: "DKI Jakarta",
                ID: "DKI Jakarta",
            },
        ];
        const tahun = [
            {
                NAME: "2003",
                ID: "2003",
            },
            {
                NAME: "2004",
                ID: "2004",
            },
        ];
        const tipeBangunan = [
            {
                NAME: "Menengah",
                ID: "Menengah",
            },
            {
                NAME: "Keatas",
                ID: "Keatas",
            },
        ];
        const legalitasKepemilikan = [
            {
                NAME: "SHM",
                ID: "SHM",
            },
            {
                NAME: "AJB",
                ID: "AJB",
            },
        ];
        const sebagai = [
            {
                NAME: "Sebagai",
                ID: "Sebagai",
            },
            {
                NAME: "Saudara",
                ID: "Saudara",
            },
        ];
        const transaksiPenawaran = [
            {
                NAME: "Penawaran",
                ID: "Penawaran",
            },
            {
                NAME: "Deal",
                ID: "Deal",
            },
        ];
        
        return (
            <React.Fragment>
                {/* START DIALOG KODE POS */}
                <Dialog fullScreen maxWidth="md" className="client-dialog" open={this.props.stateModalForm} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">FORM</DialogTitle>
                    <DialogContent>
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-xl-12">
                                <div className="form-group">
                                    <TextField id="sumberDataPembanding" select label="Sumber Data Pembanding"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Sumber Data Pembanding"
                                        fullWidth>
                                        {sumberDataPembanding.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>                                        
                                </div>
                                <div className="form-group">
                                    <TextField id="jarakDenganAktiva" fullWidth label="Jarak Dengan Aktiva" />
                                </div>
                                <div className="form-group">
                                    <TextField id="latitude" fullWidth label="Titik Koordinat Object Latitude" />
                                </div>
                                <div className="form-group">
                                    <TextField id="longitude" fullWidth label="Titik Koordinat Object Longitude" />
                                </div>
                                <div className="form-group">
                                    <TextField id="lokasiObject" fullWidth label="Lokasi Object" />
                                </div>
                                <div className="form-group">
                                    <TextField id="perumahan" fullWidth label="Perumahan" />
                                </div>
                                <div className="form-group">
                                    <TextField id="cluster" fullWidth label="Cluster" />
                                </div>
                                <div className="form-group">
                                    <TextField id="kelurahan" select label="Kelurahan"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Kelurahan"
                                        fullWidth>
                                        {kelurahan.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="kecamatan" select label="Kecamatan"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Kecamatan"
                                        fullWidth>
                                        {kelurahan.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="kota" select label="Kota/Kabupaten"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Kota/Kabupaten"
                                        fullWidth>
                                        {kota.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="provinsi" select label="Provinsi"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Provinsi"
                                        fullWidth>
                                        {provinsi.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="luasTanah" fullWidth label="Luas Tanah(m2)" />
                                </div>
                                <div className="form-group">
                                    <TextField id="totalLuasBangunan" fullWidth label="Total Luas Bangunan(m2)" />
                                </div>
                                <div className="form-group">
                                    <TextField id="tahunDibangun" select label="Tahun Dibangun/Renovasi"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Tahun Dibangun/Renovasi"
                                        fullWidth>
                                        {tahun.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="tipeBangunan" select label="Tipe Bangunan"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Tipe Bangunan"
                                        fullWidth>
                                        {tipeBangunan.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="legalitasKepemilikan" select label="Legalitas Kepemilikan"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Legalitas Kepemilikan"
                                        fullWidth>
                                        {legalitasKepemilikan.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="sumberData" fullWidth label="Sumber Data" />
                                </div>
                                <div className="form-group">
                                    <TextField id="teleponPembanding" fullWidth label="Telepon Pembanding" />
                                </div>
                                <div className="form-group">
                                    <TextField id="sebagai" select label="Sebagai"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Sebagai"
                                        fullWidth>
                                        {sebagai.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="waktuPenawaran" select label="Waktu Penawaran/Transaksi"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Waktu Penawaran/Transaksi"
                                        fullWidth>
                                        {tahun.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="dataHarga" fullWidth label="Data Harga(Rp.)" />
                                </div>
                                <div className="form-group">
                                    <TextField id="transaksiPenawaran" select label="Transaksi/Penawaran"
                                        // onChange={this.APPFLAGChange.bind(this, "FLAG_APPRAISAL_ID")} 
                                        // value={this.state.APPFLAG.FLAG_APPRAISAL_ID || ''} 
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Transaksi/Penawaran"
                                        fullWidth>
                                        {transaksiPenawaran.map(option => (
                                        <MenuItem key={option.NAME} value={option.ID}>
                                            {option.NAME}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="remarks" fullWidth label="Remarks" multiline rows="4" />
                                </div>
                                <div className="form-group">
                                    <input
                                        accept="image/*"
                                        className="visible-false"
                                        id="fotoPembanding"
                                        // multiple
                                        type="file"
                                    />
                                    {/* <label htmlFor="fotoPembanding">
                                        <MatButton variant="contained" color="primary" component="span">Upload</MatButton>
                                    </label> */}
                                    {/* <TextField id="fotoPembanding" label="Foto Pembanding" type="file" /> */}
                                    {/* <MatButton variant="contained" color="primary" className="mt-3 ml-2 text-white">UPLOAD</MatButton> */}
                                </div>
                                <div className="form-group">
                                    <MUIDataTable
                                        title=""
                                        data={this.state.data}
                                        columns={columns}
                                        options={options}
                                    />
                                </div>
                            </div>
                            <RctCollapsibleCard>
                                <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">KEMBALI</MatButton>
                                <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SIMPAN DRAFT</MatButton>
                                <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SIMPAN</MatButton>
                            </RctCollapsibleCard>
                        </div>
                    </DialogContent>
                </Dialog>
                {/* END DIALOG KODE POS */}
                
                
        </React.Fragment>
        );
    }
}

export default FormDataPembandingTanah
 