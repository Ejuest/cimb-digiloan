import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import MenuItem from '@material-ui/core/MenuItem';
 

class SearchZipcode extends React.Component {

    constructor(props) {
        super(props);
    
        this. state = {
            zipdata:[],
            openzipcode: false,
            errors: {},
            loading: false
        };
      }

      
    componentDidMount() {
  
        AparsialApi().get('RFZIPCODE/Get')
        .then(res => {
            if (res.status == 200) {
                
               const datanew = res.data.map((group, index) => [
                   group.ziP_CODE,
                   group.province,
                   group.city,
                   group.district,
                   group.subdistrict,
                   group.ziP_DESC,
                   index,
               ]);
            this.setState({
               zipdata:  datanew,
            });
        } }).catch(err => {
            console.error("error RFZIPCODE", err);
        });
    }
    handleClickopenzipcode = () => {
		this.setState({
			openzipcode: true,
			fields: {},
			errors: {}
		});
	};
    
    handleCloseZipCode = () => {
		this.setState({ openzipcode: false });
		this.setState({
			fields: {},
			errors: {}
		});
    };
    
    onSelectZipcode =(idx)=>{
        const { zipdata } = this.state;
        const selectedGroup = zipdata[idx];
     //   let fields = this.state.fields;
        
        // fields['kodePos'] =  selectedGroup[0];
        // fields['Kecamatan'] =  selectedGroup[1];
        // fields['Kabupaten'] =  selectedGroup[2];
        // fields['Propinsi'] =  selectedGroup[3];
        // fields['Kelurahan'] =  selectedGroup[4];
         this.setState({ openzipcode:false});
        // localStorage.setItem("SelectZipcode",JSON.stringify(fields));
       this.props.updatezip(selectedGroup);
        
    }
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        const columnzipcode = ["ZipCode","Propinsi","Kota","Kecamatan","Kelurahan","Dati II",
       {
           name: "Action",
           options: {
               filter: false,
               sort: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                   return (
                       <div>
                           <MatButton onClick={() => this.onSelectZipcode(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">Select</MatButton>
                       </div>
                   );
               }
           }
       }
   ];
   //  END DT SERTIFIKAT
        return (
            <React.Fragment>
              {/* START DIALOG KODE POS */}
              <Dialog maxWidth="md" className="client-dialog" open={this.state.openzipcode} onClose={this.handleCloseZipCode} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">KODE POS</DialogTitle>
                    <DialogContent>
                        <div>
                        <RctCollapsibleCard fullBlock>
                                        {this.state.loading &&
                                            <RctSectionLoader />
                                        }
                                        <MUIDataTable
                                            title="PILIH LOKASI"
                                            data={this.state.zipdata}
                                            columns={columnzipcode}
                                            options={options}
                                        />
                            </RctCollapsibleCard>
                        </div>
                    </DialogContent>
                </Dialog>
                {/* END DIALOG KODE POS */}

                <MatButton variant="contained" color="primary"  onClick={this.handleClickopenzipcode} className="mt-3 ml-2 text-white">...</MatButton>
        </React.Fragment>
        );
    }
}

export default SearchZipcode
 