/**
 * Miscellaneous Widgets Page
 */
import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';
import { AparsialApi } from 'Api';
import { RctCardContent } from 'Components/RctCard';
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';
import {
    Card,
    CardImg,
    CardText,
    CardBody,
    CardTitle,
    CardSubtitle,
    Button
 } from 'reactstrap';

import MatButton from '@material-ui/core/Button';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

export default class InternalRequestOrderBandingInbox extends Component {
    state = {
		datas: null,
        data: [],
        dataNewAparsial: [],
        newaparsialcount:0,
        reaparsialcount:0,
        banaparsialcount:0,
        penaparsialcount:0,
		selectedUser: null, // selected user to perform operations
		loading: true, // loading activity
    }
    
	componentDidMount() {
		//get list user
		// const token = this.props.token;
        AparsialApi().get('APPLICATION/Inbox')
        .then(res => {
            if (res.status == 200) {
               
                // public virtual string type { get; set; }
                // public virtual string noapplikasi { get; set; }
                // public virtual DateTime? tglterima { get; set; }
                // public virtual string namadebitur { get; set; }
                // public virtual string alamat { get; set; }
                // public virtual string typejaminan { get; set; }
                // public virtual string namajaminan { get; set; }
                // public virtual DateTime? trackdate { get; set; }
                // public virtual string aging
                const listnew = res.data.filter(l => {
                    return l.type.toLowerCase().match('new-app');
                  });
                  const listre = res.data.filter(l => {
                    return l.type.toLowerCase().match('re-app');
                  });
                  const listban = res.data.filter(l => {
                    return l.type.toLowerCase().match('banding');
                  });
                  const listpen = res.data.filter(l => {
                    return l.type.toLowerCase().match('pending');
                  });


				const datanew = listban.map((group, index) => [
					group.noapplikasi,
                    convertDateASP(group.tglterima, "DD-MM-YYYY"),
                    group.namadebitur,
                    group.namajaminan,
					group.alamat,
					convertDateASP(group.trackdate, "DD-MM-YYYY"),
					group.aging  
                ]);
                
                this.setState({
                    dataNewAparsial: datanew,
                    newaparsialcount:datanew.length,
                    reaparsialcount:listre.length,
                    banaparsialcount:listban.length,
                    penaparsialcount:listpen.length,
                    loading: false,
				});
            }
            else
            {

            }

        });
      
	}
    onClickDelete = (idx) => {
		// const {groups} = this.state;
		// this.refs.deleteConfirmationDialog.open();
		// this.setState({ selectedGroup: groups[idx] });
    }
    deleteUser = () => {
		// const { selectedGroup } = this.state;
		// const { name, tempId} = selectedGroup;
		// this.refs.deleteConfirmationDialog.close();
		// this.setState({ loading: true });
		// const reqData = {
		// 	tempId: tempId
		// };
		// const token = this.props.token;
		// getApi(token).post('CIMB-Appraisal-Security/api/json/reply/deleteGroupPending', reqData)
		// 	.then(res => {
		// 		// console.log("res:", res.data);
		// 		NotificationManager.success(`Group ${name} Deleted.`, "Success", getNotifTimeout());
		// 		//RELOAD
		// 		this.componentDidMount();
		// 	}).catch(err => {
		// 		console.error("error deleteGroupPending", err);
		// 		NotificationManager.error(`Delete Group ${name} Failed.`, "Error", getNotifTimeout());
		// 		this.setState({ loading: false });
		// 	});
	}
    render() {
  
        const { data
            , loading
            , selectedUser
            ,dataNewAparsial
            ,newaparsialcount
            ,reaparsialcount 
            ,banaparsialcount
        ,penaparsialcount
        } = this.state;
        const extendUrl = "/app/main/internal-request-order";
		const columns = ["No Aplikasi", "Tgl Terima", "Nama Debitur", "Jenis Jaminan", "Alamat Jaminan", "Track Date", "Aging",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <Link to={`${extendUrl}/banding/main/1`}>
                                    <MatButton variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-edit"></i> Detail</MatButton>
                                </Link>
								{/* <MatButton onClick={() => this.onClickDelete(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-delete"></i> Delete</MatButton> */}
							</div>
						);
					}
				}
			}
		];
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Request Order Inbox" match={this.props.match} />
                <div className="row">
                    <div className="col-md-3">
                        <div className="current-widget bg-dark">
                            <RctCardContent>
                                <div className="d-flex justify-content-between">
                                    <div className="align-items-start">
                                        <h3 className="mb-10">
                                            <Link className="text-white" to={`${extendUrl}/new-appraisal/inbox`}>
                                                New Appraisal
                                            </Link>
                                        </h3>
                                        <h2 className="mb-0">
                                            <Link className="text-white" to={`${extendUrl}/new-appraisal/inbox`}>
                                            {newaparsialcount}
                                            </Link>
                                        </h2>
                                    </div>
                                    <div className="align-items-end">
                                        <Link className="text-white" to={`${extendUrl}/new-appraisal/inbox`}>
                                            <i className="icon-user-follow"></i>
                                        </Link>
                                    </div>
                                </div>
                            </RctCardContent>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="current-widget bg-info">
                            <RctCardContent>
                                <div className="d-flex justify-content-between">
                                    <div className="align-items-start">
                                        <h3 className="mb-10">
                                            <Link className="text-white" to={`${extendUrl}/re-appraisal/inbox`}>
                                                Re Appraisal
                                            </Link>
                                        </h3>
                                        <h2 className="mb-0">
                                            <Link className="text-white" to={`${extendUrl}/re-appraisal/inbox`}>
                                                {reaparsialcount}
                  
                                            </Link>
                                        </h2>
                                    </div>
                                    <div className="align-items-end">
                                        <Link className="text-white" to={`${extendUrl}/re-appraisal/inbox`}>
                                            <i className="icon-action-undo"></i>
                                        </Link>
                                    </div>
                                </div>
                            </RctCardContent>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="current-widget bg-warning">
                            <RctCardContent>
                                <div className="d-flex justify-content-between">
                                    <div className="align-items-start">
                                        <h3 className="mb-10">
                                            <Link className="text-white" to={`${extendUrl}/banding/inbox`}>
                                                Banding
                                            </Link>
                                        </h3>
                                        <h2 className="mb-0">
                                            <Link className="text-white" to={`${extendUrl}/banding/inbox`}>
                                            {banaparsialcount}
                   
                                            </Link>
                                        </h2>
                                    </div>
                                    <div className="align-items-end">
                                        <Link className="text-white" to={`${extendUrl}/banding/inbox`}>
                                            <i className="icon-shield"></i>
                                        </Link>
                                    </div>
                                </div>
                            </RctCardContent>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="current-widget bg-danger">
                            <RctCardContent>
                                <div className="d-flex justify-content-between">
                                    <div className="align-items-start">
                                        <h3 className="mb-10">
                                            <Link className="text-white" to={`${extendUrl}/pending/inbox`}>
                                                Pending
                                            </Link>
                                        </h3>
                                        <h2 className="mb-0">
                                            <Link className="text-white" to={`${extendUrl}/pending/inbox`}>
                                            {penaparsialcount}
                                            </Link>
                                        </h2>
                                    </div>
                                    <div className="align-items-end">
                                        <Link className="text-white" to={`${extendUrl}/pending/inbox`}>
                                            <i className="icon-hourglass"></i>
                                        </Link>
                                    </div>
                                </div>
                            </RctCardContent>
                            
                        </div>
                    </div>
                </div>
                
                <div className="row mt-5">
                    <div className="col-md-12">
                        <RctCollapsibleCard fullBlock>
                            {loading &&
                                <RctSectionLoader />
                            }
                            <MUIDataTable
                                title={"Banding"}
                                data={dataNewAparsial}
                                columns={columns}
                                options={options}
                            />
                        </RctCollapsibleCard>
                        <DeleteConfirmationDialog
                            ref="deleteConfirmationDialog"
                            title={`Are you sure want to delete ${selectedUser ? selectedUser.name : ""}?`}
                            onConfirm={() => this.deleteUser()}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

