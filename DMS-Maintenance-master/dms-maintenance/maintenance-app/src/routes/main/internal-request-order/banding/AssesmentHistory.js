import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import MenuItem from '@material-ui/core/MenuItem';

 
class AssesmentHistory extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
            loading: false
        };
      }

      
    componentDidMount() {
        AparsialApi().get('ASSESSMENT_HISTORY/Get')
        .then(res => {
            if (res.status == 200) {
               
            this.setState({
                dokType: res.data,
            });
           
        }
        }).catch(err => {
            console.error("error RF_CERTIFICATE_TYPE", err);
        });
    }
 
 
    
    render() {
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        
        const coldataassygn=["Tgl Assignment", "Valuer","Assign Oleh"];
        
   //  END DT SERTIFIKAT
        return (
            <React.Fragment>
             <RctCollapsibleCard fullBlock>
               {this.state.loading &&
                  <RctSectionLoader />
                    }
                   <MUIDataTable
                    title=""
                    data={this.props.dataassygn}
                    columns={coldataassygn}
                    options={options}
                    />
            </RctCollapsibleCard>
            </React.Fragment>
        );
    }
}

 export default AssesmentHistory