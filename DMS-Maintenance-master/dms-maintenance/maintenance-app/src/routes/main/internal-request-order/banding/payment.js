/**
 * Miscellaneous Widgets Page
 */

import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
 
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';

 //page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

 //rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

 //rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

function TabContainer({ children }) {
    return (
        <Typography component="div">
            {children}
        </Typography>
    );
}
export default class InternalRequestOrderBandingPayment extends Component {
    state = {
        activeIndex: 2,
        selectedDate: moment(),
        tipePembayaran:[],
        bankPengirim:[],
        nomorRekening:[],
        reason:[],
        openSearch: false,
        sumberPembayaran: [],
        fields: {},
        errors: {},
        loading: false
    }
    componentDidMount() {
        AparsialApi().get('RF_TIPE_PEMBAYARAN/Get')
        .then(resbranch => {
            if (resbranch.status == 200) {
                
            this.setState({
                tipePembayaran:  resbranch.data,
            });
        }
        }).catch(err => {
            console.error("error RF_TIPE_PEMBAYARAN", err);
        });

        AparsialApi().get('RF_REASON/Get')
        .then(resbranch => {
            if (resbranch.status == 200) {
                
            this.setState({
                reason:  resbranch.data,
            });
        }
        }).catch(err => {
            console.error("error RF_REASON", err);
        });

        AparsialApi().get('RF_NOMOR_REKENING/Get')
        .then(resbranch => {
            if (resbranch.status == 200) {
                
            this.setState({
                nomorRekening:  resbranch.data,
            });
        }
        }).catch(err => {
            console.error("error RF_NOMOR_REKENING", err);
        });

        AparsialApi().get('RF_BANK_PENGIRIM/Get')
        .then(resbranch => {
            if (resbranch.status == 200) {
                
            this.setState({
                bankPengirim:  resbranch.data,
            });
        }
        }).catch(err => {
            console.error("error RF_BANK_PENGIRIM", err);
        });
        
        AparsialApi().get('RF_SUMBER_PEMBAYARAN/Get')
        .then(resbranch => {
            if (resbranch.status == 200) {
                
            this.setState({
                sumberPembayaran:  resbranch.data,
            });
        }
        }).catch(err => {
            console.error("error RF_BANK_PENGIRIM", err);
        });
        
    }
    handleChange(event, value) {
        const menu = ["main","document-checking","payment"];
        this.props.history.push(`/app/main/internal-request-order/banding/${menu[value]}/1`);
        this.setState({ activeIndex: value });
    }
    handleClickOpenSearch = () => {
		this.setState({
			openSearch: true,
			fields: {},
			errors: {}
		});
	};
    handleCloseSearch = () => {
        this.setState({ 
            openSearch: false,
            fields: {},
            errors: {} 
        });
    };
    handleDateChange = (date) => {
		this.setState({ selectedDate: date });
	};
    render() {
        const { activeIndex,
            selectedDate,
            tipePembayaran,
            bankPengirim,
            nomorRekening,
            reason,
            openSearch,
            sumberPembayaran,
            fields,
            errors,
            loading
        } = this.state;
        const extendUrl = "/app/main/internal-request-order";
        const columnsPayment = [
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="btn-danger mr-10 mb-10 text-white btn-icon">Delete</MatButton>
							</div>
						);
					}
				}
            },
            "Remarks 1", "Remarks 2", "Remarks 3", "Tanggal Transfer","No Referensi","Nominal Customer","Nominal Bank","Nominal Digunakan","Sumber Pembayaran"
        ];
        const dataPayment = [
            ["CIMB NIAGA", "Lala","009312111","01/01/2020","00231231","500.000","0","500.000",""],
            ["BCA", "Lili","009312111","01/02/2020","123333","100.000","0","100.000",""]
        ];
        
        const columnsDedup = ["No Referensi", "No Aplikasi", "Nama Debitur", "Tanggal Order","Status Aplikasi","Available Balance"];
        const dataDedup = [
            ["", "","","","",""],
            ["", "","","","",""],
        ];

        const columnsSearch = [
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <FormControlLabel control={
                                    <Checkbox color="primary" value="1" />
                                } label=""
                                />
							</div>
						);
					}
				}
            },
            "Remarks 1", "Remarks 2", "Remarks 3", "Tanggal Transfer","No Referensi","Nominal Customer","Nominal Bank",
            {
                name: "Nominal Digunakan",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <TextField fullWidth label="" value={value} />
							</div>
						);
					}
				}
            },
            {
                name: "Sumber Pembayaran",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <TextField select label=""
                                    value=""
                                    SelectProps={{
                                        MenuProps: {
                                        },
                                    }}
                                    helperText=""
                                    fullWidth>
                                    {sumberPembayaran.map(option => (
                                    <MenuItem key={option.id} value={option.name}>
                                        {option.name}
                                    </MenuItem>
                                    ))}
                                </TextField>
							</div>
						);
					}
				}
            }
        ];
        const dataSearch = [
            ["CIMB NIAGA", "Lala","009312111","01/01/2020","00231231","500.000","0","500.000",""]
        ];

        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Payment" match={this.props.match} />
                {/* START DIALOG SEARCH PAYMENT */}
                <Dialog maxWidth="xl" className="client-dialog" open={this.state.openSearch} onClose={this.handleCloseSearch} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">SEARCH PAYMENT</DialogTitle>
                    <DialogContent>
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField id="nomorRekening" select label="Nomor Rekening"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Nomor Rekening"
                                        fullWidth>
                                        {nomorRekening.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <DatePicker
                                        label="Tanggal Transfer"
                                        id="tanggalTransfer"
                                        format="DD/MM/YYYY"
                                        value={selectedDate}
                                        onChange={this.handleDateChange}
                                        animateYearScrolling={false}
                                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                        fullWidth
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="nomorReferensi" fullWidth label="No Referensi" value="" />
                                </div>
                                <div className="form-group">
                                    <TextField id="bankPengirim" select label="Bank Pengirim"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Bank Pengirim"
                                        fullWidth>
                                        {bankPengirim.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            </div>
                        </div>
                        <RctCollapsibleCard fullBlock>
                            {loading &&
                                <RctSectionLoader />
                            }
                            <MUIDataTable
                                title="DETAIL PENCARIAN PAYMENT"
                                data={dataSearch}
                                columns={columnsSearch}
                                options={options}
                            />
                        </RctCollapsibleCard>
                        <MatButton variant="contained" className="btn-danger mb-10 text-white">SELECT</MatButton>
                    </DialogContent>
                </Dialog>
                {/* END DIALOG SEARCH PAYMENT */}
                <Tabs value={activeIndex} onChange={(e, value) => this.handleChange(e, value)}>
                    <Tab label="Main" />
                    <Tab label="Document Checking" />
                    <Tab label="Payment" />
                </Tabs>
                <TabContainer>
                    <RctCollapsibleCard className="text-danger" heading="APPLICATION DATA">
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField id="tipePembayaran" select label="Tipe Pembayaran"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Tipe Pembayaran"
                                        fullWidth>
                                        {tipePembayaran.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="totalNominal" fullWidth label="Total Nominal"
                                        type="number"
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-sm-12 col-md-12 col-xl-6">
                                <div className="form-group">
                                    <TextField id="nominalBank" fullWidth label="Nominal Bank"
                                        type="number"
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="nominalCustomer" fullWidth label="Nominal Customer"
                                        type="number"
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="text-left">
                                    <MatButton variant="contained" className="text-white btn-icon btn-danger" onClick={this.handleClickOpenSearch}>Search</MatButton>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-20">
                            <div className="col-md-12">
                                <RctCollapsibleCard fullBlock>
                                        {loading &&
                                            <RctSectionLoader />
                                        }
                                        <MUIDataTable
                                            title="DETAIL PAYMENT"
                                            data={dataPayment}
                                            columns={columnsPayment}
                                            options={options}
                                        />
                                </RctCollapsibleCard>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <TextField id="total" fullWidth label="Total"
                                        type="number"
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField id="selisih" fullWidth label="Selisih"
                                        type="number"
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="text-left">
                                    <MatButton variant="contained" className="text-white mr-10 btn-icon btn-danger">Save</MatButton>
                                    <MatButton variant="contained" className="text-white btn-icon btn-danger">Dedup Payment</MatButton>
                                </div>
                            </div>
                        </div>
                    </RctCollapsibleCard>
                    <RctCollapsibleCard className="text-danger" heading="DEDUP PAYMENT RESULT">
                        <RctCollapsibleCard fullBlock>
                                {loading &&
                                    <RctSectionLoader />
                                }
                                <MUIDataTable
                                    title=""
                                    data={dataDedup}
                                    columns={columnsDedup}
                                    options={options}
                                />
                        </RctCollapsibleCard>
                        <div className="row">
                            <div className="col-md-4">
                                <div className="form-group">
                                    <TextField id="Reason" select label="Reason"
                                        value=""
                                        SelectProps={{
                                            MenuProps: {
                                            },
                                        }}
                                        helperText="Reason"
                                        fullWidth>
                                        {reason.map(option => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <TextField id="remarks" fullWidth label="Remarks" multiline rows="4" defaultValue="" />
                                </div>
                            </div>
                        </div>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SEND BACK</MatButton>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SAVE</MatButton>
                    </RctCollapsibleCard>
                </TabContainer>
            </div>
        );
    }

}