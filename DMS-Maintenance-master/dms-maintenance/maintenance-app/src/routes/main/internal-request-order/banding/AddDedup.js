import React, { Component, useState } from 'react';
import MUIDataTable from "mui-datatables";
import { AparsialApi  } from 'Api';
import { DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MatButton from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import MenuItem from '@material-ui/core/MenuItem';
//Actions
import { onAdd, onDelete,onUpdate } from "Actions/AppraisalAction";

export default class AddDedup extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
            reason:[],
            fieldsDedup:{},
            errors: {},
            loading: false
        };
      }
    componentDidMount() {
        AparsialApi().get('RF_REASON/Get')
         .then(resbranch => {
             if (resbranch.status == 200) {
                 
             this.setState({
                 reason:  resbranch.data,
             });
         }
         }).catch(err => {
             console.error("error RF_REASON", err);
         });
    }
    handletextChangeDedup(field, e) {
		let fieldsDedup = this.state.fieldsDedup;
        fieldsDedup[field] = e.target.value;
		this.setState({ fieldsDedup });
    }
    render() {
        console.log(this.props);
        const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
        };
        
        // START DT DEDUP
        const dataDedup = [
            ["Masih in progress", "Exact Match", "Lala", "012345", "Create LPJ", "CIMB Pusat", "False"],
            ["Match Cancel", "Exact Match", "Lili", "012345","Appraisal Support","CIMB Pusat","False"],
			 
        ];
        const columnsDedup = ["Kriteria", "Match Level", "Nama", "No Referensi", "Status", "Cabang", "Linked",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon">View</MatButton>
                                <MatButton onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="btn-danger mr-10 mb-10 text-white btn-icon">Confirm</MatButton>
							</div>
						);
					}
				}
			}
        ];
       //  END DT DEDUP
        return (
            <div>
                <RctCollapsibleCard className="text-danger" heading="DEDUP RESULT">
                    <RctCollapsibleCard fullBlock>
                    { this.state.loading &&
                        <RctSectionLoader />
                    }
                            <MUIDataTable
                                title=""
                                data={dataDedup}
                                columns={columnsDedup}
                                options={options}
                            />
                        </RctCollapsibleCard>
                        <div className="form-group">
                            <TextField id="Reason" select label="Reason"
                                value={this.state.fieldsDedup["reason"]  || ''}
                                onChange={this.handletextChangeDedup.bind( this,'reason')}
                                SelectProps={{
                                    MenuProps: {
                                    },
                                }}
                                helperText="Reason"
                                fullWidth>
                                {this.state.reason.map(option => (
                                <MenuItem key={option.id} value={option.name}>
                                    {option.name}
                                </MenuItem>
                                ))}
                            </TextField>
                        </div>
                        <div className="form-group">
                            <TextField id="remarks" fullWidth label="Remarks" multiline rows="4" value={this.state.fieldsDedup["remarks"] || ""} onChange={this.handletextChangeDedup.bind( this,'remarks')} />
                        </div>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">SEND BACK</MatButton>
                        <MatButton variant="contained" className="btn-danger mr-10 mb-10 text-white">PROCEED</MatButton>
                </RctCollapsibleCard>
            </div>
        );
    }
}
