/**
 * Pages Routes
 */
import React from 'react';
import { Helmet } from "react-helmet";
import { Redirect, Route, Switch } from 'react-router-dom';

// app config
import AppConfig from 'Constants/AppConfig';

// async components
import {
	AsyncGeneralWidgetsComponent,
	AsyncUserManagementMakerComponent,
	AsyncUserManagementMakerListUserComponent,
	AsyncUserManagementMakerAddUserComponent,
	AsyncUserManagementMakerPendingApprovalUserComponent,
	AsyncUserManagementApprovalComponent,
	AsyncUserManagementApprovalUserComponent,
	AsyncUserManagementMakerWeatherComponent,
	AsyncGroupManagementMakerComponent,
	AsyncGroupManagementMakerListGroupComponent,
	AsyncGroupManagementMakerAddGroupComponent,
	AsyncGroupManagementMakerPendingApprovalGroupComponent,
	AsyncGroupManagementMakerPendingModuleGroupComponent,
	AsyncGroupManagementApprovalComponent,
	AsyncGroupManagementApprovalGroupComponent,
	AsyncParameterSystemMakerComponent,
	AsyncParameterSystemMakerExistingParamComponent,
	AsyncParameterSystemMakerListParamTableComponent,
	AsyncParameterSystemMakerAddParamComponent,
	AsyncParameterSystemMakerAddParamFormDynamicComponent,
	AsyncParameterSystemMakerPendingApprovalComponent,
	AsyncParameterSystemMakerApprovalComponent,
	AsyncParameterDedupMakerComponent,
	AsyncParameterDedupMakerListParamComponent,
	AsyncParameterDedupMakerListPendingApprovalComponent,
	AsyncParameterDedupMakerListApprovalComponent,
	AsyncParameterDedupMakerExistingParameterComponent,
	AsyncParameterDedupMakerAddEnumDupCategoryComponent,
	AsyncParameterDedupMakerAddEnumDupOperatorComponent,
	AsyncParameterDedupMakerAddEnumDupTypeComponent,
	AsyncParameterDedupMakerAddMasterDupCleansingComponent,
	AsyncParameterDedupMakerAddMasterDupCriteriaComponent,
	AsyncParameterDedupMakerAddMasterDupCriteriaDetailComponent,
	AsyncParameterDedupMakerPendingApprovalComponent,
	AsyncParameterDedupApprovalComponent,
	AsyncInternalRequestOrderNewAppraisalInboxComponent,
	AsyncInternalRequestOrderNewAppraisalMainComponent,
	AsyncInternalRequestOrderNewAppraisalDocumentCheckingComponent,
	AsyncInternalRequestOrderNewAppraisalPaymentComponent,
	AsyncInternalRequestOrderReAppraisalInboxComponent,
	AsyncInternalRequestOrderReAppraisalMainComponent,
	AsyncInternalRequestOrderReAppraisalReAppraisalComponent,
	AsyncInternalRequestOrderReAppraisalDocumentCheckingComponent,
	AsyncInternalRequestOrderReAppraisalPaymentComponent,
	AsyncInternalRequestOrderBandingInboxComponent,
	AsyncInternalRequestOrderBandingMainComponent,
	AsyncInternalRequestOrderBandingDocumentCheckingComponent,
	AsyncInternalRequestOrderBandingPaymentComponent,
	AsyncInternalRequestOrderPendingInboxComponent,
	AsyncInternalRequestOrderPendingMainComponent,
	AsyncInternalRequestOrderPendingDocumentCheckingComponent,
	AsyncInternalRequestOrderPendingPaymentComponent,

	AsyncInternalRequestOrderInputPembandingTanahComponent,
	AsyncInternalRequestOrderInputPembandingBangunanComponent,
	AsyncInternalRequestOrderDataPembandingComponent
} from 'Components/AsyncComponent/AsyncComponent';


const userMakerUrl = "user-management-maker";
const userApprovalUrl = "user-management-approval";
const groupMakerUrl = "group-management-maker";
const groupApprovalUrl = "group-management-approval";
const parameterSystemMakerUrl = "parameter-system-maker";
const parameterDedupMakerUrl = "parameter-dedup-maker";
const parameterDedupApprovalUrl = "parameter-dedup-approval";
const internalRequestOrderUrl = "internal-request-order";

//---------aparsial
const requestorderUrl = "requestorder";

const Pages = ({ match }) => (
	<div className="content-wrapper">
		<Helmet>
			<title>{AppConfig.brandName}</title>
			<meta name="description" content={AppConfig.brandName} />
		</Helmet>
		<Switch>
			<Redirect exact from={`${match.url}/`} to={`${match.url}/general`} />
			<Route path={`${match.url}/general`} component={AsyncGeneralWidgetsComponent} />
			{/* USER MAKER */}
			<Route exact path={`${match.url}/${userMakerUrl}`} component={AsyncUserManagementMakerComponent} />
			<Route exact path={`${match.url}/${userMakerUrl}/weather`} component={AsyncUserManagementMakerWeatherComponent} />
			<Route exact path={`${match.url}/${userMakerUrl}/list-user`} component={AsyncUserManagementMakerListUserComponent} />
			<Route exact path={`${match.url}/${userMakerUrl}/list-user/:userId`} component={AsyncUserManagementMakerAddUserComponent} />
			<Route exact path={`${match.url}/${userMakerUrl}/add-user`} component={AsyncUserManagementMakerAddUserComponent} />
			<Route exact path={`${match.url}/${userMakerUrl}/pending-approval-user`} component={AsyncUserManagementMakerPendingApprovalUserComponent} />
			{/* USER APPROVAL */}
			<Route exact path={`${match.url}/${userApprovalUrl}`} component={AsyncUserManagementApprovalComponent} />
			<Route exact path={`${match.url}/${userApprovalUrl}/approval-user`} component={AsyncUserManagementApprovalUserComponent} />
			{/* GROUP MAKER */}
			<Route exact path={`${match.url}/${groupMakerUrl}`} component={AsyncGroupManagementMakerComponent} />
			<Route exact path={`${match.url}/${groupMakerUrl}/list-group`} component={AsyncGroupManagementMakerListGroupComponent} />
			<Route exact path={`${match.url}/${groupMakerUrl}/list-group/:groupId`} component={AsyncGroupManagementMakerAddGroupComponent} />
			<Route exact path={`${match.url}/${groupMakerUrl}/add-group`} component={AsyncGroupManagementMakerAddGroupComponent} />
			<Route exact path={`${match.url}/${groupMakerUrl}/pending-approval-group`} component={AsyncGroupManagementMakerPendingApprovalGroupComponent} />
			<Route exact path={`${match.url}/${groupMakerUrl}/pending-module-group`} component={AsyncGroupManagementMakerPendingModuleGroupComponent} />
			{/* GROUP APPROVAL */}
			<Route exact path={`${match.url}/${groupApprovalUrl}`} component={AsyncGroupManagementApprovalComponent} />
			<Route exact path={`${match.url}/${groupApprovalUrl}/approval-group`} component={AsyncGroupManagementApprovalGroupComponent} />
			{/* PARAMETER SYSTEM */}
			<Route exact path={`${match.url}/${parameterSystemMakerUrl}`} component={AsyncParameterSystemMakerComponent} />
			<Route exact path={`${match.url}/${parameterSystemMakerUrl}/existing-parameter`} component={AsyncParameterSystemMakerListParamTableComponent} />
			<Route exact path={`${match.url}/${parameterSystemMakerUrl}/existing-parameter/:tableName`} component={AsyncParameterSystemMakerExistingParamComponent} />
			<Route exact path={`${match.url}/${parameterSystemMakerUrl}/existing-parameter/:tableName/add-data`} component={AsyncParameterSystemMakerAddParamFormDynamicComponent} />
			<Route exact path={`${match.url}/${parameterSystemMakerUrl}/add-data`} component={AsyncParameterSystemMakerAddParamComponent} />
			<Route exact path={`${match.url}/${parameterSystemMakerUrl}/pending-approval`} component={AsyncParameterSystemMakerPendingApprovalComponent} />
			<Route exact path={`${match.url}/${parameterSystemMakerUrl}/approval`} component={AsyncParameterSystemMakerApprovalComponent} />
			{/* PARAMETER DEDUP */}
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}`} component={AsyncParameterDedupMakerComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter`} component={AsyncParameterDedupMakerListParamComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/:tableName`} component={AsyncParameterDedupMakerExistingParameterComponent} />
			
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/EnumDupCategory/add-data`} component={AsyncParameterDedupMakerAddEnumDupCategoryComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/EnumDupCategory/:id`} component={AsyncParameterDedupMakerAddEnumDupCategoryComponent} />
			
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/EnumDupOperator/add-data`} component={AsyncParameterDedupMakerAddEnumDupOperatorComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/EnumDupOperator/:id`} component={AsyncParameterDedupMakerAddEnumDupOperatorComponent} />
			
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/EnumDupType/add-data`} component={AsyncParameterDedupMakerAddEnumDupTypeComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/EnumDupType/:id`} component={AsyncParameterDedupMakerAddEnumDupTypeComponent} />
			
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/MasterDupCleansing/add-data`} component={AsyncParameterDedupMakerAddMasterDupCleansingComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/MasterDupCleansing/:id`} component={AsyncParameterDedupMakerAddMasterDupCleansingComponent} />
			
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/MasterDupCriteria/add-data`} component={AsyncParameterDedupMakerAddMasterDupCriteriaComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/MasterDupCriteria/:category/:code`} component={AsyncParameterDedupMakerAddMasterDupCriteriaComponent} />
			
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/MasterDupCriteriaDetail/add-data`} component={AsyncParameterDedupMakerAddMasterDupCriteriaDetailComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/existing-parameter/MasterDupCriteriaDetail/:criteriaId/:criteriaCode/:criteriaSequence`} component={AsyncParameterDedupMakerAddMasterDupCriteriaDetailComponent} />
			
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/pending-approval`} component={AsyncParameterDedupMakerListPendingApprovalComponent} />
			<Route exact path={`${match.url}/${parameterDedupMakerUrl}/pending-approval/:tableName`} component={AsyncParameterDedupMakerPendingApprovalComponent} />
			<Route exact path={`${match.url}/${parameterDedupApprovalUrl}/approval`} component={AsyncParameterDedupMakerListApprovalComponent} />
			<Route exact path={`${match.url}/${parameterDedupApprovalUrl}/approval/:tableName`} component={AsyncParameterDedupApprovalComponent} />

			{/* INTERNAL REQUEST ORDER */}
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/new-appraisal/inbox`} component={AsyncInternalRequestOrderNewAppraisalInboxComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/new-appraisal/main/:id`} component={AsyncInternalRequestOrderNewAppraisalMainComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/new-appraisal/document-checking/:id`} component={AsyncInternalRequestOrderNewAppraisalDocumentCheckingComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/new-appraisal/payment/:id`} component={AsyncInternalRequestOrderNewAppraisalPaymentComponent} />

			<Route exact path={`${match.url}/${internalRequestOrderUrl}/re-appraisal/inbox`} component={AsyncInternalRequestOrderReAppraisalInboxComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/re-appraisal/main/:id`} component={AsyncInternalRequestOrderReAppraisalMainComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/re-appraisal/re-appraisal/:id`} component={AsyncInternalRequestOrderReAppraisalReAppraisalComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/re-appraisal/document-checking/:id`} component={AsyncInternalRequestOrderReAppraisalDocumentCheckingComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/re-appraisal/payment/:id`} component={AsyncInternalRequestOrderReAppraisalPaymentComponent} />

			<Route exact path={`${match.url}/${internalRequestOrderUrl}/banding/inbox`} component={AsyncInternalRequestOrderBandingInboxComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/banding/main/:id`} component={AsyncInternalRequestOrderBandingMainComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/banding/document-checking/:id`} component={AsyncInternalRequestOrderBandingDocumentCheckingComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/banding/payment/:id`} component={AsyncInternalRequestOrderBandingPaymentComponent} />

			<Route exact path={`${match.url}/${internalRequestOrderUrl}/pending/inbox`} component={AsyncInternalRequestOrderPendingInboxComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/pending/main/:id`} component={AsyncInternalRequestOrderPendingMainComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/pending/document-checking/:id`} component={AsyncInternalRequestOrderPendingDocumentCheckingComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/pending/payment/:id`} component={AsyncInternalRequestOrderPendingPaymentComponent} />

			<Route exact path={`${match.url}/${internalRequestOrderUrl}/penilaian/input-pembanding-tanah/:id`} component={AsyncInternalRequestOrderInputPembandingTanahComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/penilaian/input-pembanding-bangunan/:id`} component={AsyncInternalRequestOrderInputPembandingBangunanComponent} />
			<Route exact path={`${match.url}/${internalRequestOrderUrl}/penilaian/data-pembanding/:id`} component={AsyncInternalRequestOrderDataPembandingComponent} />
						
		</Switch>
	</div>
);

export default Pages;
