/**
 * Group Management Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP, getNotifTimeOut } from 'Helpers/helpers';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class PendingApprovalGroup extends Component {

	state = {
		groups: null,
		data: [],
		selectedGroup: null,
		loading: true, // loading activity
	}

	componentDidMount = () => {
		const token = this.props.token;
		const reqData = {
			userId: this.props.user.userId
		}
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetGroupPending', reqData)
			.then(res => {
				let groups = res.data.groups;
				// console.log("Groups:", groups);
				const data = groups.map((group, index) => [
					group.id,
					group.name,
					group.isMaker ? "Yes" : "No",
					group.isApprv ? "Yes" : "No",
					group.active ? "Yes" : "No",
					group.createBy ? group.createBy : "-",
					convertDateASP(group.createDate, "DD-MM-YYYY"),
					group.updateBy ? group.updateBy : "-",
					convertDateASP(group.updateDate, "DD-MM-YYYY"),
					group.status,
					index
				]);
				this.setState({
					groups: groups,
					data: data,
					loading: false
				});
			}).catch(err => {
				console.error("error GetGroupPending", err);
				this.setState({ loading: false });
			});
	}

	onClickDelete = (idx) => {
		const {groups} = this.state;
		this.refs.deleteConfirmationDialog.open();
		this.setState({ selectedGroup: groups[idx] });
	}

	deleteGroup = () => {
		const { selectedGroup } = this.state;
		const { name, tempId} = selectedGroup;
		this.refs.deleteConfirmationDialog.close();
		this.setState({ loading: true });
		const reqData = {
			tempId: tempId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/deleteGroupPending', reqData)
			.then(res => {
				// console.log("res:", res.data);
				NotificationManager.success(`Group ${name} Deleted.`, "Success", getNotifTimeout());
				//RELOAD
				this.componentDidMount();
			}).catch(err => {
				console.error("error deleteGroupPending", err);
				NotificationManager.error(`Delete Group ${name} Failed.`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	render() {
		const { data, loading, selectedGroup } = this.state;
		const columns = [
			"Group ID",
			"Group Description",
			"Is Maker",
			"Is Approved",
			"Active",
			"Create By",
			"Create Date",
			"Update By",
			"Update Date",
			"Status",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<Button onClick={() => this.onClickDelete(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-delete"></i> Delete</Button>
						);
					}
				}
			}
		];
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="Group Management - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={"Pending Approval Group"}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="deleteConfirmationDialog"
					title={`Are you sure want to delete Group ${selectedGroup ? selectedGroup.name : ""}?`}
					onConfirm={this.deleteGroup}
				/>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps)(PendingApprovalGroup);
