/**
 * Material Text Field
 */
import React from "react";
import { connect } from 'react-redux';
import { getApi } from 'Api';
import { NotificationManager } from 'react-notifications';
import {
	Select,
	TextField,
	Checkbox,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	InputLabel,
	MenuItem,
	Button,
} from "@material-ui/core";

import { getNotifTimeout } from "Helpers/helpers";

// page title bar
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";

// rct card box
import RctCollapsibleCard from "Components/RctCollapsibleCard/RctCollapsibleCard";

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// redux action
import { logoutUser } from 'Actions';

class AddNewGroupForm extends React.Component {
	state = {
		formData: {
			id: "",
			name: "",
			email: "",
			isApprv: false,
			deleted: false,
			rm_ema: false,
			active: false,
		},
		formError: {
			id: false,
			name: false,
		},
		isEdit: false,
		loading: true
	};

	async componentDidMount() {
		const { token } = this.props;
		const { groupId } = this.props.match.params;
		// console.log("groupId", groupId);
		if (groupId) {
			this.setState({ isEdit: true });
			const reqData = {
				groupId
			}

			try {
				const res = await getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetGroupById', reqData);
				if (res.data.group) {
					const group = res.data.group;
					const { formData } = this.state;
					formData.id = group.id;
					formData.name = group.name;
					formData.isMaker = group.isMaker;
					formData.isApprv = group.isApprv;
					formData.active = group.active;
					formData.createBy = group.createBy;
					formData.createDate = group.createDate;
					
					this.setState({ 
						formData,
						loading: false
					});
				} else {
					//Group not found
					this.props.history.goBack();
				}
			} catch (err) {
				console.error("error GetGroupById", err);
				const { formError } = this.state;
				formError.save = true;
				this.setState({
					loading: false,
					formError
				});
				if (err.response && err.response.status == 401) {
					this.props.logoutUser(true);
				}
			}
		}else{
			this.setState({loading: false});
		}
	}

	handleChange = ({ target }) => {
		const { formData } = this.state;
		const { checked, name, type, value } = target;
		const val = (type === 'checkbox') ? checked : value;
		formData[name] = val;
		this.setState({ formData });
	};

	isCanSave = () => {
		const { formData } = this.state;

		this.setState({
			formError: {
				id: formData.id == '',
				name: formData.name == '',
			}
		})

		return (
			formData.id !== '' &&
			formData.name !== ''
		)
	}
	handleSave = e => {
		e.preventDefault();
		if (this.isCanSave()) {
			this.setState({ loading: true });
			const { formData, isEdit } = this.state;

			if(!isEdit){
				formData.isMaker = false;
				formData.createBy = this.props.user.userId;
			}
			formData.updateBy = this.props.user.userId;
			formData.updateDate = "";
			const data = {
				group: formData,
				userId: this.props.user.userId
			}

			console.log('formData', data);
			getApi(this.props.token).post("/CIMB-Appraisal-Security/api/json/reply/SaveOrUpdateGroupMaker", data)
				.then((res) => {
					if (res.data.responseCode == "00") {
						this.setState({ loading: false });
						this.props.history.push("/app/main/group-management-maker/pending-approval-group");
						NotificationManager.success('Group Created Successfully', "Success", getNotifTimeout());
					} else {
						this.setState({ loading: false });
						NotificationManager.error('Failed to Create Group. ' + res.data.responseDesc, "Error", getNotifTimeout());
					}
				}).catch(err => {
					console.error("error SaveOrUpdateGroupMaker", err);
					this.setState({ loading: false });
					NotificationManager.error('Failed to Create Group.', "Error", getNotifTimeout());
				});
		}
	}

	handleBack = e => {
		e.preventDefault();
		this.props.history.goBack();
	}
	render() {
		const { formData, formError, isEdit, loading } = this.state;
		return (
			<div className="textfields-wrapper">
				<PageTitleBar
					title="Group Management - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard>
					<div className="row d-flex justify-content-between py-10 px-10">
						<div>
							<h2>{isEdit? "Edit Group" : "New Group"}</h2>
						</div>
						<div>
							<Button onClick={this.handleSave} 
								disabled={formError.save}
								variant="contained" 
								className="btn-primary mr-10 mb-10 text-white btn-icon">
									<i className="zmdi zmdi-check"></i> 
									Save
							</Button>
							<Button onClick={this.handleBack} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Cancel</Button>
						</div>
					</div>
					<form noValidate autoComplete="off">
						<div className="row">
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								<div className="form-group">
									<TextField
										required
										disabled={isEdit}
										id="id"
										name="id"
										fullWidth
										label="Group ID"
										value={formData.id}
										error={formError.id}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="name"
										name="name"
										fullWidth
										label="Group Description"
										value={formData.name}
										error={formError.name}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										id="email"
										name="email"
										type="email"
										fullWidth
										label="Email"
										value={formData.email}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<FormControlLabel
										className="mb-0 mt-2"
										control={
											<Checkbox
												id="isApprv"
												name="isApprv"
												color="primary"
												checked={formData.isApprv}
												onChange={this.handleChange}
											/>
										}
										label="Approval Group"
									/>
								</div>
							</div>
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								<div className="form-group">
									<FormControlLabel
										className="mb-0 mt-2"
										control={
											<Checkbox
												id="deleted"
												name="deleted"
												color="primary"
												checked={formData.deleted}
												onChange={this.handleChange}
											/>
										}
										label="Deleted"
									/>
								</div>
								<div className="form-group">
									<FormControlLabel
										className="mb-0 mt-2"
										control={
											<Checkbox
												id="rm_ema"
												name="rm_ema"
												color="primary"
												checked={formData.rm_ema}
												onChange={this.handleChange}
											/>
										}
										label="RM_EMA"
									/>
								</div>
								<div className="form-group">
									<FormControlLabel
										className="mb-0 mt-2"
										control={
											<Checkbox
												id="active"
												name="active"
												color="primary"
												checked={formData.active}
												onChange={this.handleChange}
											/>
										}
										label="Active"
									/>
								</div>
							</div>
						</div>
					</form>
					{loading &&
						<RctSectionLoader />
					}
				</RctCollapsibleCard>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps, {
	logoutUser
})(AddNewGroupForm);