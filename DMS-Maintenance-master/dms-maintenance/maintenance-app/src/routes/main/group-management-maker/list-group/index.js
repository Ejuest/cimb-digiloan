/**
 * Group Management Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class ListGroup extends Component {

	state = {
		groups: null,
		data: [],
		selectedGroup: null,
		loading: true, // loading activity
	}

	componentDidMount() {
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetGroupMaker')
			.then(res => {
				let groups = res.data.groups;
				// console.log("Groups:", groups);
				const data = groups.map((group, index) => [
					group.id,
					group.name,
					group.isMaker ? "Yes" : "No",
					group.isApprv ? "Yes" : "No",
					group.active ? "Yes" : "No",
					group.createBy,
					convertDateASP(group.createDate, "DD-MM-YYYY"),
					group.updateBy ? group.updateBy : "-",
					convertDateASP(group.updateDate, "DD-MM-YYYY"),
					index
				]);
				this.setState({
					groups: groups,
					data: data,
					loading: false
				});
			}).catch(err => {
				console.error("error GetGroupMaker", err);
				this.setState({ loading: false });
			});
	}

	onClickEdit = (idx) => {
		const { groups } = this.state;
		// console.log(groups[idx]);
		this.props.history.push(`/app/main/group-management-maker/list-group/${groups[idx].id}`)
	}

	onClickDelete = (idx) => {
		const { groups } = this.state;
		// console.log(groups[idx]);
		this.refs.deleteConfirmationDialog.open();
		this.setState({ selectedGroup: groups[idx] });
	}

	deleteGroup = () => {
		const { selectedGroup } = this.state;
		const { id, name } = selectedGroup;
		this.refs.deleteConfirmationDialog.close();
		this.setState({ loading: true });
		const reqData = {
			groupId: id,
			userId: this.props.user.userId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/DeleteGroupMaker', reqData)
			.then(res => {
				// console.log("res:", res.data);
				if(res.data.responseCode = "00"){
					NotificationManager.success(`Group ${name} Deleted.`, "Success", getNotifTimeout());
					//RELOAD
					this.componentDidMount();
				}else{
					throw new Error(res.data.responseCode+" "+res.data.responseDesc);
				}
			}).catch(err => {
				console.error("error DeleteGroupMaker", err);
				NotificationManager.error(`Delete Group ${name} Failed. ${err}`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}
	render() {
		const { data, loading, selectedGroup } = this.state;
		const columns = [
			"Group ID",
			"Group Description",
			"Is Maker",
			"Is Approved",
			"Active",
			"Create By",
			"Create Date",
			"Update By",
			"Update Date",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
								<Button onClick={() => this.onClickEdit(value)} variant="contained" color="primary" className="mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-edit"></i> Edit</Button>
								<Button onClick={() => this.onClickDelete(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-delete"></i> Delete</Button>
							</div>
						);
					}
				}
			}
		];
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="Group Management - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={"List Group"}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="deleteConfirmationDialog"
					title={`Are you sure want to delete ${selectedGroup ? selectedGroup.name : ""}?`}
					onConfirm={() => this.deleteGroup()}
				/>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps)(ListGroup);
