/**
 * Miscellaneous Widgets Page
 */
import React, { Component } from 'react';
import {
    CustomCardWidget
} from "Components/Widgets";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

export default class GroupManagementMaker extends Component {
    state = {
        extendUrl : "/app/main/group-management-maker"
    }
    render() {
        const { extendUrl } = this.state;
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Group Management Maker" match={this.props.match} />
                <div className="dash-cards">
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget
                                text="LIST GROUP"
                                bgColor="#E5BE1D"
                                to={`${extendUrl}/list-group`}/>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget 
                                text="NEW GROUP"
                                bgColor="#E8005F" 
                                to={`${extendUrl}/add-group`}/>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget 
                                text="PENDING APPROVAL GROUP" 
                                bgColor="#01E7B9"
                                to={`${extendUrl}/pending-approval-group`}/>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget 
                                text="PENDING MODULE GROUP"
                                bgColor="#FDFEFF" 
                                to={`${extendUrl}/pending-module-group`}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
