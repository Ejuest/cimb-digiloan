/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

export default class PendingApprovalGroup extends Component {

	state = {
		all: false,
		users: null, // initial user data
		selectedUser: null, // selected user to perform operations
		loading: false, // loading activity
	}

	componentDidMount() {
	}

	/**
	 * On Delete
	 */
	onDelete(data) {
		this.refs.deleteConfirmationDialog.open();
		this.setState({ selectedUser: data });
	}

	/**
	 * Delete User Permanently
	 */
	deleteUserPermanently() {
		const { selectedUser } = this.state;
		let users = this.state.users;
		let indexOfDeleteUser = users.indexOf(selectedUser);
		users.splice(indexOfDeleteUser, 1);
		this.refs.deleteConfirmationDialog.close();
		this.setState({ loading: true });
		let self = this;
		setTimeout(() => {
			self.setState({ loading: false, users, selectedUser: null });
			NotificationManager.success('User Deleted!');
		}, 2000);
	}

	render() {
		const { loading } = this.state;
		const columns = ["Group ID", "Group Description", "Email", "Approval Group", "Group Upliner", "Department", "Deleted", "RM_EMA", "Active", "Status", "Created By"];
		const data = [
			["Gabby George", "Business Analyst", "Minneapolis", 30, "$100,000"],
			["Aiden Lloyd", "Business Consultant", "Dallas", 55, "$200,000"],
			["Jaden Collins", "Attorney", "Santa Ana", 27, "$500,000"],
			["Franky Rees", "Business Analyst", "St. Petersburg", 22, "$50,000"],
			["Aaren Rose", "Business Consultant", "Toledo", 28, "$75,000"],
			["Blake Duncan", "Business Management Analyst", "San Diego", 65, "$94,000"],
			["Frankie Parry", "Agency Legal Counsel", "Jacksonville", 71, "$210,000"],
			["Lane Wilson", "Commercial Specialist", "Omaha", 19, "$65,000"],
			["Robin Duncan", "Business Analyst", "Los Angeles", 20, "$77,000"],
			["Mel Brooks", "Business Consultant", "Oklahoma City", 37, "$135,000"],
			["Harper White", "Attorney", "Pittsburgh", 52, "$420,000"],
			["Kris Humphrey", "Agency Legal Counsel", "Laredo", 30, "$150,000"],
			["Frankie Long", "Industrial Analyst", "Austin", 31, "$170,000"],
			["Brynn Robbins", "Business Analyst", "Norfolk", 22, "$90,000"],
			["Justice Mann", "Business Consultant", "Chicago", 24, "$133,000"],
			["Addison Navarro", "Business Management Analyst", "New York", 50, "$295,000"],
			["Jesse Welch", "Agency Legal Counsel", "Seattle", 28, "$200,000"],
			["Eli Mejia", "Commercial Specialist", "Long Beach", 65, "$400,000"],
			["Gene Leblanc", "Industrial Analyst", "Hartford", 34, "$110,000"],
			["Danny Leon", "Computer Scientist", "Newark", 60, "$220,000"],
			["Lane Lee", "Corporate Counselor", "Cincinnati", 52, "$180,000"],
			["Jesse Hall", "Business Analyst", "Baltimore", 44, "$99,000"],
			["Danni Hudson", "Agency Legal Counsel", "Tampa", 37, "$90,000"],
			["Terry Macdonald", "Commercial Specialist", "Miami", 39, "$140,000"],
			["Justice Mccarthy", "Attorney", "Tucson", 26, "$330,000"],
			["Silver Carey", "Computer Scientist", "Memphis", 47, "$250,000"],
			["Franky Miles", "Industrial Analyst", "Buffalo", 49, "$190,000"],
			["Glen Nixon", "Corporate Counselor", "Arlington", 44, "$80,000"],
			["Gabby Strickland", "Business Process Consultant", "Scottsdale", 26, "$45,000"],
			["Mason Ray", "Computer Scientist", "San Francisco", 39, "$142,000"]
		];
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="Group Management - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={"Pending Module Group"}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="deleteConfirmationDialog"
					title="Are You Sure Want To Delete?"
					message="This will delete user permanently."
					onConfirm={() => this.deleteUserPermanently()}
				/>
			</div>
		);
	}
}
