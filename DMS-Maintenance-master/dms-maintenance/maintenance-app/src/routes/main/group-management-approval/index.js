/**
 * Miscellaneous Widgets Page
 */
import React, { Component } from 'react';
import {
    CustomCardWidget
} from "Components/Widgets";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

export default class GroupManagementApproval extends Component {
    state = {
        extendUrl : "/app/main/group-management-approval"
    }
    render() {
        const { extendUrl } = this.state;
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title="Group Management Approval" match={this.props.match} />
                <div className="dash-cards">
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget 
                                text="APPROVAL&nbsp; GROUP" 
                                bgColor="#FDFEFF"
                                to={`${extendUrl}/approval-group`}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
