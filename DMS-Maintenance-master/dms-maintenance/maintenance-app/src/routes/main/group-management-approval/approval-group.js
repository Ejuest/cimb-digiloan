/**
 * Group Management Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class ApprovalGroup extends Component {

	state = {
		groups: null,
		data: [],
		selectedGroup: null,
		loading: true, // loading activity
	}

	componentDidMount = () => {
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetGroupChecker')
			.then(res => {
				let groups = res.data.groups;
				// console.log("Groups:", groups);
				const data = groups.map((group, index) => [
					group.id,
					group.name,
					group.isMaker ? "Yes" : "No",
					group.isApprv ? "Yes" : "No",
					group.active ? "Yes" : "No",
					group.createBy,
					convertDateASP(group.createDate, "DD-MM-YYYY"),
					group.updateBy ? group.updateBy : "-",
					convertDateASP(group.updateDate, "DD-MM-YYYY"),
					group.status,
					index
				]);
				this.setState({
					groups: groups,
					data: data,
					loading: false
				});
			}).catch(err => {
				console.error("error GetGroupChecker", err);
				this.setState({ loading: false });
			});
	}

	onClickApprove = (idx) => {
		const { groups } = this.state;
		const selectedGroup = groups[idx];
		const { tempId, name } = selectedGroup;
		this.setState({ loading: true });
		const reqData = {
			tempId: tempId,
			userId: this.props.user.userId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/ApproveGroupChecker', reqData)
			.then(res => {
				console.log("res:", res.data);
				if (res.data.responseCode == "00") {
					NotificationManager.success(`Group ${name} Approved.`, "Success", getNotifTimeout());
					//RELOAD
					this.componentDidMount();
				} else {
					throw new Error(res.data.responseCode+" "+res.data.responseDesc);
				}
			}).catch(err => {
				console.error("error ApproveGroupChecker", err);
				NotificationManager.error(`Approve Group ${name} Failed. ${err.message}`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	onClickReject = (idx) => {
		const { groups } = this.state;
		this.refs.rejectConfirmationDialog.open();
		this.setState({ selectedGroup: groups[idx] });
	}

	rejectGroup = () => {
		const { selectedGroup } = this.state;
		const { name, tempId } = selectedGroup;
		this.refs.rejectConfirmationDialog.close();
		this.setState({ loading: true });
		const reqData = {
			tempId: tempId,
			userId: this.props.user.userId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/RejectGroupChecker', reqData)
			.then(res => {
				// console.log("res:", res.data);
				NotificationManager.success(`Group ${name} Rejected.`, "Success", getNotifTimeout());
				//RELOAD
				this.componentDidMount();
			}).catch(err => {
				console.error("error RejectGroupChecker", err);
				NotificationManager.error(`Reject Group ${name} Failed.`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	render() {
		const { data, loading, selectedGroup } = this.state;
		const columns = [
			"Group ID",
			"Group Description",
			"Is Maker",
			"Is Approved",
			"Active",
			"Create By",
			"Create Date",
			"Update By",
			"Update Date",
			"Status",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
								<Button onClick={() => this.onClickApprove(value)} variant="contained" className="btn-success mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-check"></i> Approve</Button>
								<Button onClick={() => this.onClickReject(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Reject</Button>
							</div>
						);
					}
				}
			}
		];
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="Group Management - Approval"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={"Approval Group"}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="rejectConfirmationDialog"
					title={`Are you sure want to reject Group ${selectedGroup ? selectedGroup.name : ""}?`}
					onConfirm={this.rejectGroup}
				/>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps)(ApprovalGroup);
