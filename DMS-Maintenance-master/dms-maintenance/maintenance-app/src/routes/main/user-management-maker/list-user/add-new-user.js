/**
 * Material Text Field
 */
import React from "react";
import axios from "axios";
import { connect } from 'react-redux';
import { getApi } from 'Api';
import { NotificationManager } from 'react-notifications';
import {
	Select,
	TextField,
	Checkbox,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	InputLabel,
	MenuItem,
	Button,
} from "@material-ui/core";
import { Alert } from 'reactstrap';

import { getNotifTimeout } from "Helpers/helpers";

// page title bar
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";

// rct card box
import RctCollapsibleCard from "Components/RctCollapsibleCard/RctCollapsibleCard";

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// redux action
import { logoutUser } from 'Actions';

class AddNewUserForm extends React.Component {
	state = {
		formData: {
			id: "",
			name: "",
			phone: "",
			email: "",
			group: "",
			branch: "",
			parent: "",
			userDeleted: false,
			clearLogon: false,
			dormant: false,
			passwordFailCount: 0,
			active: false,
		},
		formError: {
			id: false,
			name: false,
			email: false,
			group: false,
			branch: false,
			passwordFailCount: false,
			save: false
		},
		isEdit: false,
		upliners: null,
		branches: null,
		groups: null,
		loading: true,
		alertVisible: false,
		alertMessage: "",
		selectedUser: null
	};

	async componentDidMount() {
		const { token } = this.props;
		const { userId } = this.props.match.params;
		if (userId) {
			this.setState({ isEdit: true });
			const reqData = {
				userId
			}
			try {
				const res = await getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetUserById', reqData);
				if (res.data.user) {
					const selectedUser = res.data.user
					this.setState({
						selectedUser
					}, this.getFormData);
				} else {
					//User not found
					this.props.history.goBack();
				}
			} catch (err) {
				console.error("error GetUserById", err);
				const { formError } = this.state;
				formError.save = true;
				this.setState({
					loading: false,
					formError
				}, this.onShowAlert(err.message));
				if (err.response && err.response.status == 401) {
					this.props.logoutUser(true);
				}
			}
		} else {
			this.getFormData();
		}


	}

	async getFormData(){
		const { token } = this.props;
		const { selectedUser } = this.state;
		const reqBranchData = {
			"TableName": "MasterBranch",
			"Offset": 0,
			"Limit": 0
		};

		try {
			const [resGetBranches, resGetGroups, resGetUpliners] = await axios.all([
				getApi(token).post('CIMB-Appraisal-Parameter/api/json/reply/GetParamTable', reqBranchData),
				getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetGroupMaker'),
				getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetUsersMaker')
			]);

			if ((resGetBranches.data.responseCode == "00") &&
				(resGetGroups.data.responseCode == "00") &&
				(resGetUpliners.data.responseCode == "00")) {
				this.setState({
					branches: resGetBranches.data.content ? resGetBranches.data.content : null,
					groups: resGetGroups.data.groups ? resGetGroups.data.groups : null,
					upliners: resGetUpliners.data.users ? resGetUpliners.data.users : null,
					loading: false
				}, () => {
					if (selectedUser) {
						this.setState({
							formData: {
								id: selectedUser.id ? selectedUser.id : "",
								name: selectedUser.name ? selectedUser.name : "",
								phone: selectedUser.phone ? selectedUser.phone : "",
								email: selectedUser.email ? selectedUser.email : "",
								group: selectedUser.group ? selectedUser.group : "",
								branch: selectedUser.branch ? selectedUser.branch : "",
								parent: selectedUser.parent ? selectedUser.parent : "",
								userDeleted: selectedUser.userDeleted ? selectedUser.userDeleted: false,
								clearLogon: selectedUser.clearLogon ? selectedUser.clearLogon: false,
								dormant: selectedUser.dormant ? selectedUser.dormant : false,
								passwordFailCount: selectedUser.passwordFailCount ? selectedUser.passwordFailCount : 0,
								active: selectedUser.active ? selectedUser.active : false,
								isLogin: selectedUser.isLogin ? selectedUser.isLogin : false,
								isApproved: selectedUser.isApproved? selectedUser.isApproved : false,
								isMaker: selectedUser.isMaker? selectedUser.isMaker : false,
								password: selectedUser.password? selectedUser.password : "",
								password64: selectedUser.password64? selectedUser.password64 : "",
								createBy: selectedUser.createBy? selectedUser.createBy : "",
								createDate: selectedUser.createDate? selectedUser.createDate : "",
							}
						});
					}
				});
			} else {
				let errorMessage = resGetBranches.data.responseCode == "00" ? "" : "Cannot get Branches. " + resGetBranches.data.responseDesc;
				errorMessage += resGetGroups.data.responseCode == "00" ? "" : " Cannot get Groups. " + resGetGroups.data.responseDesc;
				errorMessage += resGetUpliners.data.responseCode == "00" ? "" : " Cannot get Upliners. " + resGetUpliners.data.responseDesc;
				throw new Error(errorMessage);
			}
		} catch (err) {
			console.error("error getData Branches / Groups / Upliners", err);
			const { formError } = this.state;
			formError.save = true;
			this.setState({
				loading: false,
				formError
			}, this.onShowAlert(err.message));
			if (err.response && err.response.status == 401) {
				this.props.logoutUser(true);
			}
		}
	}

	handleChange = ({ target }) => {
		const { formData } = this.state;
		const { checked, name, type, value } = target;
		const val = (type === 'checkbox') ? checked : value;
		formData[name] = val;
		this.setState({ formData });
	};

	isCanSave = () => {
		const { formData } = this.state;

		this.setState({
			formError: {
				id: formData.id == '',
				name: formData.name == '',
				email: formData.email == '',
				group: formData.group == '',
				branch: formData.branch == '',
				passwordFailCount: formData.passwordFailCount === ''
			}
		})

		return (
			formData.id !== '' &&
			formData.name !== '' &&
			formData.email !== '' &&
			formData.group !== '' &&
			formData.branch !== '' &&
			formData.passwordFailCount !== ''
		)
	}

	handleSave = e => {
		e.preventDefault();
		// console.log("groups", this.state.groups);
		if (this.isCanSave()) {
			this.setState({ loading: true });
			const { formData, groups, isEdit } = this.state;

			groups.forEach((group) => {
				//TODO need optimizing
				if (group.id == formData.group) {
					formData.groupName = group.name;
				}
			});
			if(!isEdit){
				formData.password = "";
				formData.password64 = "";
				formData.dormant = false;
				formData.isLogin = false;
				formData.isApproved = false;
				formData.isMaker = false;
				formData.createBy = this.props.user.userId;
			}
			formData.updateBy = this.props.user.userId;
			formData.updateDate = "";
			// console.log('formData', formData);
			getApi(this.props.token).post("/CIMB-Appraisal-Security/api/json/reply/SaveOrUpdateUsersMaker", formData)
				.then((res) => {
					if (res.data.responseCode == "00") {
						this.setState({ loading: false });
						this.props.history.push("/app/main/user-management-maker/pending-approval-user");
						NotificationManager.success('User Created Successfully', "Success", getNotifTimeout());
					} else {
						this.setState({ loading: false });
						NotificationManager.error('Failed to Create User. ' + res.data.responseDesc, "Error", getNotifTimeout());
					}
				}).catch(err => {
					console.error("error SaveOrUpdateUsersMaker", err);
					this.setState({ loading: false });
					NotificationManager.error('Failed to Create User.', "Error", getNotifTimeout());
				});
		}
	}

	handleBack = e => {
		e.preventDefault();
		this.props.history.goBack();
	}

	onShowAlert = (message) => {
		// console.log("message", message);
		this.setState({
			alertVisible: true,
			alertMessage: message
		});
	}

	onDismissAlert = (key) => {
		this.setState({ [key]: false });
	}

	render() {
		const { formData, formError, isEdit, groups, upliners, branches, loading, alertVisible, alertMessage } = this.state;
		return (
			<div className="textfields-wrapper">
				<PageTitleBar
					title="User Management - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard>
					{loading &&
						<RctSectionLoader />
					}
					<Alert color="danger" isOpen={alertVisible} toggle={() => this.onAlertDismiss("alertVisible")}>
						<b>Error! </b>
						{alertMessage}
					</Alert>
					<div className="row d-flex justify-content-between py-10 px-10">
						<div>
							<h2>{isEdit ? "Edit User" : "New User"}</h2>
						</div>
						<div>
							<Button onClick={this.handleSave}
								disabled={formError.save}
								variant="contained"
								className={`btn-primary mr-10 mb-10 btn-icon ${formError.save ? "" : "text-white"}`}
							><i className="zmdi zmdi-check"></i> Save</Button>
							<Button onClick={this.handleBack} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-close"></i> Cancel</Button>
						</div>
					</div>
					<form noValidate autoComplete="off">
						<div className="row">
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								<div className="form-group">
									<TextField
										required
										disabled={isEdit}
										id="id"
										name="id"
										fullWidth
										label="User ID"
										value={formData.id}
										error={formError.id}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="name"
										name="name"
										fullWidth
										label="Nama Lengkap"
										value={formData.name}
										error={formError.name}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										id="phone"
										name="phone"
										fullWidth
										label="No. Handphone"
										value={formData.phone}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<TextField
										required
										id="email"
										name="email"
										type="email"
										fullWidth
										label="Email"
										value={formData.email}
										error={formError.email}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<FormControl
										required
										fullWidth
										error={formError.group}
									>
										<InputLabel htmlFor="group">Group</InputLabel>
										<Select
											value={formData.group}
											onChange={this.handleChange}
											inputProps={{ name: 'group', id: 'group', }}>
											<MenuItem value=""><em>None</em></MenuItem>
											{(groups) ? groups.map((group, key) => (
												<MenuItem key={key} value={group.id}>{group.name}</MenuItem>
											)) : ""
											}
										</Select>
									</FormControl>
								</div>
								<div className="form-group">
									<FormControl
										required
										fullWidth
										error={formError.branch}
									>
										<InputLabel htmlFor="branch">Branch</InputLabel>
										<Select
											value={formData.branch}
											onChange={this.handleChange}
											inputProps={{ name: 'branch', id: 'branch', }}>
											<MenuItem value=""><em>None</em></MenuItem>
											{(branches) ? branches.map((branch, key) => (
												<MenuItem key={key} value={branch.id}>{branch.name}</MenuItem>
											)) : ""
											}
										</Select>
									</FormControl>
								</div>
							</div>
							<div className="col-sm-6 col-md-6 col-xl-6 d-block">
								<div className="form-group">
									<FormControl fullWidth>
										<InputLabel htmlFor="upliner">Upliner</InputLabel>
										<Select
											value={formData.parent}
											onChange={this.handleChange}
											inputProps={{ name: 'parent', id: 'parent', }}>
											<MenuItem value=""><em>None</em></MenuItem>
											{(upliners) ? upliners.map((upliner, key) => (
												<MenuItem key={key} value={upliner.id}>{upliner.name}</MenuItem>
											)) : ""
											}
										</Select>
									</FormControl>
								</div>
								{isEdit ?
									<div className="form-group">
										<FormControlLabel
											className="mb-0 mt-2"
											control={
												<Checkbox
													id="userDeleted"
													name="userDeleted"
													color="primary"
													checked={formData.userDeleted}
													onChange={this.handleChange}
												/>
											}
											label="User Deleted"
										/>
									</div>
									: ''}
								{isEdit ?
									<div className="form-group">
										<FormControlLabel
											className="mb-0 mt-2"
											control={
												<Checkbox
													id="clearLogon"
													name="clearLogon"
													color="primary"
													checked={formData.clearLogon}
													onChange={this.handleChange}
												/>
											}
											label="Clear Logon"
										/>
									</div>
									: ''}
								{isEdit ?
									<div className="form-group">
										<FormControlLabel
											className="mb-0 mt-2"
											control={
												<Checkbox
													id="dormant"
													name="dormant"
													color="primary"
													checked={formData.dormant}
													onChange={this.handleChange}
												/>
											}
											label="Dormant"
										/>
									</div>
									: ''}
								<div className="form-group">
									<TextField
										required
										id="passwordFailCount"
										name="passwordFailCount"
										type="number"
										fullWidth
										label="False Password Count"
										value={formData.passwordFailCount}
										error={formError.passwordFailCount}
										onChange={this.handleChange}
									/>
								</div>
								<div className="form-group">
									<FormControlLabel
										className="mb-0 mt-2"
										control={
											<Checkbox
												required
												id="active"
												name="active"
												color="primary"
												checked={formData.active}
												onChange={this.handleChange}
											/>
										}
										label="Active"
									/>
								</div>
							</div>
						</div>
					</form>

				</RctCollapsibleCard>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps, {
	logoutUser
})(AddNewUserForm);