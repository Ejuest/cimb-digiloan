/**
 * User Management Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MUIDataTable from "mui-datatables";
import { NotificationManager } from 'react-notifications';

// api
import { getApi } from 'Api';

// app config
import AppConfig from 'Constants/AppConfig';

//helper
import { convertDateASP, getNotifTimeout } from 'Helpers/helpers';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class PendingApprovalUser extends Component {

	state = {
		users: null,
		data: [],
		selectedUser: null, // selected user to perform operations
		loading: true, // loading activity
	}

	componentDidMount() {
		//get list user
		const reqData = {
			userId: this.props.user.userId
		}
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetUsersPending', reqData)
			.then(res => {
				let users = res.data.user;
				// console.log("Users:", users);
				const data = users.map((user, index) => [
					user.id,
					user.name,
					user.email,
					user.group,
					user.branch,
					user.parent? user.parent : "-",
					user.dormant ? "Yes" : "No",
					user.passwordFailCount,
					user.active ? "Yes" : "No",
					user.createBy,
					convertDateASP(user.createDate, "DD-MM-YYYY"),
					user.isApproved ? "Yes" : "No",
					user.isMaker ? "Yes" : "No",
					user.status,
					index
				]);
				this.setState({
					users: users,
					data: data,
					loading: false
				});
			}).catch(err => {
				console.error("error GetUsersPending", err);
				this.setState({
					loading: false
				});
				NotificationManager.error(err.message, "Error", getNotifTimeout());
			});
	}

	onClickDelete = (idx) => {
		const {users} = this.state;
		this.refs.deleteConfirmationDialog.open();
		this.setState({ selectedUser: users[idx] });
	}

	deleteUser = () => {
		const { selectedUser } = this.state;
		const {tempId, name} = selectedUser;
		this.refs.deleteConfirmationDialog.close();
		this.setState({ loading: true });
		const reqData = {
			tempId: tempId
		};
		const token = this.props.token;
		getApi(token).post('CIMB-Appraisal-Security/api/json/reply/DeleteUsersPending', reqData)
			.then(res => {
				// console.log("res:", res.data);
				NotificationManager.success(`User ${name} Deleted.`, "Success", getNotifTimeout());
				//RELOAD
				this.componentDidMount();
			}).catch(err => {
				console.error("err DeleteUsersPending", err);
				NotificationManager.error(`Delete User ${name} Failed.`, "Error", getNotifTimeout());
				this.setState({ loading: false });
			});
	}

	render() {
		const { data, loading, selectedUser } = this.state;
		const columns = [
			"User ID",
			"Nama Lengkap",
			"Email",
			"Group",
			"Branch",
			"Upliner",
			"Dormant",
			"False Pwd Count",
			"Active",
			"Create By",
			"Create Date",
			"Approved",
			"Maker",
			"Status",
			{
				name: "Action",
				options: {
					filter: false,
					sort: false,
					customBodyRender: (value, tableMeta, updateValue) => {
						return (
							<div>
								<Button onClick={() => this.onClickDelete(value)} variant="contained" className="btn-danger mr-10 mb-10 text-white btn-icon"><i className="zmdi zmdi-delete"></i> Delete</Button>
							</div>
						);
					}
				}
			}
		];
		const options = {
			filterType: 'dropdown',
			responsive: 'scrollMaxHeight',
			selectableRows: 'none'
		};
		return (
			<div className="user-management">
				<Helmet>
					<title>{AppConfig.brandName}</title>
					<meta name="description" content={AppConfig.brandName} />
				</Helmet>
				<PageTitleBar
					title="User Management - Maker"
					match={this.props.match}
				/>
				<RctCollapsibleCard fullBlock>
					{loading &&
						<RctSectionLoader />
					}
					<MUIDataTable
						title={"Pending Approval User"}
						data={data}
						columns={columns}
						options={options}
					/>
				</RctCollapsibleCard>
				<DeleteConfirmationDialog
					ref="deleteConfirmationDialog"
					title={`Are you sure want to delete ${selectedUser? selectedUser.name: ""}?`}
					onConfirm={() => this.deleteUser()}
				/>
			</div>
		);
	}
}


// map state to props
const mapStateToProps = ({ authUser }) => {
	const user = JSON.parse(authUser.user);
	const token = JSON.parse(authUser.token);
	return { user, token };
}

export default connect(mapStateToProps)(PendingApprovalUser);
