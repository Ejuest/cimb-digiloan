/**
 * Miscellaneous Widgets Page
 */
import React, { Component } from 'react';
import {
    CustomCardWidget
} from "Components/Widgets";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

export default class UserManagementMaker extends Component {
    state = {
        extendUrl : "/app/main/user-management-maker"
    }
    render() {
        const {extendUrl} = this.state;
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title="User Management Maker" match={this.props.match} />
                <div className="dash-cards">
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget
                                text="LIST USER"
                                bgColor="#E5BE1D"
                                to={`${extendUrl}/list-user`}/>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget 
                                text="NEW USER"
                                bgColor="#E8005F" 
                                to={`${extendUrl}/add-user`}/>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget 
                                text="PENDING APPROVAL USER" 
                                bgColor="#01E7B9"
                                to={`${extendUrl}/pending-approval-user`}/>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-3 col-xl-3 w-xs-half-block">
                            <CustomCardWidget 
                                    text="Weather" 
                                    bgColor="#E5BE1D"
                                    to={`${extendUrl}/weather`}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
