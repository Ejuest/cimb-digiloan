/**
 * Auth Actions
 */
import axios from 'axios';
import { NotificationManager } from 'react-notifications';
import {
   LOGIN_USER,
   LOGIN_USER_SUCCESS,
   LOGIN_USER_FAILURE,
   LOGOUT_USER,
   SIGNUP_USER,
   SIGNUP_USER_SUCCESS,
   SIGNUP_USER_FAILURE,
   SET_SIDEBAR_MENU,
} from 'Actions/types';

import { getTokenApi, getApi } from 'Api';
import { getNotifTimeout } from 'Helpers/helpers';

/**
 * @function changeMenusFormat changeMenusFormat from Backend (temporary)
 * @param {*} menus oldMenus
 */

/**
 * Redux Action To Sigin User
 */
export const signinUser = (user, history) => (dispatch) => {
   dispatch({ type: LOGIN_USER });
   //TODO call login API here
  
   const token = "12345";
   localStorage.setItem("token", JSON.stringify(token));
   
  
   const newMenus = { category1:[
    {
      "menu_title": "INBOX",
      "menu_icon": "zmdi zmdi-email",
      "type_multi": null,
      "child_routes": [
        {
          "menu_title": "New Appraisal",
          "path": "/app/main/internal-request-order/new-appraisal/inbox"
        },
        {
          "menu_title": "Re Appraisal",
          "path": "/app/main/internal-request-order/re-appraisal/inbox"
        },
        {
          "menu_title":  "Banding",
          "path": "/app/main/internal-request-order/banding/inbox"

        },
        {
          "menu_title":  "Pending",
          "path": "/app/main/internal-request-order/pending/inbox"

        }
       
      ]
    }
    ,
      {
        "menu_title": "APPRAISAL",
        "menu_icon": "zmdi zmdi-view-web",
        "type_multi": null,
        "child_routes": [
          {
            "menu_title": "New Appraisal",
            "path": "/app/main/internal-request-order/new-appraisal/main/1"
          },
          {
            "menu_title": "Re Appraisal",
            "path": "/app/main/internal-request-order/re-appraisal/re-appraisal/1"
          },
          {
            "menu_title":  "Banding",
            "path": "/app/main/internal-request-order/banding/main/1"
  
          },
          {
            "menu_title":  "Pending",
            "path": "/app/main/internal-request-order/pending/main/1"
  
          }
         
        ]
      },
      {
        "menu_title": "User & Group Management - Maker",
        "type_multi": null,
        "child_routes": [
          {
            "menu_title": "User Management - Maker",
            "path": "/app/main/user-management-maker"
          },
          {
            "menu_title": "Group Management - Maker",
            "path": "/app/main/group-management-maker"
          }
        ,
          {
            "menu_title": "Group Management - Weather",
            "path": "/app/main/user-management-maker/weather"
            
          }
        ]
      },
      {
        "menu_title": "User & Group Management - Approval",
        "type_multi": null,
        "child_routes": [
          {
            "menu_title": "User Management - Approval",
            "path": "/app/main/user-management-approval"
          },
          {
            "menu_title": "Group Management - Approval",
            "path": "/app/main/group-management-approval"
          }
        ]
      },
      {
        "menu_title": "Parameter System - Maker",
        "type_multi": null,
        "child_routes": [
          {
            "menu_title": "Existing Parameter",
            "path": "/app/main/parameter-system-maker/existing-parameter"
          },
          {
            "menu_title": "Pending Approval",
            "path": "/app/main/parameter-system-maker/pending-approval"
          }
        ]
      },
      {
        "menu_title": "Parameter System - Approval",
        "type_multi": null,
        "child_routes": [
          {
            "menu_title": "Approval",
            "path": "/app/main/parameter-system-maker/approval"
          }
        ]
      },
      {
        "menu_title": "Parameter Dedup - Maker",
        "type_multi": null,
        "child_routes": [
          {
            "menu_title": "Existing Parameter",
            "path": "/app/main/parameter-dedup-maker/existing-parameter"
          },
          {
            "menu_title": "Pending Approval",
            "path": "/app/main/parameter-dedup-maker/pending-approval"
          }
        ]
      }
    ]}

   //localStorage.setItem("menus", menuforce);
   localStorage.setItem("menus", JSON.stringify(newMenus));
  // console.log("return menu",JSON.stringify(newMenus) );
   dispatch({ 
      type: SET_SIDEBAR_MENU, 
      payload: newMenus
   });

   const data = {
      'UserId': "user.email",
      'Password': "user123"
   }
  
   localStorage.setItem("user", JSON.stringify(data));
         dispatch({ 
            type: LOGIN_USER_SUCCESS, 
            payload: { 
               user: localStorage.getItem('user'), 
               token: localStorage.getItem('token'),
               menus: localStorage.getItem('menus'),
            }
         });
         history.push('/');
         NotificationManager.success('User Login Successfully!', "Success", getNotifTimeout());
}

/**
 * Redux Action To Signout User
 */
export const logoutUser = (isForced) => (dispatch) => {
   dispatch({ type: LOGOUT_USER });
   localStorage.removeItem('user');
   localStorage.removeItem('token');
   localStorage.removeItem('menus');
   if (isForced){
      NotificationManager.error("You are not authorized.", "Error", getNotifTimeout());
   }else{
      NotificationManager.success('User Logout Successfully', "Success", getNotifTimeout());
   }
}

/**
 * Redux Action To Signup User
 */
// export const signupUser = (user, history) => (dispatch) => {
//    dispatch({ type: SIGNUP_USER });
//    firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
//       .then((success) => {
//          localStorage.setItem("user_id", "user-id");
//          dispatch({ type: SIGNUP_USER_SUCCESS, payload: localStorage.getItem('user_id') });
//          history.push('/');
//          NotificationManager.success('Account Created Successfully!');
//       })
//       .catch((error) => {
//          dispatch({ type: SIGNUP_USER_FAILURE });
//          NotificationManager.error(error.message);
//       })
// }