/**
 * Chat App Actions
 */
import {
    ADD,
    DELETE,
    UPDATE
 } from 'Actions/types';
 
 export const onAdd = (data) => ({
    type: ADD,
    payload: data
 });
 
 export const onDelete = (data) => ({
    type: DELETE,
    payload: data
 });
 
 export const onUpdate= (data, ID) => ({
    type: UPDATE,
    payload: { data, ID }
 })