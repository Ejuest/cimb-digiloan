/**
 * Auth Actions
 */
import axios from 'axios';
import { NotificationManager } from 'react-notifications';
import {
   LOGIN_USER,
   LOGIN_USER_SUCCESS,
   LOGIN_USER_FAILURE,
   LOGOUT_USER,
   SIGNUP_USER,
   SIGNUP_USER_SUCCESS,
   SIGNUP_USER_FAILURE,
   SET_SIDEBAR_MENU,
} from 'Actions/types';

import { getTokenApi, getApi } from 'Api';
import { getNotifTimeout } from 'Helpers/helpers';

/**
 * @function changeMenusFormat changeMenusFormat from Backend (temporary)
 * @param {*} menus oldMenus
 */

const changeMenusFormat = (menus) => {
   let menu = menus[0].menu;
   const newMenus = {
      'category1' : menu.map((mn, index) => {
         const newMenu = {
            "menu_title": mn.menu_desc,
            "type_multi": null,
            "child_routes": mn.child_routes.map((subMenu) =>{
               let path = subMenu.path;
               switch(subMenu.menu_name){
                  case "UserMngtMaker":
                     path = "/app/main/user-management-maker";
                     break;
                  case "GroupMngtMaker":
                     path = "/app/main/group-management-maker";
                     break;
                  case "UserMngtApproval":
                     path = "/app/main/user-management-approval";
                     break;
                  case "GroupMngtApproval":
                     path = "/app/main/group-management-approval";
                     break;
                  case "ExistingParameter":
                     path = "/app/main/parameter-system-maker/existing-parameter";
                     break;
                  case "PendingApproval":
                     path = "/app/main/parameter-system-maker/pending-approval";
                     break;
                  case "Approval":
                     path = "/app/main/parameter-system-maker/approval";
                     break;
                  case "DedupExistingParameter":
                     path = "/app/main/parameter-dedup-maker/existing-parameter";
                     break;
                  case "DedupPendingApproval":
                     path = "/app/main/parameter-dedup-maker/pending-approval";
                     break;
                  case "DedupApproval":
                     path = "/app/main/parameter-dedup-approval/approval";
                     break;
                  default:
                     path = "/";
                     break;
               }
               const newSubMenu = {
                  "menu_title": subMenu.menu_desc,
                  "path": path
               }
               return newSubMenu;
            })
         }

         return newMenu;
      })
   };
   return newMenus;
}

/**
 * Redux Action To Sigin User
 */
export const signinUser = (user, history) => (dispatch) => {
   dispatch({ type: LOGIN_USER });
   //TODO call login API here
   getTokenApi().post('CIMB-Appraisal-Security/api/json/reply/Token')
   .then((resToken)=>{
      // console.log('resToken', resToken);
      const token = resToken.data;
      localStorage.setItem("token", JSON.stringify(token));
      
      const data = {
         'userId': user.email
      }
      return getApi(token).post('CIMB-Appraisal-Security/api/json/reply/GetAllMenus', data);
   })
   .then((resMenus)=>{
      console.log('resMenus', resMenus);
      const menus = resMenus.data.menus;
      const newMenus = changeMenusFormat(menus);
      const token = localStorage.getItem('token');
      const menuforce = { category1:[
         {
           "menu_title": "User & Group Management - Maker",
           "type_multi": null,
           "child_routes": [
             {
               "menu_title": "User Management - Maker",
               "path": "/app/main/user-management-maker"
             },
             {
               "menu_title": "Group Management - Maker",
               "path": "/app/main/group-management-maker"
             }
           ,
             {
               "menu_title": "Group Management - Weather",
               "path": "/app/main/user-management-maker/weather"
               
             }
           ]
         },
         {
           "menu_title": "User & Group Management - Approval",
           "type_multi": null,
           "child_routes": [
             {
               "menu_title": "User Management - Approval",
               "path": "/app/main/user-management-approval"
             },
             {
               "menu_title": "Group Management - Approval",
               "path": "/app/main/group-management-approval"
             }
           ]
         },
         {
           "menu_title": "Parameter System - Maker",
           "type_multi": null,
           "child_routes": [
             {
               "menu_title": "Existing Parameter",
               "path": "/app/main/parameter-system-maker/existing-parameter"
             },
             {
               "menu_title": "Pending Approval",
               "path": "/app/main/parameter-system-maker/pending-approval"
             }
           ]
         },
         {
           "menu_title": "Parameter System - Approval",
           "type_multi": null,
           "child_routes": [
             {
               "menu_title": "Approval",
               "path": "/app/main/parameter-system-maker/approval"
             }
           ]
         },
         {
           "menu_title": "Parameter Dedup - Maker",
           "type_multi": null,
           "child_routes": [
             {
               "menu_title": "Existing Parameter",
               "path": "/app/main/parameter-dedup-maker/existing-parameter"
             },
             {
               "menu_title": "Pending Approval",
               "path": "/app/main/parameter-dedup-maker/pending-approval"
             }
           ]
         },
         {
           "menu_title": "Parameter Dedup - Approval",
           "type_multi": null,
           "child_routes": [
             {
               "menu_title": "Approval",
               "path": "/app/main/parameter-dedup-approval/approval"
             }
           ]
         }
       ]}

      //localStorage.setItem("menus", menuforce);
      localStorage.setItem("menus", JSON.stringify(menuforce));
     // console.log("return menu",JSON.stringify(newMenus) );
      dispatch({ 
         type: SET_SIDEBAR_MENU, 
         payload: newMenus
      });

      const data = {
         'UserId': user.email,
         'Password': user.password
      }
      return getApi(token).post('CIMB-Appraisal-Security/api/json/reply/SignIn', data);
   })
   .then((resLogin) => {
      // console.log('resLogin', resLogin);
      const data = resLogin.data;
      if(data.responseCode == '00'){
         localStorage.setItem("user", JSON.stringify(data));
         dispatch({ 
            type: LOGIN_USER_SUCCESS, 
            payload: { 
               user: localStorage.getItem('user'), 
               token: localStorage.getItem('token'),
               menus: localStorage.getItem('menus'),
            }
         });
         history.push('/');
         NotificationManager.success('User Login Successfully!', "Success", getNotifTimeout());
      }else{
         NotificationManager.error(data.responseDesc, "Error", getNotifTimeout());
      }
      
   })
   .catch((error) => {
      console.log('error', error);
      dispatch({ type: LOGIN_USER_FAILURE });
      NotificationManager.error(error.message, "Error", getNotifTimeout());
   });
}

/**
 * Redux Action To Signout User
 */
export const logoutUser = (isForced) => (dispatch) => {
   dispatch({ type: LOGOUT_USER });
   localStorage.removeItem('user');
   localStorage.removeItem('token');
   localStorage.removeItem('menus');
   if (isForced){
      NotificationManager.error("You are not authorized.", "Error", getNotifTimeout());
   }else{
      NotificationManager.success('User Logout Successfully', "Success", getNotifTimeout());
   }
}

/**
 * Redux Action To Signup User
 */
// export const signupUser = (user, history) => (dispatch) => {
//    dispatch({ type: SIGNUP_USER });
//    firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
//       .then((success) => {
//          localStorage.setItem("user_id", "user-id");
//          dispatch({ type: SIGNUP_USER_SUCCESS, payload: localStorage.getItem('user_id') });
//          history.push('/');
//          NotificationManager.success('Account Created Successfully!');
//       })
//       .catch((error) => {
//          dispatch({ type: SIGNUP_USER_FAILURE });
//          NotificationManager.error(error.message);
//       })
// }