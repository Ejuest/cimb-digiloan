/**
 * Page Title Bar Component
 * Used To Display Page Title & Breadcrumbs
 */
import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// get display string
const getDisplayString = (sub) => {
   const arr = sub.split("-").join(" ");
   return arr;
   // if (arr.length > 1) {
   //    return <IntlMessages id={`sidebar.${arr[0].charAt(0) + arr[0].slice(1) + arr[1].charAt(0).toUpperCase() + arr[1].slice(1)}`} />
   // } else {
   //    return <IntlMessages id={`sidebar.${sub.charAt(0) + sub.slice(1)}`} />
   // }

};

// get url string
const getUrlString = (path, sub, index) => {
   if (index === 0) {
      return '/';
   } else {
      return '/' + path.split(sub)[0] + sub;
   }
};

const PageTitleBar = ({ title, match, enableBreadCrumb, history, reload }) => {
   const path = match.url.substr(1);
   const subPath = path.split('/');
   
   const handleBack = e =>{
      e.preventDefault();
      if(reload)
         window.location.reload();
      else
         history.goBack();
   }

   return (
      <div className="page-title d-flex justify-content-between align-items-center">
         {title &&
            <div className="page-title-wrap">
               <i className="ti-angle-left" style={{cursor: "pointer"}} onClick={handleBack}></i>
               <h2 className="">{title}</h2>
            </div>
         }
         {enableBreadCrumb &&
            <Breadcrumb className="mb-0" tag="nav">
               {subPath.map((sub, index) => {
                  if(index > 5){
                     return "";
                  }else if(subPath.length > 5){
                     return <BreadcrumbItem active={(6 === index + 1)}
                     tag={(6 === index + 1) ? "span" : Link} key={index}
                     to={getUrlString(path, sub, index)}>{getDisplayString(sub)}</BreadcrumbItem>   
                  }
                  return <BreadcrumbItem active={(subPath.length === index + 1)}
                     tag={(subPath.length === index + 1) ? "span" : Link} key={index}
                     to={getUrlString(path, sub, index)}>{getDisplayString(sub)}</BreadcrumbItem>
               }
               )}
            </Breadcrumb>
         }
      </div>
   )
};

// default props value
PageTitleBar.defaultProps = {
   enableBreadCrumb: true
}

export default withRouter(PageTitleBar);
