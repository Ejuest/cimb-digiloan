/**
 * Custom Card Widget
 */
import React, { Component } from 'react';
import  { Textfit } from 'react-textfit-17';
import { Link } from 'react-router-dom';

class CustomCard extends Component {
    render() {
        const { text, bgColor, to } = this.props;
        return (
            <div className="card p-15" style={{backgroundColor: bgColor}}>
                <Link to={to? to: "#"} style={{color: "#464D69"}}>
                    <Textfit mode="multi" style={{height: 150, maxHeight: 150}}>
                        {text}
                    </Textfit>
                </Link>
                <hr style={{borderTop: '2px solid', margin: 0}}/>
            </div>
        );
    }
}

export default CustomCard;
