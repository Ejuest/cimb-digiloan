/**
 ** Session Slider Static
 **/
import React, { Component } from "react";
import Slider from "react-slick";

// api
import api from "Api";

export default class SessionSliderStatic extends Component {
	state = {
		sessionUsersData:{
			id: 1,
			name: "PT Bank CIMB Niaga Tbk",
			profile: require('Assets/img/cimb-niaga-login.png'),
			body:
				"CIMB Niaga is Indonesia's sixth largest bank by assets, established in 1955. CIMB Niaga, which is majority-owned by CIMB Group, is the largest payment bank in terms of transaction value under the Indonesian Central Securities Depository. With 11% of market share, CIMB Niaga is the third largest mortgage provider in Indonesia."
		}
	};

	render() {
		const { sessionUsersData } = this.state;
		return (
			<div className="session-slider">
				<div>
					<img
						src={sessionUsersData.profile}
						alt="session-slider"
						className="img-fluid"
						width="377"
						height="588"
					/>
				</div>
			</div>
		);
	}
}
