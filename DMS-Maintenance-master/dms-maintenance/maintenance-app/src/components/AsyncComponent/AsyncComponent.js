/**
 * AsyncComponent
 * Code Splitting Component / Server Side Rendering
 */
import React from 'react';
import Loadable from 'react-loadable';

// rct page loader
import RctPageLoader from 'Components/RctPageLoader/RctPageLoader';


// ecommerce dashboard
const AsyncEcommerceDashboardComponent = Loadable({
	loader: () => import("Routes/dashboard/ecommerce"),
	loading: () => <RctPageLoader />,
});

// agency dashboard
const AsyncSaasDashboardComponent = Loadable({
	loader: () => import("Routes/dashboard/saas"),
	loading: () => <RctPageLoader />,
});

// agency dashboard
const AsyncAgencyDashboardComponent = Loadable({
	loader: () => import("Routes/dashboard/agency"),
	loading: () => <RctPageLoader />,
});

// boxed dashboard
const AsyncNewsDashboardComponent = Loadable({
	loader: () => import("Routes/dashboard/news"),
	loading: () => <RctPageLoader />,
});

const AsyncUserWidgetComponent = Loadable({
	loader: () => import("Routes/widgets/user-widgets"),
	loading: () => <RctPageLoader />,
});

const AsyncUserChartsComponent = Loadable({
	loader: () => import("Routes/widgets/charts-widgets"),
	loading: () => <RctPageLoader />,
});

const AsyncGeneralWidgetsComponent = Loadable({
	loader: () => import("Routes/widgets/general-widgets"),
	loading: () => <RctPageLoader />,
});

const AsyncPromoWidgetsComponent = Loadable({
	loader: () => import("Routes/widgets/promo-widgets"),
	loading: () => <RctPageLoader />,
});

/*---------------- Session ------------------*/

// Session Login
const AsyncSessionLoginComponent = Loadable({
	loader: () => import("Routes/session/login"),
	loading: () => <RctPageLoader />,
});

// Session Register
const AsyncSessionRegisterComponent = Loadable({
	loader: () => import("Routes/session/register"),
	loading: () => <RctPageLoader />,
});

// Session Lock Screen
const AsyncSessionLockScreenComponent = Loadable({
	loader: () => import("Routes/session/lock-screen"),
	loading: () => <RctPageLoader />,
});

// Session Forgot Password
const AsyncSessionForgotPasswordComponent = Loadable({
	loader: () => import("Routes/session/forgot-password"),
	loading: () => <RctPageLoader />,
});

// Session Page 404
const AsyncSessionPage404Component = Loadable({
	loader: () => import("Routes/session/404"),
	loading: () => <RctPageLoader />,
});

// Session Page 404
const AsyncSessionPage500Component = Loadable({
	loader: () => import("Routes/session/500"),
	loading: () => <RctPageLoader />,
});

// terms and condition
const AsyncTermsConditionComponent = Loadable({
	loader: () => import("Routes/pages/terms-condition"),
	loading: () => <RctPageLoader />,
});

/*--------main user management maker------*/

const AsyncUserManagementMakerComponent = Loadable({
	loader: () => import("Routes/main/user-management-maker"),
	loading: () => <RctPageLoader />,
});

const AsyncUserManagementMakerListUserComponent = Loadable({
	loader: () => import("Routes/main/user-management-maker/list-user"),
	loading: () => <RctPageLoader />,
});

const AsyncUserManagementMakerAddUserComponent = Loadable({
	loader: () => import("Routes/main/user-management-maker/list-user/add-new-user"),
	loading: () => <RctPageLoader />,
});

const AsyncUserManagementMakerPendingApprovalUserComponent = Loadable({
	loader: () => import("Routes/main/user-management-maker/pending-approval-user"),
	loading: () => <RctPageLoader />,
});


const AsyncUserManagementMakerWeatherComponent = Loadable({
	loader: () => import("Routes/main/user-management-maker/weather"),
	loading: () => <RctPageLoader />,
});

/*--------main user management approval------*/

const AsyncUserManagementApprovalComponent = Loadable({
	loader: () => import("Routes/main/user-management-approval"),
	loading: () => <RctPageLoader />,
});


const AsyncUserManagementApprovalUserComponent = Loadable({
	loader: () => import("Routes/main/user-management-approval/approval-user"),
	loading: () => <RctPageLoader />,
});

/*--------main group management maker------*/

const AsyncGroupManagementMakerComponent = Loadable({
	loader: () => import("Routes/main/group-management-maker"),
	loading: () => <RctPageLoader />,
});

const AsyncGroupManagementMakerListGroupComponent = Loadable({
	loader: () => import("Routes/main/group-management-maker/list-group"),
	loading: () => <RctPageLoader />,
});

const AsyncGroupManagementMakerAddGroupComponent = Loadable({
	loader: () => import("Routes/main/group-management-maker/list-group/add-new-group"),
	loading: () => <RctPageLoader />,
});

const AsyncGroupManagementMakerPendingApprovalGroupComponent = Loadable({
	loader: () => import("Routes/main/group-management-maker/pending-approval-group"),
	loading: () => <RctPageLoader />,
});

const AsyncGroupManagementMakerPendingModuleGroupComponent = Loadable({
	loader: () => import("Routes/main/group-management-maker/pending-module-group"),
	loading: () => <RctPageLoader />,
});

/*--------main group management approval------*/

const AsyncGroupManagementApprovalComponent = Loadable({
	loader: () => import("Routes/main/group-management-approval"),
	loading: () => <RctPageLoader />,
});

const AsyncGroupManagementApprovalGroupComponent = Loadable({
	loader: () => import("Routes/main/group-management-approval/approval-group"),
	loading: () => <RctPageLoader />,
});

/*--------main parameter system ------*/

const AsyncParameterSystemMakerComponent = Loadable({
	loader: () => import("Routes/main/parameter-system-maker"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterSystemMakerExistingParamComponent = Loadable({
	loader: () => import("Routes/main/parameter-system-maker/ExistingParameter"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterSystemMakerListParamTableComponent = Loadable({
	loader: () => import("Routes/main/parameter-system-maker/ListParameterTable"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterSystemMakerAddParamComponent = Loadable({
	loader: () => import("Routes/main/parameter-system-maker/AddNewParam"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterSystemMakerAddParamFormDynamicComponent = Loadable({
	loader: () => import("Routes/main/parameter-system-maker/AddParamFormDynamic"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterSystemMakerPendingApprovalComponent = Loadable({
	loader: () => import("Routes/main/parameter-system-maker/PendingApprovalParam"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterSystemMakerApprovalComponent = Loadable({
	loader: () => import("Routes/main/parameter-system-maker/ApprovalParam"),
	loading: () => <RctPageLoader />,
});

/*--------main parameter dedup ------*/

const AsyncParameterDedupMakerComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupMakerListParamComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/ListParameterDedup"),
	loading: () => <RctPageLoader />,
	render(loaded, props) {
		let Component = loaded.default;
		return <Component {...props} linkPage="existing-parameter" />;
	}
});

const AsyncParameterDedupMakerListPendingApprovalComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/ListParameterDedup"),
	loading: () => <RctPageLoader />,
	render(loaded, props) {
		let Component = loaded.default;
		return <Component {...props} linkPage="pending-approval" />;
	}
});

const AsyncParameterDedupMakerListApprovalComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/ListParameterDedup"),
	loading: () => <RctPageLoader />,
	render(loaded, props) {
		let Component = loaded.default;
		return <Component {...props} linkPage="approval" />;
	}
});

const AsyncParameterDedupMakerExistingParameterComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/ExistingParameterDedup"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupMakerAddEnumDupCategoryComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/AddEnumDupCategory"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupMakerAddEnumDupOperatorComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/AddEnumDupOperator"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupMakerAddEnumDupTypeComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/AddEnumDupType"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupMakerAddMasterDupCleansingComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/AddMasterDupCleansing"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupMakerAddMasterDupCriteriaComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/AddMasterDupCriteria"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupMakerAddMasterDupCriteriaDetailComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/AddMasterDupCriteriaDetail"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupMakerPendingApprovalComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-maker/PendingApprovalDedup"),
	loading: () => <RctPageLoader />,
});

const AsyncParameterDedupApprovalComponent = Loadable({
	loader: () => import("Routes/main/parameter-dedup-approval/ApprovalDedup"),
	loading: () => <RctPageLoader />,
});

// internal-request-order
const AsyncInternalRequestOrderNewAppraisalInboxComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/new-appraisal/inbox"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderNewAppraisalMainComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/new-appraisal/main"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderNewAppraisalDocumentCheckingComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/new-appraisal/document-checking"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderNewAppraisalPaymentComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/new-appraisal/payment"),
	loading: () => <RctPageLoader />,
});

const AsyncInternalRequestOrderReAppraisalInboxComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/re-appraisal/inbox"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderReAppraisalMainComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/re-appraisal/main"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderReAppraisalReAppraisalComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/re-appraisal/re-appraisal"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderReAppraisalDocumentCheckingComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/re-appraisal/document-checking"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderReAppraisalPaymentComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/re-appraisal/payment"),
	loading: () => <RctPageLoader />,
});

const AsyncInternalRequestOrderBandingInboxComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/banding/inbox"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderBandingMainComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/banding/main"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderBandingDocumentCheckingComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/banding/document-checking"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderBandingPaymentComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/banding/payment"),
	loading: () => <RctPageLoader />,
});

const AsyncInternalRequestOrderPendingInboxComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/pending/inbox"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderPendingMainComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/pending/main"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderPendingDocumentCheckingComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/pending/document-checking"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderPendingPaymentComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/pending/payment"),
	loading: () => <RctPageLoader />,
});

const AsyncInternalRequestOrderInputPembandingTanahComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/penilaian/input-pembanding-tanah"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderInputPembandingBangunanComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/penilaian/input-pembanding-bangunan"),
	loading: () => <RctPageLoader />,
});
const AsyncInternalRequestOrderDataPembandingComponent = Loadable({
	loader: () => import("Routes/main/internal-request-order/penilaian/data-pembanding"),
	loading: () => <RctPageLoader />,
});

export {
	AsyncUserWidgetComponent,
	AsyncUserChartsComponent,
	AsyncGeneralWidgetsComponent,
	AsyncPromoWidgetsComponent,
	AsyncSessionLoginComponent,
	AsyncSessionRegisterComponent,
	AsyncSessionLockScreenComponent,
	AsyncSessionForgotPasswordComponent,
	AsyncSessionPage404Component,
	AsyncSessionPage500Component,
	AsyncTermsConditionComponent,
	AsyncEcommerceDashboardComponent,
	AsyncSaasDashboardComponent,
	AsyncAgencyDashboardComponent,
	AsyncNewsDashboardComponent,
	AsyncUserManagementMakerComponent,
	AsyncUserManagementMakerListUserComponent,
	AsyncUserManagementMakerAddUserComponent,
	AsyncUserManagementMakerPendingApprovalUserComponent,
	AsyncUserManagementMakerWeatherComponent,
	AsyncUserManagementApprovalComponent,
	AsyncUserManagementApprovalUserComponent,
	AsyncGroupManagementMakerComponent,
	AsyncGroupManagementMakerListGroupComponent,
	AsyncGroupManagementMakerAddGroupComponent,
	AsyncGroupManagementMakerPendingApprovalGroupComponent,
	AsyncGroupManagementMakerPendingModuleGroupComponent,
	AsyncGroupManagementApprovalComponent,
	AsyncGroupManagementApprovalGroupComponent,
	AsyncParameterSystemMakerComponent,
	AsyncParameterSystemMakerExistingParamComponent,
	AsyncParameterSystemMakerListParamTableComponent,
	AsyncParameterSystemMakerAddParamComponent,
	AsyncParameterSystemMakerAddParamFormDynamicComponent,
	AsyncParameterSystemMakerPendingApprovalComponent,
	AsyncParameterSystemMakerApprovalComponent,
	AsyncParameterDedupMakerComponent,
	AsyncParameterDedupMakerListParamComponent,
	AsyncParameterDedupMakerListPendingApprovalComponent,
	AsyncParameterDedupMakerListApprovalComponent,
	AsyncParameterDedupMakerExistingParameterComponent,
	AsyncParameterDedupMakerAddEnumDupCategoryComponent,
	AsyncParameterDedupMakerAddEnumDupOperatorComponent,
	AsyncParameterDedupMakerAddEnumDupTypeComponent,
	AsyncParameterDedupMakerAddMasterDupCleansingComponent,
	AsyncParameterDedupMakerAddMasterDupCriteriaComponent,
	AsyncParameterDedupMakerAddMasterDupCriteriaDetailComponent,
	AsyncParameterDedupMakerPendingApprovalComponent,
	AsyncParameterDedupApprovalComponent,

	AsyncInternalRequestOrderNewAppraisalInboxComponent,
	AsyncInternalRequestOrderNewAppraisalDocumentCheckingComponent,
	AsyncInternalRequestOrderNewAppraisalMainComponent,
	AsyncInternalRequestOrderNewAppraisalPaymentComponent,

	AsyncInternalRequestOrderReAppraisalInboxComponent,
	AsyncInternalRequestOrderReAppraisalDocumentCheckingComponent,
	AsyncInternalRequestOrderReAppraisalMainComponent,
	AsyncInternalRequestOrderReAppraisalReAppraisalComponent,
	AsyncInternalRequestOrderReAppraisalPaymentComponent,

	AsyncInternalRequestOrderBandingInboxComponent,
	AsyncInternalRequestOrderBandingDocumentCheckingComponent,
	AsyncInternalRequestOrderBandingMainComponent,
	AsyncInternalRequestOrderBandingPaymentComponent,

	AsyncInternalRequestOrderPendingInboxComponent,
	AsyncInternalRequestOrderPendingDocumentCheckingComponent,
	AsyncInternalRequestOrderPendingMainComponent,
	AsyncInternalRequestOrderPendingPaymentComponent,

	AsyncInternalRequestOrderInputPembandingTanahComponent,
	AsyncInternalRequestOrderInputPembandingBangunanComponent,
	AsyncInternalRequestOrderDataPembandingComponent
};
