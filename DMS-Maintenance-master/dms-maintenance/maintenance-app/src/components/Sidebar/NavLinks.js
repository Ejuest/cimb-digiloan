// sidebar nav links
export default {
   category1: [
      {
         "menu_title": "Main",
         "type_multi": true,
         "new_item": false,
         "child_routes": [
            {
               "menu_title": "User & Group Management - Maker",
               "child_routes": [
                  {
                     "path": "/app/main/user-management-maker",
                     "menu_title": "User Management Maker"
                  },
                  {
                     "path": "/app/main/group-management-maker",
                     "menu_title": "Group Management Maker"
                  },
                  {
                     "path": "/app/main/group-management-maker",
                     "menu_title": "Weather"
                  }
               ]
            },
            {
               "menu_title": "User & Group Management - Approval",
               "child_routes": [
                  {
                     "path": "/app/widgets/general",
                     "menu_title": "User Management Approval"
                  },
                  {
                     "path": "/app/widgets/general",
                     "menu_title": "Group Management Approval"
                  }
               ]
            },
            {
               "menu_title": "Inquiry User & Group Management",
               "child_routes": [
                  {
                     "path": "/app/widgets/general",
                     "menu_title": "User Management Maker"
                  },
                  {
                     "path": "/app/widgets/general",
                     "menu_title": "User Deleted Maker"
                  },
                  {
                     "path": "/app/widgets/general",
                     "menu_title": "Group Management Maker"
                  },
                  {
                     "path": "/app/widgets/general",
                     "menu_title": "Group Deleted Makerr"
                  },
                  
               ]
            }
         ]
      },
      {
         "menu_title": "Parameter System - Maker",
         "type_multi": null,
         "new_item": false,
         "child_routes": [
            {
               "path": "/dashboard/crm/projects",
               "new_item": true,
               "menu_title": "sidebar.projects"
            },
            {
               "path": "/dashboard/crm/clients",
               "new_item": true,
               "menu_title": "sidebar.clients"
            },
            {
               "path": "/dashboard/crm/reports",
               "new_item": true,
               "menu_title": "sidebar.reports"
            }
         ]
      }
      
   ]
}
